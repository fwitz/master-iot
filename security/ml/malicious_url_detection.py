# Load Libraries - Make sure to run this cell!
import pandas as pd
import numpy as np
import re, os
from string import printable
from sklearn import model_selection
import sklearn.metrics as metrics
import matplotlib.pyplot as plt
import seaborn as sn

#import gensim
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential, Model, model_from_json, load_model
from keras import regularizers
from keras.layers.core import Dense, Dropout, Activation, Lambda, Flatten
from keras.layers import Input, ELU, LSTM, Embedding, Convolution2D, MaxPooling2D, \
BatchNormalization, Convolution1D, MaxPooling1D, concatenate
from keras.preprocessing import sequence
from tensorflow.keras.optimizers import SGD, Adam, RMSprop
from keras.utils import np_utils
from keras import backend as K

from pathlib import Path
import json

import warnings
warnings.filterwarnings("ignore")

## Load data URL
DATA_HOME = 'datasets/'
df = pd.read_csv(DATA_HOME + 'url_data_mega_deep_learning.csv')
df.sample(n=25).head(25) 

# Initial Data Preparation URL

# Step 1: Convert raw URL string in list of lists where characters that are contained in "printable" are stored encoded as integer 
url_int_tokens = [[printable.index(x) + 1 for x in url if x in printable] for url in df.url]

# Step 2: Cut URL string at max_len or pad with zeros if shorter
max_len=75
X = sequence.pad_sequences(url_int_tokens, maxlen=max_len)
 
# Step 3: Extract labels form df to numpy array
target = np.array(df.isMalicious)

print('Matrix dimensions of X: ', X.shape, 'Vector dimension of target: ', target.shape)

# Simple Cross-Validation: Split the data set into training and test data
X_train, X_test, target_train, target_test = model_selection.train_test_split(X, target, test_size=0.25, random_state=33)

def plot_metrics(nb_epochs, loss, accuracy, val_loss, val_accuracy):
    """ Method that plot training and testing metrics of a network """
    fig, axs = plt.subplots(2, sharex=True, sharey=True)
    fig.suptitle('Training and validation metrics')
    axs[0].plot(nb_epochs, loss, 'g', label='Training loss')
    axs[0].plot(nb_epochs, accuracy, 'r', label='Training accuracy')
    axs[1].plot(nb_epochs, val_loss, 'g', label='Validation loss')
    axs[1].plot(nb_epochs, val_accuracy, 'r', label='Validation accuracy')
    axs[1].set_xlabel('Epochs')
    axs[0].legend(loc='upper right')
    axs[1].legend(loc='upper right')
    plt.show()

def plot_cm_roc(model, x_test, y):
    """ Method that print confusion matrix and roc curve for the specified model """
    pred: list[int] = model.predict(x_test)
    pred = np.reshape(pred, (pred.shape[0]))
    conf = metrics.confusion_matrix(y, pred.round())
    sn.set(font_scale=1.4) # for label size
    sn.heatmap(conf, annot=True, annot_kws={"size": 16}) # font size
    plt.show()
    fpr, tpr, threshold = metrics.roc_curve(y, pred)
    roc_auc = metrics.auc(fpr, tpr)
    plt.title('ROC-AUC graph')
    plt.plot(fpr, tpr, 'b', label = 'AUC = %0.2f' % roc_auc)
    plt.legend(loc = 'lower right')
    plt.plot([0, 1], [0, 1],'r--')
    plt.xlim([0, 1])
    plt.ylim([0, 1])
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    plt.show()

## Deep Learning model Definition --- A --- (Simple LSTM)
def simple_lstm(max_len=75, emb_dim=32, max_vocab_len=100, lstm_output_size=32):
    # Input
    main_input = Input(shape=(max_len,), dtype='int32', name='main_input')
    # Embedding layer
    emb = Embedding(input_dim=max_vocab_len, output_dim=emb_dim, input_length=max_len)(main_input) 

    # LSTM layer
    lstm = LSTM(lstm_output_size)(emb)
    lstm = Dropout(0.5)(lstm)
    
    # Output layer (last fully connected layer)
    output = Dense(1, activation='sigmoid', name='output')(lstm)

    # Compile model and define optimizer
    model = Model(main_input, output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model

## Deep Learning model Definition --- B--- (Bidirectionnal LSTM)
def bidirrectional_lstm(max_len=75, emb_dim=32, max_vocab_len=100, lstm_output_size=32):
    # Input
    main_input = Input(shape=(max_len,), dtype='int32', name='main_input')
    # Embedding layer
    emb = Embedding(input_dim=max_vocab_len, output_dim=emb_dim, input_length=max_len)(main_input) 

    # LSTM layer
    lstm = LSTM(lstm_output_size)(emb)
    lstm = Dropout(0.5)(lstm)
    lstm = LSTM(lstm_output_size)(lstm, go_backwards=True)
    
    # Output layer (last fully connected layer)
    output = Dense(1, activation='sigmoid', name='output')(lstm)

    # Compile model and define optimizer
    model = Model(main_input, output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model

def lstm_conv(max_len=75, emb_dim=32, max_vocab_len=100, lstm_output_size=32):
    # Input
    main_input = Input(shape=(max_len,), dtype='int32', name='main_input')
    # Embedding layer
    emb = Embedding(input_dim=max_vocab_len, output_dim=emb_dim, input_length=max_len)(main_input) 
    emb = Dropout(0.25)(emb)

    # Conv layer
    conv = Convolution1D(kernel_size=5, filters=256)(emb)
    conv = ELU()(conv)

    conv = MaxPooling1D(pool_size=4)(conv)
    conv = Dropout(0.5)(conv)

    # LSTM layer
    lstm = LSTM(lstm_output_size)(conv)
    lstm = Dropout(0.5)(lstm)
    
    # Output layer (last fully connected layer)
    output = Dense(1, activation='sigmoid', name='output')(lstm)

    # Compile model and define optimizer
    model = Model(main_input, output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    return model

def bilstm_conv(max_len=75, emb_dim=32, max_vocab_len=100, lstm_output_size=32):
    # Input
    main_input = Input(shape=(max_len,), dtype='int32', name='main_input')
    # Embedding layer
    emb = Embedding(input_dim=max_vocab_len, output_dim=emb_dim, input_length=max_len)(main_input) 
    emb = Dropout(0.25)(emb)

    # Conv layer
    conv = Convolution1D(kernel_size=5, filters=256)(emb)
    conv = ELU()(conv)

    conv = MaxPooling1D(pool_size=4)(conv)
    conv = Dropout(0.5)(conv)

    # LSTM layer
    lstm = LSTM(lstm_output_size, return_sequences=True)(conv)
    lstm = Dropout(0.5)(lstm)
    lstm = LSTM(lstm_output_size)(lstm)
    
    # Output layer (last fully connected layer)
    output = Dense(1, activation='sigmoid', name='output')(lstm)

    # Compile model and define optimizer
    model = Model(main_input, output)
    adam = Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    model.compile(optimizer=adam, loss='binary_crossentropy', metrics=['accuracy'])
    model.summary()
    return model

# Fit model and Cross-Validation, ARCHITECTURE 1 SIMPLE LSTM
epochs = 3
batch_size = 32

"""model_simple = simple_lstm()
history_simple = model_simple.fit(X_train, target_train, epochs=epochs, batch_size=batch_size, validation_data=(X_test, target_test), verbose=1)

print('\nFinal Cross-Validation Accuracy', history_simple.history['val_accuracy'][-1], '\n')
plot_metrics(history_simple.epoch, history_simple.history['loss'],  history_simple.history['accuracy'], history_simple.history['val_loss'], history_simple.history['val_accuracy'])
plot_cm_roc(model_simple, X_test, target_test)

model_bi = simple_lstm()
history_bi = model_bi.fit(X_train, target_train, epochs=epochs, batch_size=batch_size, validation_data=(X_test, target_test), verbose=1)

print('\nFinal Cross-Validation Accuracy', history_bi.history['val_accuracy'][-1], '\n')
plot_metrics(history_bi.epoch, history_bi.history['loss'],  history_bi.history['accuracy'], history_bi.history['val_loss'], history_bi.history['val_accuracy'])
plot_cm_roc(model_bi, X_test, target_test)"""

# Fit model and Cross-Validation, ARCHITECTURE 2 CONV + LSTM
epochs = 5
batch_size = 32

"""model_simple_conv = lstm_conv()
history_simple_conv = model_simple_conv.fit(X_train, target_train, epochs=epochs, batch_size=batch_size, validation_data=(X_test, target_test), verbose=1)

print('\nFinal Cross-Validation Accuracy', history_simple_conv.history['val_accuracy'][-1], '\n')
plot_metrics(history_simple_conv.epoch, history_simple_conv.history['loss'],  history_simple_conv.history['accuracy'], history_simple_conv.history['val_loss'], history_simple_conv.history['val_accuracy'])
plot_cm_roc(model_simple_conv, X_test, target_test)"""

model_bi_conv = bilstm_conv()
history_bi_conv = model_bi_conv.fit(X_train, target_train, epochs=epochs, batch_size=batch_size, validation_data=(X_test, target_test), verbose=1)

print('\nFinal Cross-Validation Accuracy', history_bi_conv.history['val_accuracy'][-1], '\n')
plot_metrics(history_bi_conv.epoch, history_bi_conv.history['loss'],  history_bi_conv.history['accuracy'], history_bi_conv.history['val_loss'], history_bi_conv.history['val_accuracy'])
plot_cm_roc(model_bi_conv, X_test, target_test)

# As you can see after executing the code that the simple LSTM has better results than the bidirectionnal one
# It can be explained maybe by the fact that urls have a one way reading and as bidirectionnal LSTM interpret data forward
# and backward, the backward part isn't efficient in this case.