import pandas as pd
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import confusion_matrix, classification_report
from xgboost import XGBClassifier
import matplotlib.pyplot as plt

def char_tokens(input_str: str) -> list[str]:
    """ Method that break a string in a list of char """
    return [x for x in input_str]

def run():
    """ Method doing all the setup and the computing """
    # Reading dataset
    data = pd.read_csv("datasets/passwordDataset.csv", dtype={"password":"str", "strength": "int"}, index_col=None)
    # Shuffle data
    data = data.sample(frac=1)
    # Split the data in two datasets for training and testing
    l = len(data.index)
    train_data = data.head(int(l*0.8))
    test_data = data.tail(int(l*0.2))
    # Create features labels and featured data
    y_train = train_data.pop("strength").values
    y_test = test_data.pop("strength").values
    x_train = train_data.values.flatten()
    x_test = test_data.values.flatten()
    # Creating pipeline to perform TF-IDF on the password's chars
    password_clf = Pipeline(
        [("vect", TfidfVectorizer(tokenizer=char_tokens)), ("clf", XGBClassifier())]
    )
    # Training and testing the pipeline
    password_clf.fit(x_train, y_train)
    score = password_clf.score(x_test, y_test)
    print(score)
    common_password: str = "qwerty"
    strong_password: str = "c9lCwLBFmdLbG6iWla4H"
    strengths: list[int] = password_clf.predict([common_password, strong_password])
    print("Strength of the common password: ", strengths[0])
    print("Strength of the strong password: ", strengths[1])
    pred: list[int] = password_clf.predict(x_test)
    conf = confusion_matrix(y_test, pred)
    plt.imshow(conf, cmap='binary', interpolation='None')
    plt.show()
    print(classification_report(y_test, pred, target_names=[0, 1, 2]))

if __name__ == "__main__":
    """ Main method """
    run()
