def logical_xor(bit1: str, bit2: str) -> str:
    """ Method that compute a logical xor on the two given str bits"""
    return str(int(bool(int(bit1)) ^ bool(int(bit2))))

def logical_and(bit1: str, bit2: str) -> str:
    """ Method that compute a logical and on the two given str bits"""
    return str(int(bool(int(bit1)) & bool(int(bit2))))

def count_numbers(numbers: list[str]) -> tuple[int, int]:
    """ Method that counts the number of odd and even numbers in a number list"""
    odd: int = 0
    even: int = 0
    for n in numbers:
        if int(n)%2 == 0:
            even += 1
        else:
            odd += 1
    return (even, odd)


def permutation(chain: str, shift: int) -> str:
    """ Method that makes the n bits right/left permutation of a chain. If shift > 0, 
    it permute the chain to the right"""
    return chain[-shift:] + chain[:-shift]