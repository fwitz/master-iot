from rsa import euclide_entendu
from fast_exponentiation import modular_exponentiation
import random
from math import floor, log2,pow
from utils import logical_xor, permutation

def generate_keys() -> tuple[int, tuple[int, int]]:
    """ Method that generate a key pair for the Blum Goldwasser algorithm"""
    p: int = 4*random.randint(1e08, 1e15) + 3 # Randomy picking a big prime number
    q: int = 4*random.randint(1e08, 1e15) + 3 # Randomly picking a second prime number
    n: int = p*q # Computing the multiplication of the two previous picked prime numbers
    return (n, (p, q))

def encrypt(msg: str, public_key: int) -> list[str]:
    """ Method to encrypt a message using Blum Goldwasser algorithm """
    # First we compute the block size in bits
    h: int = floor(log2(log2(public_key)))
    # Then we transform the msg in binary
    bin_msg: list[str] = list("".join([bin(ord(m))[2:] for m in msg]))
    # Adn finally we cut the binary message in blocks of h bits
    blocks: list[str] = [bin_msg[i:i+h] for i in range(0, len(bin_msg), h)]
    # Selecting a random integer r<public_key
    r: int = random.randint(2, public_key)
    # Encrypting the message
    x: list[int] = [pow(r, 2)%public_key]
    p: list[str] = []
    c: list[int] = []
    for i in range(1, len(blocks)):
        x.append(pow(x[i-1],2)%public_key)
        p.append(bin(x[i])[2:][-h:])
        c.append(int("".join([logical_xor(blocks[i][j], p[i-1][j]) for j in range(h)])))
    last: int = pow(x[-1],2)%public_key
    return c.append(last)


def decrypt(encrypted: list[int], private_key: tuple[int, int], public_key: int) -> str:
    """ Method to decrypt a message using Blum Goldwasser algorithm """
    # First we compute the block size in bits
    h: int = floor(log2(log2(public_key)))
    # Computing first some variables that we'll use after
    dp: int = pow((private_key[0]+1)/4, len(encrypted))%(private_key[0]-1)
    dq: int = pow((private_key[1]+1)/4, len(encrypted))%(private_key[1]-1)
    up: int = modular_exponentiation(encrypted[-1], dp, private_key[0])
    uq: int = modular_exponentiation(encrypted[-1], dq, private_key[1])
    rp, rq = euclide_entendu(private_key[0], private_key[1])[1:]
    # Start the decryption process
    x: list[int] = [uq*rp*private_key[0] + (up*rq*private_key[1]%public_key)]
    p: list[str] = []
    m: list[str] = []
    for i in range(len(encrypted)-1):
        x.append(pow(x[i-1], 2)%public_key)
        p.append(bin(x[i])[2:][-h:])
        m.append(str("".join([logical_xor(encrypted[i][j], p[i-1][j]) for j in range(h)])))
    return "".join(m)

if __name__ == "__main__":
    # Generating keys
    public_key, private_key = generate_keys()
    msg: str = input("Enter your message: ") # Getting message
    # Crypting and decrypting to test the method implementation
    encrypted: list[str] = encrypt(msg, public_key)
    print("Encrypted message: ", "".join(encrypted))
    decrypted: str = decrypt(encrypted, private_key, public_key)
    print("Decrypted message: ", decrypted)