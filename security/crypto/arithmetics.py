import random
import time
import statistics

from fast_exponentiation import modular_exponentiation

def eratosthene(n: int) -> list[int]:
    """ Method that returns all the primes number between 2 and n using the ertosthene method """
    # Generating a list of integer between 2 and n
    int_list: list[int] = [x for x in range(2, n)]
    prime: list[int] = []
    divider: int = int_list[0] # The first divider is 2
    # We loop until the divider is equal to n/2, it means we have reached the end of the list
    while divider != n/2:
        # We loop through each element and remove the one wo are divided by the divider
        for e in int_list:
            if e%divider == 0:
                int_list.remove(e)
        prime.append(divider) # Then we add the divider to the prime numbers list
        # We take the first remaining as divider if the list isn't empty
        if len(int_list) != 0:
            divider = int_list[0]
        else:
            break
    return prime

def fermat(n: int) -> bool:
    """ Method that determine if a prime number n is prime or not using the Fermat little theorem """ 
    a: int = random.randint(2, n-2) # Pick a random number in [2, n-2]
    if modular_exponentiation(a, n-1, n) != 1:
        return False
    else:
        return True

def miller_rabin(d: int, n: int) -> bool:
    """ Method that determine if a number n is prime or not using the miller-rabin algorithm """
    a: int = random.randint(2, n-2) # Pick a random number in [2, n-2]
    x: int = modular_exponentiation(a, d, n) # Compute the modular exponentiation of a by d mod n
    # If x is equal to 1 or n-1 he is prime for sure
    if x == 1 or x == (n-1):
        return True
    while d != (n-1):
        x = (x * x) % n
        d *= 2
        if x == 1:
            return False
        if x == (n-1):
            return True
 
    # If we don't know for sure, we return false
    return False


def is_prime(n: int, k: int, method: str) -> bool:
    """ Method that check if the number n is prime, iterating k times"""
    # Corner cases
    if (n <= 1 or n == 4):
        return False
    if (n <= 3):
        return True
    # Using the given method
    if method == 'mr':
        # Find r such that n = 2^d*r + 1 for some r >= 1
        d = n - 1
        while (d % 2 == 0):
            d //= 2
        # Iterate given nber of 'k' times
        for i in range(k):
            return miller_rabin(d, n)

    elif method == 'fermat':
        for i in range(k):
           return fermat(10000)

if __name__ == "__main__":
    measures_erathostène = []
    for i in range(100):
        start = time.time()
        prime: list[int] = eratosthene(10000)
        end = time.time()
        measures_erathostène.append(end - start)

    mean = statistics.mean(measures_erathostène)
    stdev = statistics.stdev(measures_erathostène)

    print('\nExecution time for eratosthène method:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')
    print("\nPrime number between 2 and {} are {}".format(10000, prime))

    measures_mr = []
    for i in range(100):
        prime: list[int] = []
        start = time.time()
        for j in range(10000):
            if is_prime(j, 4, 'mr'):
                prime.append(j)
        end = time.time()
        measures_mr.append(end - start)

    mean = statistics.mean(measures_mr)
    stdev = statistics.stdev(measures_mr)

    print('\nExecution time for miller-rabin method:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')
    print("\nPrime number smallers than {} are {}".format(10000, prime))

    measures_fermat = []
    for i in range(100):
        prime: list[int] = []
        start = time.time()
        for j in range(10000):
            if is_prime(j, 4, 'fermat'):
                prime.append(j)
        end = time.time()
        measures_fermat.append(end - start)

    mean = statistics.mean(measures_fermat)
    stdev = statistics.stdev(measures_fermat)

    print('\nExecution time for Fermat method:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')
    print("\nPrime number smallers than {} are {}".format(10000, prime))

