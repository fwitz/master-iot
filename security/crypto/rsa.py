from genericpath import exists
import os
import random
import base64

from arithmetics import is_prime, miller_rabin
from fast_exponentiation import modular_exponentiation

def euclide_entendu(a, b) -> list[int]:
    """ Method that compute Bezout coefficient for a and b in a recursive way"""
    if b == 0:
        return [a, 1, 0]
    else:
        coeff: list[int] = euclide_entendu(b, a%b)
        return [coeff[0], coeff[2], coeff[1] - (a//b)*coeff[2]]


def compute_keys(p1: int, p2: int) -> tuple[tuple[int, int], int]:
    """ Method that compute public and private key for the RSA algorithm """
    n:int = p1 * p2 # Computing n, multiplication of two great prime numbers
    c: int = random.randint(1, 20)
    coeff: int = euclide_entendu(c, (p1-1)*(p2-1))
    while coeff[0] != 1 or coeff[1] < 0:
        c += 1
        coeff = euclide_entendu(c, (p1-1)*(p2-1))
    return ((n, c), coeff[1])


def str_to_number(chain: str) -> int:
    """ Method that convert a message into a number"""
    return int("".join([str(ord(s)) for s in chain]))


def number_to_str(encrypted: str) -> str:
    """ Method that convert a large number into a message """
    return "".join([chr(int(list(encrypted)[i] + list(encrypted)[i+1])) for i in range(0, len(encrypted), 2)])


def str_to_block(chain: str, n: int) -> list[str]:
    """ Method that cut a chain into block of size n"""
    blocks: list[int] = []
    counter: int = 0
    finished: bool = False
    while not finished:
        block: list[str] = []
        for i in range(counter, counter + n):
            try:
                block.append(list(str(chain))[i])
            except IndexError:
                finished = True
                break
        blocks.append(int("".join(block)))
        counter += n
    return blocks


def rsa_encrypt(public_key: tuple[int, int], to_encrypt: int) -> int:
    """ Method to encrypt a message using rsa algorithm """
    blocks: list[str] = str_to_block(to_encrypt, public_key[0])
    # Encrypt each block with rsa method and return a congregate of each encrypted block
    encrypted: list[str] = []
    for b in blocks:
        encrypted.append(str(modular_exponentiation(b, public_key[1],public_key[0])))
    print(int("".join(encrypted)))
    return int("".join(encrypted))


def rsa_decrypt(private_key: int, n: int, to_decrypt: int) -> int:
    """ Method to decrypt a message using rsa algorithm """
    blocks: list[str] = str_to_block(to_decrypt, n)
    # Decrypt each block with rsa method and return a congregate of each decrypted block
    decrypted: list[str] = []
    for b in blocks:
        decrypted.append(str(modular_exponentiation(b, private_key,n)))
    return int("".join(decrypted))


if __name__ == "__main__":
    # Firstly we choose two great prime numbers by using the miller-rabin method
    print("Generating RSA keys...")
    primes: list[int] = []
    for i in range(2):
        is_prime: bool = False
        while not is_prime:
            # Generating a random number of 512 digits
            rand_nb: int = random.getrandbits(1700)
            # Finding a r for the miller-rabin algorithm
            d: int = rand_nb - 1
            while (d % 2 == 0):
                d //= 2
            # Checking if the choosed number is prime with the miller-rabin method
            if miller_rabin(d, rand_nb):
                is_prime = True
        primes.append(rand_nb)
    # Computing RSA keys
    keys: tuple[tuple[int, int], int] = compute_keys(primes[0], primes[1])
    next: str = "y"
    while next == "y":
        method: str = input("Encrypt or decrypt ? [e/d] ")
        while method != 'e' and method != 'd':
            method = input("Wrong choice. Encrypt or decrypt ? [e/d] ")
        if method == 'e':
            choice: str = input("Encrypt a message or a file ? [m/f] ")
            while choice != "m" and choice != "f":
                choice = input("Wrong choice. Encrypt a message or a file ? [m/f]")  
            if choice == 'm':
                msg: str = input("Enter your message: ")
                encoded: int = str_to_number(msg) # Converting entered message to a large number
                encrypted: str = number_to_str(str(rsa_encrypt(keys[0], encoded))) # Encrypt message
                print("Encrypted message: ", encrypted)
                next = input("Do you want to do an other operation ? [y/n]")
                while next != 'y' and next != 'n':
                    next = input("Wrong choice. Do you want to do an other operation ? [y/n] ")
            elif choice == 'f':
                exist: bool = False
                while not exist:
                    path: str = input("Enter your file complete path: ")
                    # Checking if file exists
                    if os.path.exists(path):
                        # Encode the file as b64 string to convert it into a number
                        with open(path, 'rb') as file:
                            b64_encoded: str = base64.b64encode(file.read()).decode('utf-8')
                        exist = True
                    else:
                        print("Wrong path, impossible to open the specified file")
                encoded: int = str_to_number(b64_encoded) # Converting b64 encoded file to a large number
                encrypted: str = number_to_str(str(rsa_encrypt(keys[0], encoded))) # Encrypt file
                print("Encrypted file: ", encrypted)
                next = input("Do you want to do an other operation ? [y/n]")
                while next != 'y' and next != 'n':
                    next = input("Wrong choice. Do you want to do an other operation ? [y/n] ")
        elif method == 'd':
            choice: str = input("Decrypt a message or a file ? [m/f] ")
            while choice != "m" and choice != "f":
                choice = input("Wrong choice. Decrypt a message or a file ? [m/f]")
            if choice == 'm':
                msg: str = input("Enter your message: ")
                encoded: int = str_to_number(msg) # Converting entered message to a large number
                decrypted: str = number_to_str(str(rsa_decrypt(keys[1], keys[0][0], encoded))) # Encrypt message
                print("Decrypted message: ", decrypted)
                next = input("Do you want to do an other operation ? [y/n]")
                while next != 'y' and next != 'n':
                    next = input("Wrong choice. Do you want to do an other operation ? [y/n] ")
            elif choice == 'f':
                pass