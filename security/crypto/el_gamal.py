import random

from arithmetics import miller_rabin
from fast_exponentiation import fast_exponentiation, modular_exponentiation
from rsa import str_to_number, number_to_str


def gen_public_key(p: int, g: int, a: int) -> tuple[int, int, int]:
    """ Method that generate public key for el gamal algorithm """
    A:int = modular_exponentiation(g, a, p)
    return (p, g, A)


def encrypt(msg: int, public_key: tuple[int, int, int], private_key: int) -> tuple[int, int]:
    """ Method to encrypt a message using el gamal algorithm """
    B: int = modular_exponentiation(public_key[1], private_key, public_key[0])
    c: int = msg*modular_exponentiation(public_key[2], private_key, public_key[0])
    return (B, c)


def decrypt(to_decrypt: tuple[int, int], public_key: tuple[int, int, int], private_key: int) -> int:
    """ Method to decrypt a message using el gamal algorithm """
    return (modular_exponentiation(to_decrypt[0], (public_key[0]-1-private_key)*to_decrypt[1], public_key[0]))


if __name__ == "__main__":
    # Picking a big random prime number (512 bits) using miller-rabin method
    nb_is_prime: bool = False
    while not nb_is_prime:
        # Generating a random number of 512 digits
        p: int = random.getrandbits(1700)
        # Finding a r for the miller-rabin algorithm
        d: int = p - 1
        while (d % 2 == 0):
            d //= 2
        # Checking if the choosed number is prime with the miller-rabin method
        if miller_rabin(d, p):
            nb_is_prime = True
    # Generating keys
    g: int = random.randint(2, p)
    a: int = random.randint(2, p-1)
    while a == g:
        g = random.randint(2, p)
        a = random.randint(2, p-1)
    public_key: tuple[int, int, int] = gen_public_key(p, g, a)
    msg: str = input("Enter a message to encrypt: ")
    encrypted: int = encrypt(str_to_number(msg), public_key, a)
    print("Encrypted message: ", str(encrypted[1]))
    decrypted: int = decrypt(encrypted, public_key, a)
    print("Decrypted message: ", number_to_str(str(decrypted)))

    
    