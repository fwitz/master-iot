import random
import time
import statistics

def fast_exponentiation(x: int, e: int) -> int:
    """ Method that compute x**e by using the fast exponentiation method"""
    # Converting e into base 2, remove LSB and replace all 0 by 'S' and all 1 by 'SX' to obtain the exponentiation pattern
    pattern: list[str] = list("".join(['S' if b == 0 else 'SX' for b in list((bin(e)[2:])[:-1])]))
    res: int = x
    for p in pattern:
        if p == 'S':
            res = res * res
        elif p == 'X':
            res = res * x
    return res

def modular_exponentiation(x: int, y: int, p: int) -> int:
    """ Method that compute x^y using the modulare exponentiation algorithm """
    res: int = 1
    x = x % p # Update x if x => p by computing x%p
    while (y > 0):
        # If y is odd, we compute (res * x ) mod p
        if (y & 1):
            res = (res * x) % p
        y = y>>1; # y = y/2
        x = (x * x) % p
    return res

if __name__ == "__main__":
    # Compute 100 time x**e with big x and e generated randomly to have an overview of the method efficiency
    measures = []
    for i in range(100):
        x: int = random.randint(0,1000)
        e: int = random.randint(0,1000)
        start = time.time()
        res: int = fast_exponentiation(x, e)
        end = time.time()
        measures.append(end - start)

    mean = statistics.mean(measures)
    stdev = statistics.stdev(measures)

    print('\nExecution time for fast exponentiation:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("\nThe result of {}^{} is: {}".format(str(x), str(e), str(res)))
    