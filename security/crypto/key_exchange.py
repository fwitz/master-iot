import random

from arithmetics import miller_rabin
from fast_exponentiation import modular_exponentiation

if __name__ == "__main__":
    # Picking a big random prime number (512 bits) using miller-rabin method
    nb_is_prime: bool = False
    while not nb_is_prime:
        # Generating a random number of 512 digits
        p: int = random.getrandbits(1700)
        # Finding a r for the miller-rabin algorithm
        d: int = p - 1
        while (d % 2 == 0):
            d //= 2
        # Checking if the choosed number is prime with the miller-rabin method
        if miller_rabin(d, p):
            nb_is_prime = True
    print("Prime number: ", p)
    # Picking generator and one numbers for Alice and Bob
    g: int = random.randint(2, p-1)
    a: int = random.randint(2, p)
    b: int = random.randint(2, p)
    print("\nGenerator: ", g)
    # Computing secret key
    A: int = modular_exponentiation(g, a, p)
    B: int = modular_exponentiation(g, b, p)
    print("\nAlice send to Bob the following number: ", A)
    print("\nBob send to Alice the following number: ", B)
    print("\nAlice obtain the following secret key: ", modular_exponentiation(B, a, p))
    print("\nBob obtain the following secret key: ", modular_exponentiation(A, b, p))