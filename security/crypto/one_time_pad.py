import secrets
import random
import generators

from utils import logical_xor

def generate_message(size: int) -> str:
    """ Method that generate a message to encrypt randomly. The message is a random chain of bits"""
    message = [str(random.randint(0, 1)) for i in range(size)]
    return "".join(message)


def generate_mask(size: int) -> str:
    """ Method that generate in a strong and safe way a one time mask using secrets module. The mask is a chain of bits"""
    mask = [str(secrets.randbits(1)) for i in range(size)]
    return "".join(mask)


def encrypt(message: str, mask: str, size: int) -> str:
    """ Method that encrypt a given message using the one time pad method (or Vernam cypher)"""
    msg = [logical_xor(message[i], mask[i]) for i in range(size)]
    return "".join(msg)


def complex_encrypt(message: str, mask: str, size: int) -> str:
    """ Method that encrypt a given message using the one time pad method using a complex mask"""
    msg = [str((int(message[i]) + int(mask[i]))%2) for i in range(size)]
    return "".join(msg)
    

if __name__ == "__main__":
    random.seed()
    # Inputing message and mask size
    value: str = input("Enter message and mask size: ")
    try:
        size: int = int(value)
    except ValueError:
        print("Size must be an integer !")
    # Generate the message and print it
    message: str = generate_message(size)
    print("Generated message: " + message)
    # Generate the one time pad mask and print it
    random_mask: str = generate_mask(size)
    linear_mask: str = "".join(generators.linear_congruency_generator(5, 201, 1024, size))
    print("Generated random mask: " + random_mask)
    print("Generated linear mask: " + linear_mask)
    # Encrypt the message and print it
    encrypted_random_msg: str = encrypt(message, random_mask, size)
    encrypted_linear_msg: str = complex_encrypt(message, linear_mask, size)
    print("Encrypted message with random mask: " + encrypted_random_msg)
    print("Encrypted message with linear mask: " + encrypted_linear_msg)