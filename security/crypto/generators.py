import random
import time
import statistics
from string import digits

from utils import *


def linear_congruency_generator(a: int, c: int, m: int, n: int) -> list[str]:
    """ 
    Method that generate a serie of n number between 0 and m-1 using linear congruency method
    a, c and m are the parameters of the generator that must be choose correctly
    """
    seed: int = random.randint(0, m-1) # Generating a base seed between 0 and m-1
    generated: list[str] = [str(seed)]
    # Generate a chain of numbers usign the linear congruency formula
    for i in range(n-1):    
        generated.append(str((a*int(generated[i]) + c)%m))
    return generated


def xorshift_generator(a: int, b: int, c: int, n:int, size: int) -> str:
    """ 
    Method that generate a chain of 'size' bits using Marsaglia's xorshift method 
    a, b and c are the shift parameters that must be in [0, size]
    """
    seed: list[str] = [str(random.randint(0, 1)) for i in range(size)] # Generating a base seed of 'size' bits
    generated: list[str] = ["".join(seed)]
    # Generating n chain using xorshift method and returning the last generated one
    for i in range(n):
        generated.append("".join[(
            logical_xor(
                logical_xor(
                    logical_xor(
                        "".join(generated[i][j]), 
                        permutation("".join(generated[i][j], a))
                    ), 
                    permutation("".join(generated[i][j], -b))
                ), 
                permutation("".join(generated[i], c))
            ) for j in range(size))])
    return generated[-1]


def linear_feedback_shift_register_generator(n: int, size: int, index: list[int]) -> str:
    """ Method that generate a chain of 'size' bits using a linear feedback shift register"""
    seed: list[str] = [str(random.randint(0, 1)) for i in range(size)] # Generating a base seed of 'size' bits
    generated: list[str] = ["".join(seed)]
    # Generating n chain using LFSR method and returning the last one
    for i in range(n):
        permuted: str = permutation(generated[i], 1) # Shifting the chain of 1 bits to the right
        out: str = ""
        # Computing the out bits to put at the begining of the shifted chain
        # We use the index list to compute logical xor between all the bits
        for j in range(len(index)):
            if index[j] <= size:
                if out == "":
                    out = logical_xor(generated[i][index[0]], generated[i][index[1]])
                else:
                    out = logical_xor(out, generated[i][index[j]])
        # Then, we put the out bit at the MSB place
        permuted_list: list[str] = list(permuted)
        permuted_list[0] = out
        generated.append("".join(permuted_list))
    return generated[-1]


def mersenne_twister_generator(w: int, n: int, m: int, r: int, a: int, u: int, d: int, s: int, b: int, t: int, c: int, l: int, f: int) -> str:
    """ Method that generate a chain of w bits using the mersenne-twister algorithm """
    # Creating a list to store the generator state and initialize it with a random seed of w digits converted to binary
    state: list[str] = [(bin(int("".join(random.choice(digits) for i in range(w))))[2:])]
    index: int = n
    lower_mask: str = bin((1 << r) - 1)[2:]
    upper_mask: str = (bin(~int(lower_mask))[2:])[-w:]
    # The first step is to loop over all element to initialize them
    for i in range(1,n): 
        # Computing bitwise xor between state[i-1] number and itself shifted of w-2 to the left
        # and storing it on state[i]
        state.append(
            (bin(f * int("".join([
                logical_xor(state[i-1][j],str((int(state[i-1][j]) >> (w-2)))) for j in range(w)
            ])) + i)[2:])[-w:]
        )
    # The second step is to extract a tempered number from each state, then we compute a specific xA value
    # and finally we replace the initial state value with the computed one
    if index >= n:
        for i in range(0,n):
            x: str = (bin(int("".join([
                logical_and(state[i][j], upper_mask[j]) for j in range(w)
            ])) + int("".join([
                logical_and(state[(i+1)%(n-1)][j], lower_mask[j]) for j in range(w-1)
            ])))[2:])[-w:]
            xA: str = bin(int(x) >> 1)[2:]
            if (int(x)%2) != 0:
                xA = bin(int("".join([
                    logical_xor(xA[j], (bin(a)[2:])[j]) for j in range(w)
                ])))[2:][-w:]
            state[i] = (bin(int("".join([
                logical_xor(state[(i+m)%(n-1)][j], xA[j]) for j in range(w-1)
            ])))[2:])[-w:]
        index = 0

    # Finally we iterate n time to generate the final chain
    # We compute a series of bitwise logical xor 
    generated: list[str] = []
    for k in range(n):
        y: str = state[k]
        y = bin(int("".join([
            logical_xor(y[j], logical_and((bin(int(y)>>u)[2:])[j], (bin(d)[2:])[j])) for j in range(w)
        ])))[2:]
        y = bin(int("".join([
            logical_xor(y[j], logical_and((bin(int(y)<<s)[2:])[j], (bin(b)[2:])[j])) for j in range(w)
        ])))[2:]
        y = bin(int("".join([
            logical_xor(y[j], logical_and((bin(int(y)<<t)[2:])[j], (bin(c)[2:])[j])) for j in range(w)
        ])))[2:]
        y = bin(int("".join([
            logical_xor(y[j], (bin(int(y)>>l)[2:])[j]) for j in range(len(y))
        ])))[2:]
    
        generated.append(y[-w:])
    return generated[-1]


def bbs_generator(n: int, size: int) -> str:
    """ Method that generate a chain of n bits using the blum blum shub algorthim """
    # Init the series with a random int between 2 and n-1
    generated: list[str] = [str(random.randint(2, n-1))]
    p: int = 4*random.randint(1e08, 1e15) + 3 # Randomy picking a big blum prime number
    q: int = 4*random.randint(1e08, 1e15) + 3 # Radnomly picking a second blum prime number
    m: int = p*q # Computing the multiplication of the two previous picked blum prime number
    # Generating a series of n-1 size usgin bbs series formula
    for i in range(n-1):
        generated.append(str((int(generated[i])**2)%m))
    return bin(int("".join(generated)))[-size:] # Return the last 'size' bits


def f(a: int, i: int) -> str:
    """ Method used in isaac generator to compute shifts of a depending of i"""
    shifts: list[int] = [13, 6, 2, 16] # Storing a's shifts values in an array
    mod: int = i%4
    if mod == 0 or mod == 3:
        return a << shifts[mod]
    elif mod == 1 or mod == 2:
        return a >> shifts[mod]


def isaac_generator(a: int, b: int, c: int, n: int) -> str:
    """ Method that generate n list of 256 32 bits words using the isaac algorithm and return a random one of the last generated list """
    # Generating the initial state list with 256 random 32 bits words
    state: list[str] = [bin(int("".join([
        str(random.randint(0, 1)) for j in range(32)
    ])))[2:] for i in range(256)]
    for i in range(n):
        b = b + c + 1
        generated: list[str] = []
        # We iterate through the state list and update it
        for j in range(256):
            x: str = state[j] # Getting the 
            a = f(a, j) + int(state[(j+128)%256])
            state[j] = (bin(a + b + int(state[(int(x)>>2)%256]))[2:])[-32:]
            generated.append(
                (bin(int(x) + int(state[(int(state[j]) >> 10)%256]))[2:])[-32:]
            )
            b = int(generated[j][-32:])
    # Returning a random generated 32 bits words
    return generated[random.randint(0, 255)][-32:]

if __name__ == "__main__":
    # Generating a 100 series to measure the mean execution time of the linear congruency method
    measures_linear = []
    for i in range(100):
        start = time.time()
        chain: list[str] = linear_congruency_generator(5, 201, 1024, 50)
        end = time.time()
        measures_linear.append(end - start)
    
    mean = statistics.mean(measures_linear)
    stdev = statistics.stdev(measures_linear)

    print('Execution time for linear congruency:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    count: tuple[int, int] = count_numbers(chain)
    print("\nNumber od even numbers: " + str(count[0]))
    print("Number of odd numbers: " + str(count[1]))

    print("\nLinear Congruency Chain: " + "".join(chain))

    # Generating 100 chain to measures the mean execution time of the xorshift method
    measures_xor = []
    for i in range(100):
        start = time.time()
        chain = xorshift_generator(12, 25, 27, 10, 32)
        end = time.time()
        measures_xor.append(end - start)

    mean = statistics.mean(measures_xor)
    stdev = statistics.stdev(measures_xor)

    print('\nExecution time for XORShift:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("XorShift Chain: " + chain)

    # Generating 100 chain to measure the execution time of the LFSR method
    measures_lfsr = []
    for i in range(100):
        start = time.time()
        chain = linear_feedback_shift_register_generator(10, 32, [5, 9, 16, 24])
        end = time.time()
        measures_lfsr.append(end - start)

    mean = statistics.mean(measures_lfsr)
    stdev = statistics.stdev(measures_lfsr)

    print('\nExecution time for LFSR:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("\nLFSR Chain: " + chain)

    # Generating 100 chain to measure the execution time of the mersenne-twister method
    measures_mt = []
    for i in range(100):
        start = time.time()
        chain = mersenne_twister_generator(32, 624, 397, 31, 2567483615, 11, 4294967295, 7, 2636928640, 15, 4022730752, 18, 1812433253)
        end = time.time()
        measures_mt.append(end - start)

    mean = statistics.mean(measures_mt)
    stdev = statistics.stdev(measures_mt)

    print('\nExecution time for Mersenne-Twister:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("\nMersenne-Twister Chain: " + chain)

    # Generating 100 series to measure the execution time of the bbs method
    measures_bbs= []
    for i in range(100):
        start = time.time()
        chain = bbs_generator(10, 32)
        end = time.time()
        measures_bbs.append(end - start)

    mean = statistics.mean(measures_bbs)
    stdev = statistics.stdev(measures_bbs)

    print('\nExecution time for BBS:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("\nBBS Chain: " + chain)

    # Generating 100 chain to measure the execution time of the isaac method
    measures_isaac = []
    for i in range(100):
        start = time.time()
        chain = isaac_generator(50, 20, 35, 1)
        end = time.time()
        measures_isaac.append(end - start)

    mean = statistics.mean(measures_isaac)
    stdev = statistics.stdev(measures_isaac)

    print('\nExecution time for Isaac:')
    print(f' - Mean : {mean:.1}ms')
    print(f' - Standard Deviation : {stdev:.1}ms')

    print("\nIsaac Chain: " + chain)
