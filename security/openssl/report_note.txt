Task 1:
All the files used and generated in this part are in the folder task1

1. As base64 use only a limited number of characters, it can be easily recognized. As their is no encryption key, the attacker has only to decode the text file to obtain the password.
   Used the command : openssl enc -base64 -in password.txt -out pass-b64.txt

2. As I mentionned in the last question, it's not a safe way cause the data is not encrypted it's encoded.  

3. Used the command : openssl enc -base64 -d -in pass-b64.txt -out pass-decoded.data

Task 2:
All the files generated in this part are in the folder task2

4. We get the following string: SGVsbG8gZnJvbSB0aGUgb3RoZXIgc2lkZQ== by using the command: echo -n "Hello from the other side" | openssl base64  

5. To do so, we use the command: openssl rand -hex 100. We get: d9097ac8cc2a35c99eefabc75accada09d2a9f1071c861154ccfafd9ae055013a79bfbd31445adc289b2be487dcb37b10322b72cf9cd751ba7f50e64de6aff909779ccbd28cee6a7f34138c3cbda5e8ef8aff4d6cd90d3376fd29991200fc214d805c228

6. We do that with the command : openssl rand -out rand.txt 200

7. For this step we use the command: openssl rand -out rand.base64 -base64 300

Task 3:

The files used and generated with the different cyphertext and modes are in the folder task3

9. When we open the file encrypted using the ECB mode, we can still see what's in the image. We have the impression that the image is very noisy but we still get the two shapes on the images. On the other hand, when we open
the file using the CBC mode, the image is entirely made of colored noise, so we can't see anything.

10. We use the file cyphertext.txt in the folder task3

11. To do so we use the command: openssl enc -aes-128-<mode> -e -in task3/cyphertext.txt -out task3/cyphertext_enc_<mode>.bin with <mode> the encryption mode we use. In this context we used the 4 following ones: ECB, CBC, CFB and OFB

14. I think that the corrupted OFB encrypted file won't be to altered but the other will be. After checking each file, my hypothesis is correct. Indeed, in the OFB decrypted file, the corrupted data is just a wrong letter while in the
other files, a lot of faulty characters have been inserted.

15. In the contrary of the other mode, OFB doesn't use a XOR operation on the already encrypted data but on the block cipher key (a combination of the iv and the key for the first one, and a combination of the last block cipher and the key for the next ones)

16. As I have seen in the documentation, the major difference for the OFB mode is if the attacker knows the iv for the message he wants to crack and the clear message of an other encrypted messsage, he totally can compute the clear
message using powerfull modern maths.

17. We know that PKCS5 padd a block using zeros. We know that AES-128 is a 128-bits block encryption method, so each block will have a size of 16 bytes. With this process, if we test it with a text of 20 bytes, 12 bytes of zeros should be added during the encryption process. (-> to reexplain clearly)
When we test this method, we see that the result of the encryption of the 20bytes_text.txt file is a binary size of 32 bytes, so 12 bytes have been added, like I supposed. Same for the 32bytes_text.txt file, no padding have been added.
Moreover, openssl warns us when padding is used during the encryption, the method here allow us to check how many bytes have been added.

18. In these four modes, there is two different encryption process: by block (ECB and CBC) and by flux (CFB and OFB). As the method using flux encryption doesn't cut data in blocks, they doesn't need padding (because there are no data
block to fill). 

Task 4

The files used and generated for this part in the folder task4

19. I hashed the file to_hash.txt with sha256 with the command openssl sha256 to_hash.txt and I obtained the following hash SHA256(to_hash.txt)= 70825140b14f822d3767a5e4ea6db46039b6bc0a432004e6fb20fbdfba225d03

20. Firstly, I need the hashes of the different files that I want the integrity to be checked. If I have them and the algorithm used, and I can re hash the file and compare the hashes I get to the hashes list I've had with the files. If any hash is different, the integrity is not complete.

After checking the hash with MD5 of each 4 files (by using the command openssl md5 <file>), we see that only the file 2.txt has been modified because its hash is completly different.

21. After modification I get the following hash (with command openssl sha256 <filename>) SHA256(to_hash.txt)= acf5a8782596bb494a7d1c2625ac5909a8d6bc71fae39c8366c73c441c16d82d. We can see that this hash is completly different from the previous generated one.

22. Yes they match because we generate what is called a checksum, and the two hashes must be identical when this checksum is computed. One is stored on a distant server for example et the other one is recomputed each time the file
is sent or downloaded and compared to the original one. If the file chnage during transmission or storing, the hash won't be the same and so we know that the integrity of the file has been compromised.

23. I get, after executing echo -n "Hello from the other side" | openssl dgst -sha3-512, (stdin)= 649e1b6a379649b7f48d7cd77b5947a686efed29985a6ae14b1661274b7d630fc5ce9ac04edb4e4aee58f3de91733fd8d27b43ef7dccce9492d7f70521621df9

24&25. MD5 has 32 hex char because a MD5 hash is 128 bits length. SHA-1 is 160 bits length so a hash has 40 hex char. SHA-256 is 256 bits so a hash is 64 hex char long. SHA-384 is 384 bit so a hash has 96 hex char.
And finally, SHA-512 is 512 bits so a hash is 128 hex char long.

26. To do so we use the command, openssl dgst -sha384 -mac HMAC -macopt hexkey:369bd7d655 rand.txt and we getHMAC-SHA384(rand.txt)= db4153eafeb41b4659e293cd45379877872e9a282678c688589cb3965d726b71bf8da7644d6c968884e06ac4c88207af

27. With the command openssl dgst -sha512 -mac HMAC -macopt hexkey:369bd7d655 and we get (stdin)= aeb0821f90631debd7483366383e376bc92d2add43a3c2259fe98e15f380717a7cecd327837efd48dc2ab6d2fe6a8233152c11c13651a6ff70beaa187862f9dd

Task 5

All the files used and generated on this part are in the folder Task5

28&29. The command openssl genrsa -out mySmall.pem 50 output an error because it's impossible to generate a 50bits RSA key with openssl because the actual minimum limit for a RSA private key is 512bits long.

31. We use the commande openssl genrsa -out private1.pem 1024

32. We use the command openssl genrsa -des3 -out private2.pem 1024

30. The private key should never be stored as plaintext on a random file on a computer, it should be encrypted and stored on a specific place.

33. We use the command openssl rsa -in <privatekey_file> -text -noout 

For private1.pem we get
RSA Private-Key: (1024 bit, 2 primes)
modulus:
    00:eb:18:48:6b:f0:c2:2e:3f:cc:2e:81:c1:0a:29:
    e0:e6:be:ba:49:ff:0b:c7:36:27:83:08:d1:81:78:
    ca:5b:7e:2f:67:26:c9:92:06:ae:dd:17:54:ac:a6:
    45:35:c0:bd:53:a4:76:07:ec:c1:92:b8:79:84:0d:
    48:2f:16:79:06:d6:66:67:59:9b:83:c1:9d:c4:fe:
    c1:11:75:27:4f:00:97:7e:3e:32:b5:db:96:16:16:
    0e:24:ca:db:99:d2:33:74:ae:58:67:64:40:88:af:
    99:55:11:15:36:75:70:f9:b0:16:0f:4c:92:00:38:
    bb:e1:92:ab:1d:92:34:c6:2b
publicExponent: 65537 (0x10001)
privateExponent:
    00:c2:27:6f:c8:15:31:88:d8:8f:5b:e9:34:ef:9b:
    de:6d:32:1e:cf:75:9e:d8:8a:d4:de:d6:ad:8f:6b:
    9e:53:29:47:24:bd:57:c3:0d:59:e4:a5:9b:fc:69:
    e4:c6:9b:c2:c0:0d:16:9d:3c:de:30:b9:c1:41:59:
    66:bf:dc:a3:5d:78:ee:66:35:0d:f9:38:30:40:05:
    c4:2d:b0:92:20:88:a8:d5:9f:a9:22:ed:d5:a6:71:
    65:de:eb:35:d5:eb:9a:15:54:e6:27:2c:0d:99:24:
    b3:d8:2a:87:ff:5e:d0:8b:18:a7:73:bd:ef:db:7d:
    e0:86:95:b0:3e:f2:36:52:a1
prime1:
    00:fa:88:c4:44:a0:1d:29:ec:5a:07:6e:e0:1f:8a:
    55:2b:78:a1:fb:81:a4:67:2f:00:ab:72:cd:98:3f:
    1f:49:0d:a5:f6:29:ef:de:bd:cb:17:77:13:f3:96:
    a6:72:4c:b1:3d:ee:9b:ea:0f:57:d9:15:29:b3:d5:
    8e:27:12:70:65
prime2:
    00:f0:39:49:89:d8:d7:cb:f5:f1:7e:45:e5:c9:05:
    ea:d0:ee:d0:1c:a1:a7:cb:47:42:7c:ba:ef:49:5e:
    2b:dc:90:56:c1:6f:83:08:77:cc:a9:88:fb:b5:09:
    f8:b3:17:f9:ec:9b:ab:e4:0d:e1:33:1d:57:68:84:
    5f:a2:94:cb:4f
exponent1:
    02:fb:55:ba:ab:5d:2d:7e:8e:a3:c0:02:12:de:06:
    9d:79:d6:13:b4:3b:4c:af:16:86:f3:da:d6:91:09:
    c2:48:d3:4c:d6:ba:f0:1f:ba:27:11:57:bf:72:6f:
    90:b4:b0:f7:57:bb:ab:51:7b:6e:2d:fe:4d:11:a1:
    9b:7d:70:b1
exponent2:
    00:e7:58:94:6c:cf:cb:4b:5e:c9:3a:ad:80:15:6f:
    c0:6b:84:82:22:00:c1:02:b0:d6:e4:1e:77:51:f6:
    02:51:c1:23:fb:d8:33:9e:15:19:a1:f0:0d:50:95:
    90:d0:91:77:9c:99:73:1a:07:0f:e9:55:09:97:db:
    5b:ea:cc:57:5b
coefficient:
    15:d7:a3:70:03:2b:e7:69:24:e4:ca:23:1a:5d:6e:
    68:e6:35:df:e9:f5:f4:24:47:7c:86:c0:56:eb:27:
    33:f6:4f:ce:b7:45:29:1b:ec:ac:73:94:11:b9:93:
    ce:7b:6a:ab:e0:17:f8:30:30:b9:ee:83:29:15:33:
    52:96:3f:e5

and for private2.pem we get(after entering the correct passphrase)

RSA Private-Key: (1024 bit, 2 primes)
modulus:
    00:ac:5c:33:94:f0:66:db:5b:32:2d:3b:06:12:4d:
    4a:eb:79:a2:13:d8:18:1f:91:8c:e7:af:85:9e:dc:
    5c:54:30:78:03:54:cc:23:58:a2:b6:a5:79:9f:9d:
    c1:96:10:3c:47:15:34:c6:44:10:a8:a8:b5:eb:a2:
    65:71:09:58:d1:9c:e9:f3:72:c2:c0:55:e3:11:88:
    07:da:12:50:0f:ea:6d:aa:a5:73:2c:f8:04:fe:88:
    de:14:ad:69:e4:0f:01:47:6f:e9:7f:f2:75:b3:4d:
    25:0b:1d:0d:f6:1f:91:f1:cd:96:87:10:9f:cf:a3:
    22:68:87:47:1c:24:9f:8c:59
publicExponent: 65537 (0x10001)
privateExponent:
    00:8d:26:7c:bf:8e:18:4d:af:63:6a:13:1a:27:c6:
    d0:82:02:7c:2e:d0:1b:01:4f:4b:1b:39:6c:ad:ea:
    3c:5a:5d:cf:0a:bb:58:e9:e1:5e:6f:41:18:57:14:
    f4:42:16:4d:a6:d0:c1:05:55:1c:67:62:b2:88:a3:
    d7:7b:38:72:58:ed:9f:3b:94:0a:09:d9:59:4e:fe:
    b6:bd:ed:1a:f8:08:41:29:2f:b4:0e:62:41:8a:0f:
    25:d1:b4:39:93:a1:21:c6:9d:72:e9:cf:99:fd:9e:
    7f:fc:ef:67:dd:49:16:19:68:96:4c:97:ec:5e:43:
    96:eb:34:b9:fe:34:cf:a8:b9
prime1:
    00:e2:c3:ff:b1:2a:a0:af:c9:06:49:95:ad:9a:f9:
    8c:30:07:8b:5f:a1:b8:77:3a:10:50:31:f7:48:9a:
    45:10:db:a2:a5:67:ec:71:b0:c4:64:aa:72:58:25:
    33:ab:4b:4d:b4:d4:19:a9:e4:74:13:93:bd:9d:b5:
    77:82:ef:aa:ff
prime2:
    00:c2:94:a5:63:f1:ac:90:58:73:97:06:2b:b9:08:
    bd:a8:49:66:8a:51:42:54:f7:a1:e8:ce:4d:23:e2:
    ff:3e:ea:5a:ed:9f:d4:92:46:95:22:ee:56:08:21:
    da:14:67:72:29:88:24:a8:cf:30:a1:c8:3c:85:0d:
    54:d8:b9:00:a7
exponent1:
    00:85:c1:16:10:6a:14:5d:9b:35:1a:32:2f:34:39:
    fd:76:56:fc:de:6b:85:62:cd:dc:d1:70:a7:6b:2b:
    93:91:5d:fb:91:94:ba:e2:54:61:82:ff:06:72:f7:
    7e:82:8a:e8:4a:c0:d0:62:65:5e:12:2e:12:a9:f3:
    5f:8a:db:d3
exponent2:
    5b:b9:92:d5:b0:61:76:92:1b:bb:3f:cf:70:a4:6c:
    33:5e:96:95:f1:84:61:65:72:cf:ba:20:a7:20:24:
    ce:d5:cf:13:61:5a:e5:b1:9c:30:21:46:31:03:34:
    38:96:23:3c:ec:ce:cb:3d:10:61:d3:70:a4:10:0a:
    c5:37:40:d5
coefficient:
    13:6a:48:7c:cf:20:aa:aa:d4:ea:9e:97:68:b7:de:
    06:32:de:77:2b:b2:88:65:fe:51:1f:01:59:c5:f9:
    74:de:1c:0f:e2:9f:c0:53:29:5d:40:c8:2d:e3:93:
    1f:75:5e:d6:30:d1:9f:3b:68:0d:5a:aa:76:47:78:
    05:b0:87:c9

34. The two public key have been stored on the .rsa files and generated with the command openssl rsa -in <private_key_file> -pubout -out <public_key_file>

35. Yes we can, it's a public key so by definition everybody can know it

36. We use the command openssl rsa -in -pubin <public_key_file> -text -noout .For the first public key we get:

RSA Public-Key: (1024 bit)
Modulus:
    00:eb:18:48:6b:f0:c2:2e:3f:cc:2e:81:c1:0a:29:
    e0:e6:be:ba:49:ff:0b:c7:36:27:83:08:d1:81:78:
    ca:5b:7e:2f:67:26:c9:92:06:ae:dd:17:54:ac:a6:
    45:35:c0:bd:53:a4:76:07:ec:c1:92:b8:79:84:0d:
    48:2f:16:79:06:d6:66:67:59:9b:83:c1:9d:c4:fe:
    c1:11:75:27:4f:00:97:7e:3e:32:b5:db:96:16:16:
    0e:24:ca:db:99:d2:33:74:ae:58:67:64:40:88:af:
    99:55:11:15:36:75:70:f9:b0:16:0f:4c:92:00:38:
    bb:e1:92:ab:1d:92:34:c6:2b
Exponent: 65537 (0x10001)

and for the second:

RSA Public-Key: (1024 bit)
Modulus:
    00:ac:5c:33:94:f0:66:db:5b:32:2d:3b:06:12:4d:
    4a:eb:79:a2:13:d8:18:1f:91:8c:e7:af:85:9e:dc:
    5c:54:30:78:03:54:cc:23:58:a2:b6:a5:79:9f:9d:
    c1:96:10:3c:47:15:34:c6:44:10:a8:a8:b5:eb:a2:
    65:71:09:58:d1:9c:e9:f3:72:c2:c0:55:e3:11:88:
    07:da:12:50:0f:ea:6d:aa:a5:73:2c:f8:04:fe:88:
    de:14:ad:69:e4:0f:01:47:6f:e9:7f:f2:75:b3:4d:
    25:0b:1d:0d:f6:1f:91:f1:cd:96:87:10:9f:cf:a3:
    22:68:87:47:1c:24:9f:8c:59
Exponent: 65537 (0x10001)

39. For this pair we get:

RSA Private-Key: (4096 bit, 2 primes)
modulus:
    00:eb:b6:fe:99:35:a0:eb:8c:a1:be:4b:b4:d6:ca:
    d8:de:4c:a3:1a:41:1d:e5:2a:b3:6a:07:e0:08:12:
    d4:91:5b:b3:1c:8c:1a:41:f3:8c:c5:9f:9a:98:9f:
    c5:c6:0e:b0:71:f2:6d:a5:41:2d:ed:88:b6:ac:c3:
    e3:bb:25:04:56:da:c5:e3:47:34:3b:15:95:4b:70:
    6d:28:7e:e1:59:1b:33:a9:a1:ba:d0:d0:38:23:84:
    1a:54:53:70:93:32:ae:cd:f1:d5:c4:c0:c3:ec:2f:
    95:86:b6:c7:52:0d:41:0f:0d:c5:af:aa:68:08:c8:
    48:eb:f8:53:c8:fe:b7:a2:65:29:33:3e:23:8f:76:
    54:6a:e4:35:1f:40:dc:31:fb:44:e3:68:06:7e:fd:
    43:85:9d:10:32:23:7c:8b:a0:53:22:47:57:1c:81:
    a3:6c:97:ce:07:97:a2:76:f0:30:76:8c:58:2c:00:
    c0:ca:12:b9:46:0d:28:c4:93:cc:af:bc:d7:30:b3:
    57:27:f9:c5:62:17:d5:b0:0e:92:25:9a:65:71:bf:
    1f:ad:68:c2:f2:ac:aa:56:91:f3:06:7d:05:7b:45:
    d3:e8:cc:a0:c8:1d:66:ff:83:b8:59:27:31:a5:a0:
    25:ef:26:1b:de:c9:09:e8:6d:ec:74:2d:d1:4c:fa:
    12:89:5b:fd:b4:77:49:b7:33:3f:b5:3d:54:cc:2b:
    a2:3d:9c:5f:9b:bb:2c:2c:25:fb:9f:96:8d:91:80:
    ac:ea:f1:28:0f:da:4c:f6:61:47:71:ac:13:e8:dc:
    2a:cc:45:2d:20:a4:a1:44:40:b6:c7:01:bb:03:27:
    0e:96:2b:41:2a:9f:dc:be:43:a1:7d:8c:1b:2e:d2:
    31:80:3b:25:b7:6c:27:11:6d:41:8a:e5:1e:54:e6:
    78:98:83:9e:3a:0f:20:9e:bf:62:07:34:19:d4:c0:
    15:82:1f:b9:e7:51:6b:f2:ed:a1:12:e4:9e:90:ca:
    83:bf:2d:dc:20:ef:be:7a:20:9a:33:0c:80:4a:b8:
    83:65:31:03:1a:05:89:74:e1:82:5a:f0:2c:73:d4:
    56:74:7b:51:e7:be:09:cf:15:79:7c:4c:a1:2b:54:
    58:e8:f2:0b:35:94:82:0f:c8:17:df:ab:99:39:19:
    a2:a4:7f:75:c3:73:96:22:72:a7:8e:99:89:6f:e8:
    bb:3d:d4:64:ae:4d:16:99:59:60:a4:f6:d6:e2:22:
    22:04:cd:38:ee:b0:70:c2:7a:5d:40:71:9f:47:5d:
    81:90:c5:0f:9a:e6:a8:a3:97:05:8f:29:b8:6d:76:
    3a:54:05:c1:8b:68:a8:57:3a:ec:15:95:54:7e:c4:
    d2:47:39
publicExponent: 65537 (0x10001)
privateExponent:
    00:e2:0a:2f:66:be:ed:dd:54:94:3a:c7:a0:d8:a5:
    d7:88:7e:17:9e:f2:b8:f7:6d:96:75:e4:ad:6b:30:
    9b:95:f5:48:f2:e4:4b:bd:d9:0f:b2:e8:16:39:ac:
    65:dd:43:72:e3:22:a7:10:e5:4b:64:d1:48:d4:ae:
    59:71:ba:d2:c9:73:ac:77:02:6e:4d:06:aa:8f:98:
    75:a5:72:df:29:33:13:bc:e5:32:20:52:bc:b3:1a:
    58:a1:6b:0e:b6:a9:d0:c5:ff:d6:8e:82:88:9f:b3:
    16:02:04:bf:69:a8:fa:2d:b4:78:51:b7:e9:62:1a:
    a7:0b:5a:ad:06:3f:75:dc:0e:84:8e:f6:54:e5:9b:
    84:e2:a8:8b:64:b8:2f:30:e3:13:b1:bb:ad:17:14:
    4a:2f:14:6a:56:9a:7f:31:ea:82:2e:8a:19:38:65:
    fd:51:02:d6:79:a3:69:12:03:cd:85:b1:9a:50:40:
    3c:a0:cb:89:ce:6a:8a:20:d0:d7:65:e3:82:bc:da:
    0e:87:70:57:5f:75:19:df:66:69:b2:13:b8:9a:76:
    bc:aa:ef:9e:9b:57:07:56:53:49:a9:7c:34:c8:7d:
    32:f4:b9:5e:a9:e8:c0:3f:e1:91:e4:34:d8:c0:4a:
    ae:76:02:a6:91:63:5a:d4:00:ad:d2:cf:a9:39:d7:
    49:f3:86:13:e8:ab:52:55:c9:97:9f:36:9e:8e:a6:
    8c:d5:50:e7:6d:30:4e:f5:bd:f9:a7:fa:87:2e:2a:
    ad:be:83:83:f7:69:2b:23:d5:32:bf:84:03:5a:99:
    8f:d9:5e:f6:6e:c7:4e:51:52:cc:c8:c4:a4:d1:37:
    55:c3:b9:db:39:55:66:d5:ee:7a:79:50:b7:df:66:
    b9:6f:06:59:c2:f1:4a:6f:2a:ef:4f:ab:39:d2:d1:
    62:73:97:58:5c:71:28:8e:4a:c6:2c:e9:2d:1f:24:
    7a:3a:f4:a4:56:98:a7:36:74:7e:88:e2:b5:9d:a5:
    68:6f:dc:0e:47:35:c0:fe:a7:b6:86:47:15:34:14:
    59:a1:a1:78:d7:17:99:6d:26:b4:16:08:45:d3:34:
    ba:fe:56:23:b4:c6:9f:4e:4f:3f:96:e9:a8:19:07:
    43:01:45:3d:23:67:40:dd:1e:19:30:48:3e:d7:72:
    be:ac:68:b1:3c:86:c6:ba:6e:98:3a:b1:89:be:a9:
    e0:1d:ab:1a:a7:26:09:5e:2b:21:e6:da:63:1b:f0:
    65:2f:f4:20:ac:81:c7:d0:07:49:24:33:60:c8:86:
    fd:f3:8b:85:4c:7c:5c:34:30:12:36:57:a6:f5:de:
    b3:e5:1e:a1:5e:f2:dd:1a:98:6f:2c:df:71:66:8f:
    9e:8d:01
prime1:
    00:f7:94:ac:80:bd:d1:bc:44:3b:34:7d:b3:36:6e:
    a4:e1:0b:a8:03:35:a9:cc:0a:e1:64:00:a1:df:12:
    3f:12:6e:99:36:e4:8b:88:de:48:2f:04:ed:8b:b2:
    e7:b4:ef:54:79:97:67:f2:e6:5b:8a:a0:42:16:4b:
    e4:01:73:58:04:a8:49:da:ed:50:fb:61:e9:5e:1d:
    c8:bd:b0:1c:e5:8e:0e:83:95:75:8e:dc:9c:78:16:
    f6:77:df:f5:ec:59:c4:1b:e7:7a:55:7b:53:ad:06:
    28:20:ed:f5:c4:2a:43:13:06:4b:2c:7d:54:51:f1:
    6c:62:e7:9d:a3:07:14:94:ed:4b:34:10:f4:56:ad:
    6b:fb:56:33:45:03:1b:a1:ab:22:87:db:16:fc:e7:
    dd:ff:8f:5a:d9:10:c6:6f:b0:18:eb:35:e6:a0:3c:
    9e:eb:f9:ee:ac:8d:95:72:14:dc:aa:07:5a:6d:66:
    d5:bc:1a:36:8c:88:ad:6f:38:31:be:a9:a4:cc:72:
    d6:0f:b2:99:22:d7:d4:25:1f:d8:2d:53:e6:97:29:
    19:53:c9:81:d4:78:d2:b8:9a:34:c5:20:38:ce:03:
    4c:95:ae:00:ca:0f:f7:30:3d:8c:b1:12:31:8b:fb:
    88:52:76:71:45:79:af:ad:41:d5:93:db:66:fb:bc:
    a2:19
prime2:
    00:f3:bb:05:6e:bc:59:4b:bb:c9:3a:71:58:e9:e1:
    30:42:37:0d:0d:54:07:a1:fb:0c:7e:cc:b0:99:ac:
    75:84:8d:d7:83:db:76:42:fd:10:51:ee:18:42:98:
    bc:9e:df:b0:f1:53:8f:d3:60:3b:7d:c3:19:7d:68:
    f5:83:a1:66:d2:6c:4a:94:00:92:3d:84:13:94:9e:
    b5:b5:fc:92:f2:f5:dd:cb:6a:cf:18:ee:0e:98:39:
    e5:b0:77:8e:a0:b6:af:59:71:6b:45:59:8b:69:06:
    f8:88:2d:b2:17:b2:06:bf:a1:75:7e:30:1e:71:20:
    05:ed:0f:9e:c4:31:27:0c:81:f7:d7:dd:e3:6f:e9:
    1a:27:9f:d6:fd:86:93:e7:d4:87:66:b3:86:73:f5:
    88:e2:cc:d4:f9:c7:80:6f:78:fa:9b:a6:ee:b9:a4:
    5b:c0:05:18:4e:11:d0:18:e0:8d:ad:bd:32:11:a1:
    c3:8c:2f:60:b2:b8:83:92:04:5e:b9:c1:7b:86:b7:
    9a:3a:81:37:a2:a0:be:17:06:bb:e1:a5:88:1e:0c:
    ad:aa:e8:74:82:e3:f3:00:c2:9a:8b:b0:78:b3:9d:
    32:8b:9f:a9:ff:d1:b5:f7:ff:f8:e2:12:46:3d:04:
    76:24:0f:17:34:21:a2:54:40:6a:02:0a:cc:40:ac:
    b2:21
exponent1:
    32:ff:d1:39:03:e0:fc:63:21:bf:02:55:b4:54:be:
    6a:0d:38:a2:d0:9f:15:84:f8:40:dd:50:7a:df:0b:
    57:04:a9:53:01:69:2c:dc:7d:0a:12:7d:cc:55:22:
    1d:c7:5f:23:5c:e8:f0:c4:3a:ff:27:5b:f5:b3:d4:
    57:e8:26:ad:6e:ed:27:c7:d2:f2:b5:8c:98:f2:91:
    b3:61:5f:d2:79:9c:cb:c6:31:dd:27:dc:a3:70:37:
    7d:22:09:4a:92:83:2f:2c:53:aa:e0:28:b0:6f:a0:
    3c:88:48:24:0c:80:9c:9e:a3:45:9a:db:8f:b5:a7:
    9a:08:14:14:cd:14:94:48:fd:40:d0:6d:44:9d:9c:
    ac:bd:68:fa:42:55:a2:cb:d9:e5:1a:60:75:e0:5d:
    7e:93:4d:27:81:a9:1e:c0:ab:a1:68:0d:05:dc:f6:
    ef:ad:20:59:33:21:21:ca:e2:59:b4:02:10:9f:2a:
    50:01:26:53:74:a4:25:50:f2:0e:50:6b:be:21:b9:
    ae:aa:1f:99:69:d4:2d:81:8d:ca:68:8a:0b:81:4f:
    d2:4e:a4:0b:dc:87:ab:b7:38:3f:93:0f:a0:80:55:
    7a:4f:c9:a1:d6:00:19:53:d4:73:75:47:f7:89:ec:
    de:ef:bf:0d:8c:af:7f:85:1c:35:c4:f0:45:d2:e9:
    a1
exponent2:
    00:ed:0d:20:0a:84:dc:20:c1:a2:c4:d9:cb:4d:a5:
    fc:a6:da:1e:32:b1:60:bf:15:8a:20:a5:7a:b9:ec:
    37:5c:56:0c:9f:ad:4a:c7:7a:94:6d:eb:31:e3:4b:
    e4:60:6d:56:0e:51:64:98:84:87:38:19:12:68:9c:
    35:f5:f1:7c:2c:fa:b0:4a:64:87:d7:96:e6:e8:6c:
    b8:75:29:13:cf:84:7c:54:c5:2c:69:a5:d7:c2:0c:
    c5:ae:2d:8a:26:1b:a8:4e:48:f6:b9:3b:8f:50:f6:
    3b:b1:4f:28:60:cd:80:ac:cf:1f:94:3c:be:ce:ee:
    03:75:7e:f4:bf:02:c3:64:f1:df:99:2f:b1:6e:7d:
    fc:37:3f:c9:fd:48:ef:ff:d2:00:63:4e:06:5e:30:
    18:13:e6:7e:d9:f5:44:a0:a6:bc:af:b8:db:84:e9:
    2b:67:86:4d:0e:59:5a:d9:67:45:c5:0e:37:6d:58:
    b4:f5:5a:7b:25:38:3a:97:40:f2:d4:9e:2f:43:66:
    bd:75:5b:8a:3d:fd:33:04:f8:42:b2:03:aa:70:f7:
    89:cd:a9:4a:f0:a7:3e:08:a6:3d:63:d1:9e:83:ab:
    47:a6:ef:02:8b:d2:6e:b6:a7:5a:6c:9e:b3:7b:d3:
    24:08:8c:38:d8:36:42:48:c9:61:91:5c:e9:2d:9a:
    09:a1
coefficient:
    1a:7f:4a:39:66:f8:d2:6e:bf:cc:1c:73:94:54:e7:
    4d:c6:c0:52:0f:c9:2a:f9:72:c7:b1:d3:34:1b:f7:
    6b:bb:91:55:9d:7b:0c:99:aa:96:63:68:2a:71:7d:
    f5:46:5c:37:ce:27:69:e9:e9:71:11:55:f2:93:c4:
    01:f1:f2:41:cc:35:27:7a:5c:59:13:5a:de:47:9c:
    46:ed:68:b3:47:09:bb:89:87:d8:d0:bc:26:d7:03:
    84:6a:54:19:07:64:59:f6:91:5d:09:2d:64:6c:c6:
    8a:73:0c:42:7c:e8:3c:e8:a2:77:d7:5f:14:50:4c:
    84:30:4d:b0:41:3d:4a:b9:d0:0e:f3:3e:9d:ac:3e:
    33:15:f0:9e:65:1e:57:24:81:52:78:c1:2b:40:71:
    c7:d4:d0:ff:07:ca:3b:84:05:9a:1b:ee:be:2e:58:
    0e:32:e4:4e:b8:43:43:1b:30:ea:f4:99:ca:ef:19:
    e0:7e:fd:3d:0c:06:51:cb:d2:ef:b6:47:90:91:e9:
    db:25:2c:93:25:03:31:89:56:10:82:16:a6:27:76:
    82:13:ee:6e:2c:89:a7:40:d1:d5:7f:8f:2d:0c:bc:
    9a:e6:32:d3:ba:f7:02:9e:e2:a7:23:20:a2:fc:23:
    47:e2:80:6a:32:ba:e8:73:8a:53:c7:17:90:91:b8:
    bd

37. openssl genrsa -out private4096.pem 4096

38. openssl rsa -in private4096.pem -pubout -out public4096.rsa

39. openssl rsa -in private4096.pem -text -noout

40. The encrypted private key is stored in the file private4096_enc.bin and the password is 123456789 (just for the example, in the reality passwords must be stronger than this one) with the command openssl enc -aes-256 -e -in private4096.pem -out private4096_enc.bin

Task 6

Balbal in folder task6

41. To do so we use the command: openssl ecparam -name <ecurvename> -out <outfile> -genkey

42. Yes I can view my key because it's not encrypted

43. We use the command openssl enc -des3 -e -in <file> -out <enc_file_name>

44. We use the following command for each previously generated keys: openssl ec -in <file_name> -out <publ_file_name> -pubout

Task 7

Blabla in folder task7

45. We use the file small_file.txt and a the key public4096.rsa generated in the Task 5 section
The encrypt command is openssl rsautl -encrypt -in small_file.txt -inkey public4096.rsa -pubin -out small_file_enc.txt
the decrypt command is openssl rsautl -decrypt -in small_file_enc -inkey private4096.pem -out small_file_dec.txt

46. We re use the file ciphertext.txt for this exercice, and we use the command openssl rsautl -encrypt -in ciphertext.txt -inkey public4096.txt -pubin -out ciphertext_enc.txt. Openssl can't encrypt the file because it's too large for 
the key size. It happens because RSA is a block encryption algorithm and the key must be greater than the data to encrypt.

47. To do that we use the command echo -n "I'm a very very very very very very very very long message" | openssl enc -aes-128-ofb and we give the password 123456789. Then we encrypt this password with rsa with the command 
echo -n "123456789" | openssl rsautl -inkey public4096 -pubin and we get the encrypted password. Then to decrypt the file we first need to decrypt the password with our private key and then decrypt the message with the decrypted
paswword

48&49. Same as 45 but with the image pic_original.bmp

Task 8

50. We should use the private key for a signature, because anybody can use our public key. So to make sure of the authenticity of the signature, the private key must be used.

51. First we generate a hash for our message with echo -n 'Message to sign' | openssl dgst -sha512 > message_hash.txt and then we sign it with the command  openssl rsautl -sign -in message_hash.tx -inkey private4096.pem > signature.txt

52. Then we check the signature by getting the signed message hash with the command openssl rsautl -verify -in signature.txt -pubin -inkey public4096.rsa > checked_hash.txt, and then by using the diff tool we compare the two hash files.
Here they are the same so the signature is checked succesfully !

53. If the hashes we get with the openssl rsautl -verify command aren't the same as the originals ones, they were modified

Task 9

Blabla task9

54. we use the command openssl req -new -key private4096.pem -out user-request.pem

55. We use the openssl x509 -in user-request.pem -text -noout command but we can't visualize this because the request haven't be signed by a certification authority

56. We generate a pair of key using RSA (has we have done before) called ca_rsa_private.pem/ca_rsa_pub.pem and create a certificate request

57. We autosign the certificate with the command openssl x509 -req -in ca-request.pem -signkey private-ca.pem -out ca-certificate.pem

58. We vizualise the certificate using the command openssl x509 -in ca-certificate.pem -text -noout and we get:

Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            46:d9:db:b2:80:80:76:c0:1e:93:eb:89:cf:55:0d:13:84:17:ce:4a
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = AU, ST = Some-State, O = Internet Widgits Pty Ltd
        Validity
            Not Before: Nov 16 18:37:58 2021 GMT
            Not After : Dec 16 18:37:58 2021 GMT
        Subject: C = AU, ST = Some-State, O = Internet Widgits Pty Ltd
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (1024 bit)
                Modulus:
                    00:c5:89:c7:49:c7:7d:4d:97:72:cd:d9:14:1d:e3:
                    06:cb:12:ee:53:7d:38:53:b4:b7:a8:4c:c1:0d:06:
                    05:6c:cd:5d:53:83:3c:0a:ab:eb:3d:f6:b5:52:07:
                    eb:ec:4e:44:b2:40:df:88:5e:65:e5:34:55:75:95:
                    84:84:ae:83:59:e0:ec:5f:ce:95:7c:d4:ee:3c:ed:
                    78:e0:e4:45:2b:38:fd:31:d0:7b:a7:11:81:c7:e8:
                    72:2a:a8:75:7c:4d:d6:c1:50:1d:af:18:2e:3e:ec:
                    27:08:c5:2a:54:e5:d0:d9:55:38:30:49:ea:db:6e:
                    0f:14:71:15:51:ab:10:c1:39
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         53:43:88:90:34:b6:00:ba:d2:5d:ae:a8:83:74:48:ab:0a:bf:
         93:77:bf:9b:03:e6:db:f0:6f:05:48:07:c8:97:a2:7b:45:9c:
         30:b6:2a:db:b5:e4:cb:f4:68:9f:fb:3e:0b:93:79:fa:8d:91:
         97:77:45:b4:f2:88:98:00:8a:9e:c5:9f:20:d9:1c:02:67:49:
         14:3f:92:44:d4:fc:6a:6f:e6:8f:28:d1:f4:e4:8f:d3:b8:51:
         20:8f:93:f1:c8:3e:bd:3d:da:13:17:72:3a:6f:66:0b:f6:52:
         a0:2a:d3:2a:fc:2e:8a:c6:1b:78:9e:8c:69:fd:c0:86:7b:ad:
         58:ad

59. Then we sign the certificate request user-request.pem with the command openssl x509 -days 15 -CAserial serial.txt -CA ca-certificate.pem -CAkey ca_rsa_private.key -in user-request.pem -res -out user-certificate.pem

60. we verify it with the command openssl verify -CAfile ca-certificate.pem user-certificate.pem

61. If we try to change the user certificate file, the verfication process will detect it because the hashes aren't the same anymore, and so the certificate won't be accepted

