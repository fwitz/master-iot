import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage-angular';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
//import jwt_decode from "jwt-decode";

import * as models from '../../models';

import network from '../../../network-config.json'
import { Location } from '../../models';
import { UsersPage } from 'src/app/pages/users/users.page';

export const USER_INFOS = 'user_infos';

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {
  private _storage: Storage | null = null;
  private apiUrl: string = "http://".concat(network.ip, ":", network.port)

  private authHeaders = new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Headers': 'Content-Type', 'Access-Control-Allow-Origin': '*', 'Authorization': ""})
  private anonymousHeaders = new HttpHeaders({'Content-Type': 'application/json', 'Accept': 'application/json', 'Access-Control-Allow-Headers': 'Content-Type', 'Access-Control-Allow-Origin': '*'})


  constructor(private storage: Storage, private httpClient: HttpClient, private router: Router, private platform: Platform) { 
    this.init()
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
  }

  /* Method that retrieve the user auth token in local storage */
  public getToken():  Promise<string>{
    let promise = new Promise<string>((resolve) => {
      this._storage?.get(USER_INFOS).then(res => {
        if(res){
          resolve(res['auth_token'])
        }
      })
    })
    return promise
  }

  /* Method that retrieve the user id in local storage */
  public getID():  Promise<number>{
    let promise = new Promise<number>((resolve) => {
      this._storage?.get(USER_INFOS).then(res => {
        if(res){
          resolve(res['user_id'])
        }
      })
    })
    return promise
  }

  /* Method that retrieve the user admin state in local storage */
  public isAdmin(): Promise<boolean>{
    let promise = new Promise<boolean>((resolve) => {
      this._storage?.get(USER_INFOS).then(res => {
        if(res){
          resolve(res['is_admin'])
        }
      })
    })
    return promise
  }

  /* Method that make a request to the api to check auth token validity */
  public async isTokenValid(): Promise<boolean>{
    let promise = new Promise<boolean>((resolve) => {
      this._storage?.get(USER_INFOS).then(res => {
        if(res){
            this.httpClient.post(this.apiUrl.concat('/auth/check'), JSON.stringify({}) , { headers: this.authHeaders.set("Authorization", res['auth_token'])}).toPromise().then(
              (res: any) => {
                console.log(res["code"])
                if(res["code"] === 200){
                  resolve(true)
                }else{
                  this._storage?.remove(USER_INFOS)
                  this.router.navigate(['/login']);
                  resolve(false)
                }
              }
            ).catch(e =>{
              this._storage?.remove(USER_INFOS)
              this.router.navigateByUrl('/login', { replaceUrl: true });
              resolve(false)
            })
          }
      })})
    return promise
  }

  /* Method to make a request to log in a user */
  public async loginRequest(auth_infos: models.UserPayload): Promise<string | Error>{
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.post(this.apiUrl.concat('/auth/login'), auth_infos, {headers: this.anonymousHeaders}).toPromise().then(
        (res: any) => {
            var user: models.UserLogged = new models.UserLogged(res.data.data.auth_token, res.data.data.is_admin, res.data.data.user_id)
            delete res['data']
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            this._storage?.set(USER_INFOS, user)
            resolve(msg.toString())
          }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to make a request to log out a user */
  public async logoutRequest(): Promise<string | Error>{
    let token: string =  await this.getToken();
      let promise = new Promise<string | Error>((resolve) => {
        this.httpClient.post(this.apiUrl.concat('/auth/logout'), JSON.stringify('{}'), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
          (res: any) => {
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            this._storage?.remove(USER_INFOS)
            resolve(msg.toString())
          }
        ).catch(e => {
          var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(Error(msg.toString()))
        })
      })
      return promise
  }

  /* Method to return the user authentication state */
  public async isAuthenticated(): Promise<boolean> {
      let state: boolean = await this.isTokenValid();
      return state
  }
  
  /* Method that retrieves all the existing locations */
  public async getLocations(): Promise<Array<models.Location> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<models.Location> | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/location/'), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let locations: Array<models.Location> = []
          res['data']['data'].forEach(obj => {
            locations.push(new models.Location(obj.id, obj.name, obj.indoor, obj.longitude, obj.latitude))
          })
          resolve(locations)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to create a new location */
  public async newLocation(payload: models.LocationPayload): Promise<string | Error>{
    let token: string =  await this.getToken();
      let promise = new Promise<string | Error>((resolve) => {
        this.httpClient.post(this.apiUrl.concat('/location'), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
          (res: any) => {
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            resolve(msg.toString())
          }
        ).catch(e => {
          var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(Error(msg.toString()))
        })
      })
      return promise
  }

  /* Method to get a specific location */
  public async getLocation(locId: number): Promise<Location | Error> {
    let token: string =  await this.getToken();
    let promise = new Promise<models.Location | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/location/', locId.toString()), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let location: models.Location = new models.Location(res.data.data.id, res.data.data.name, res.data.data.indoor, res.data.data.longitude, res.data.data.latitude)
          resolve(location)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to update a specific location */
  public async updateLocation(locId: number, payload: models.LocationPayload): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.patch(this.apiUrl.concat('/location/', locId.toString()), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to delete a specific location */
  public async deleteLocation(locId: number): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.delete(this.apiUrl.concat('/location/', locId.toString()), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to get location in range of a specific user */
  public async getReachableLoc(userId: number): Promise<Array<models.Location> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<models.Location> | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/location/reach/', userId.toString()), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let locations: Array<models.Location> = []
          res['data']['data'].forEach(obj => {
            locations.push(new models.Location(obj.id, obj.name, obj.indoor, obj.longitude, obj.latitude))
          })
          resolve(locations)
        }
      ).catch(e => {
        console.log(e)
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method that retrieves all users */
  public async getUsers(): Promise<Array<models.User> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<models.User> | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/user'), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let users: Array<models.User> = []
          res['data']['data'].forEach(obj => {
            users.push(new models.User(obj.id, obj.username))
          })
          resolve(users)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  public async getReachableUsers(userId: number): Promise<Array<models.User> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<models.User> | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/user/reachable/', userId.toString()), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let users: Array<models.User> = []
          res['data']['data'].forEach(obj => {
            users.push(new models.User(obj.id, obj.username))
          })
          resolve(users)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to create a new user */
  public async newUser(payload: models.UserPayload): Promise<string | Error>{
    let token: string =  await this.getToken();
      let promise = new Promise<string | Error>((resolve) => {
        this.httpClient.post(this.apiUrl.concat('/user'), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
          (res: any) => {
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            resolve(msg.toString())
          }
        ).catch(e => {
          var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(Error(msg.toString()))
        })
      })
      return promise
  }

  /* Method to get a specific user */
  public async getUser(userId: number): Promise<models.User | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<models.User | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/user/', userId.toString()), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let user: models.User = new models.User(res.data.data.id, res.data.data.username)
          resolve(user)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to update a specific user */
  public async updateUser(userId: number, payload: models.UserUpdate): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.patch(this.apiUrl.concat('/user/', userId.toString()), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to delete a specific user */
  public async deleteUser(userId: number): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.delete(this.apiUrl.concat('/user/', userId.toString()), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage() 
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to get the actual position of a user */
  public async getUserPosition(userId: number): Promise<models.Location | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<models.Location | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/user/', userId.toString(), '/position'), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let location: models.Location = new models.Location(res.data.data.id, res.data.data.name, res.data.data.indoor, res.data.data.longitude, res.data.data.latitude)
          resolve(location)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.code, e.title, e.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to update the position of a user */
  public async updateUserPosition(userId: number, locId: number): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.patch(this.apiUrl.concat('/user/', userId.toString(), '/position'), JSON.stringify({'location_id': locId}), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to create a meeting party */
  public async createMeet(payload: models.MeetPayload): Promise<string | Error>{
    let token: string =  await this.getToken();
      let promise = new Promise<string | Error>((resolve) => {
        this.httpClient.post(this.apiUrl.concat('/meet/'), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
          (res: any) => {
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            resolve(msg.toString())
          }
        ).catch(e => {
          var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(Error(msg.toString()))
        })
      })
      return promise
  }

  /* Method to check if a user is involved in a meeting party */
  public async checkUserParticipation(userID: number): Promise<string | models.Meet | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | models.Meet | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/meet/search/', userID.toString()), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          if(Object.keys(res.data).length === 0){
            resolve("You're not involved in any meet party, try to create one")
          }else{
            let meet: models.Meet = new models.Meet(res.data.data.users, res.data.data.destination, res.data.data.session_uuid)
            resolve(meet)
          }
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to update specific user's infos on a specific meeting party */
  public async updateMeetUser(sessionUUID: string, userID: number, payload: models.UserMeetUpdate): Promise<string | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.patch(this.apiUrl.concat('/meet/', sessionUUID, '/', userID.toString()), JSON.stringify(payload), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to delete a specific user's from a specific meeting party */
  public async deleteMeetUser(sessionUUID: string, userID: number): Promise<string | Error> {
    let token: string =  await this.getToken();
    let promise = new Promise<string | Error>((resolve) => {
      this.httpClient.delete(this.apiUrl.concat('/meet/', sessionUUID, '/', userID.toString()), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          var response: models.Response = new models.Response(res.code, res.title, res.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(msg.toString())
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage() 
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to get the itinerary of a user from a specific meeting party */
  public async getUserItinerary(sessionUUID: string, userID: number): Promise<Array<string> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<string>  | Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/meet/', sessionUUID, '/', userID.toString()), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let itinerary: Array<string>  = res.data.data.itinerary
          resolve(itinerary)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

  /* Method to set a user checkpoint */
  public async setUserCheckpoint(sessionUUID: string, userID: number, checkpoint: number): Promise<string | Error>{
    let token: string =  await this.getToken();
      let promise = new Promise<string | Error>((resolve) => {
        this.httpClient.patch(this.apiUrl.concat('/meet/', sessionUUID, '/', userID.toString(), '/checkpoint'), JSON.stringify({"checkpoint_id": checkpoint}), {headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
          (res: any) => {
            var response: models.Response = new models.Response(res.code, res.title, res.details)
            var msg: models.ResponseMessage = response.getResponseMessage()
            resolve(msg.toString())
          }
        ).catch(e => {
          var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
          var msg: models.ResponseMessage = response.getResponseMessage()
          resolve(Error(msg.toString()))
        })
      })
      return promise
  }

  /* Method to get the status of a meeting party */
  public async getMeetStatus(sessionUUID: string): Promise<Array<models.MeetStatus> | Error>{
    let token: string =  await this.getToken();
    let promise = new Promise<Array<models.MeetStatus>| Error>((resolve) => {
      this.httpClient.get(this.apiUrl.concat('/meet/', sessionUUID, '/status'), { headers: this.authHeaders.set("Authorization", token)}).toPromise().then(
        (res: any) => {
          let status: Array<models.MeetStatus> = []
          res['data']['data'].forEach(obj => {
            status.push(new models.MeetStatus(obj.user_id, obj.status, obj.checkpoint, obj.time_left))
          })
          resolve(status)
        }
      ).catch(e => {
        var response: models.Response = new models.Response(e.error.code, e.error.title, e.error.details)
        var msg: models.ResponseMessage = response.getResponseMessage()
        resolve(Error(msg.toString()))
      })
    })
    return promise
  }

}