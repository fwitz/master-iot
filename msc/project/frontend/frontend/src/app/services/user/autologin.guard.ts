import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanLoad, RouterStateSnapshot, UrlTree, Router, Route } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiRequestService } from '../api/api-request.service';

@Injectable({
  providedIn: 'root'
})
export class AutoLoginGuard implements CanLoad {

  constructor(public api: ApiRequestService, private router: Router){}

  canLoad(route: Route){
    const state = new Observable<boolean>((observer) => {
      this.api.isAuthenticated().then((res: boolean) => {
        if(res){
          observer.next(false)
          this.router.navigate(['/index/meet']);;
        }else{
          observer.next(true)
        }
      })
    })
    return state
  }
}
