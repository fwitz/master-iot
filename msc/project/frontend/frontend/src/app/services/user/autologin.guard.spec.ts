import { TestBed } from '@angular/core/testing';

import { AutoLoginGuard } from './autologin.guard';

describe('AuthGuard', () => {
  let guard: AutoLoginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AutoLoginGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
