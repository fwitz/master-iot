import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanLoad, RouterStateSnapshot, UrlTree, Router, Route } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { ApiRequestService } from '../api/api-request.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(public api: ApiRequestService, private router: Router){}

  canLoad(): Observable<boolean> | boolean{
     /* const state = new Observable<boolean>((observer) => {
        this.api.isAuthenticated().then((res: boolean) => {
          console.log(res)
          if(res){
            observer.next(true)
            observer.complete()
          }else{
            observer.next(false)
            console.log("Login again")
            this.router.navigate(['']);
          }
        })
      })
      return state*/
      return true
  }
}
