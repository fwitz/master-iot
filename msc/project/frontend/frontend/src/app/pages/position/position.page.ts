import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiRequestService } from 'src/app/services/api/api-request.service';
import { AlertController, LoadingController } from '@ionic/angular';

import { StatusPage } from 'src/app/component/status/status.page'

import * as models from '../../models';

@Component({
  selector: 'app-position',
  templateUrl: './position.page.html',
  styleUrls: ['./position.page.scss'],
})
export class PositionPage implements OnInit {

  private showUpdateButton: boolean = true
  private userLoc: models.Location
  private selectedLoc: number
  private locations: Array<models.Location> = []

  constructor( 
    private api: ApiRequestService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController,) { }

  ngOnInit() {
    this.getUserLoc()
    this.getLocations()
  }

  public showUpdateForm(){
    this.showUpdateButton = !this.showUpdateButton
  }

  public async getUserLoc(){
    let userID = await this.api.getID()
    let result = await this.api.getUserPosition(userID)
    if(result instanceof Error){

    }else{
      this.userLoc = result
    }
  }

  public async updateLoc(){
    const loading = await this.loadingController.create();
    await loading.present();
    let userID = await this.api.getID()
    var meetPromise = await this.api.updateUserPosition(userID, this.selectedLoc)
    if (meetPromise instanceof Error) {
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: 'Operation failed',
        message: "Something wrong happened...",
        buttons: ['OK'],
      });
      await alert.present();
    } else {

      await loading.dismiss()
    }
  }

  async getLocations(){
    let user_id = await this.api.getID()
    let locs = await this.api.getLocations()
    if(locs instanceof Error){

    }else{
      this.locations = locs
    }
  }
}
