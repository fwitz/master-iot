import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from '../../services/user/auth.guard';

import { UsernavPage } from './usernav.page';

const routes: Routes = [
  {
    path: '',
    component: UsernavPage,
    children: [
      {
        path: 'meet',
        loadChildren: () => import('../meet/meet.module').then(m => m.MeetPageModule),
        canLoad: [AuthGuard]
      },
      {
        path: 'position',
        loadChildren: () => import('../position/position.module').then(m => m.PositionPageModule),
        canLoad: [AuthGuard]
      },
      {
        path: '',
        redirectTo: '/index/meet',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/index/meet',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsernavPageRoutingModule {}
