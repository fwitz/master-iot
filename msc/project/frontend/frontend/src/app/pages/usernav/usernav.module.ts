import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsernavPageRoutingModule } from './usernav-routing.module';

import { UsernavPage } from './usernav.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsernavPageRoutingModule
  ],
  declarations: [UsernavPage]
})
export class UsernavPageModule {}
