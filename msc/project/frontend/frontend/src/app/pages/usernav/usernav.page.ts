import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiRequestService } from 'src/app/services/api/api-request.service';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-usernav',
  templateUrl: './usernav.page.html',
  styleUrls: ['./usernav.page.scss'],
})
export class UsernavPage implements OnInit {

  private isAdmin: boolean 

  constructor(
    private api: ApiRequestService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController) { }

  ngOnInit() {
    this.checkAdmin()
  }

  openAdmin(){
    this.router.navigateByUrl('/admin', { replaceUrl: true });
  }

  async checkAdmin(){
    let admin = await this.api.isAdmin()
    this.isAdmin = admin
  }

  async logout() {
    const loading = await this.loadingController.create();
    await loading.present();
    var logoutPromise: string| Error = await this.api.logoutRequest()
    if (logoutPromise instanceof Error) {
      const alert = await this.alertController.create({
        header: 'Logout failed',
        message: "Logout failed for unknown reason",
        buttons: ['OK'],
      });
      await alert.present();
    } else {
      await loading.dismiss();        
        this.router.navigateByUrl('/login', { replaceUrl: true });
    }
  }

  openProfile(){
    this.router.navigateByUrl('/user', { replaceUrl: true });
  }

}
