import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ApiRequestService } from 'src/app/services/api/api-request.service';
import { AlertController, LoadingController, ModalController } from '@ionic/angular';

import { StatusPage } from 'src/app/component/status/status.page'

import * as models from '../../models';

@Component({
  selector: 'app-meet',
  templateUrl: './meet.page.html',
  styleUrls: ['./meet.page.scss'],
})
export class MeetPage implements OnInit {

  private isParty: boolean = false
  private partyCreator: boolean = false
  private meetStatus: string
  private displayStatus: string
  private noPartyMsg: string
  private displaySessionUUID: string
  private sessionUUID: string
  private status: Array<models.MeetStatus>
  private showNewForm: boolean = false
  private showStatus: boolean = false
  private showNewButton: boolean
  private reachableUsers: Array<models.User>
  private reachableLoc: Array<models.Location>
  private locations: Array<models.Location>
  private selectedUsers: Array<number>
  private selectedLoc: number
  private itinerary: Array<string> = []
  private lastCheckpoint: string
  private timeLeft: string
  private destinationDisplay: string
  private destinationID: number
  private showItinerary: boolean
  private showStart: boolean
  private actualCheckpoint: number
  private friends: Array<models.MeetStatus> = []

  constructor( 
    private api: ApiRequestService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController,
    public modalController: ModalController
  ) { }

  async ngOnInit() {
    await this.checkParticipation()
    await this.getReachableUser()
    await this.getReachableLoc()
  }

  async checkParticipation(){
    let userId: number = await this.api.getID();
    let result = await this.api.checkUserParticipation(userId)
    if(result instanceof (Error)){
      this.isParty = false
      this.noPartyMsg = "Oops something went wrong..."
    }else if(result instanceof models.Meet){
      this.isParty = true
      this.displaySessionUUID = result.getDisplaySessionUUID()
      this.sessionUUID = result.getSessionUUID()
      this.destinationID = result.getDestination()
      if(this.partyCreator == true){
        await this.api.updateMeetUser(this.sessionUUID, userId, new models.UserMeetUpdate("joined"))
      }
      let tmpStatus = await this.api.getMeetStatus(this.sessionUUID)
      if(tmpStatus instanceof Error){
        
      }else{
        this.status = tmpStatus
        tmpStatus.forEach(async obj => {
          if(obj.getID() == userId){
            this.meetStatus = obj.getStatus()
            if(this.meetStatus != 'waiting'){
              this.showNewButton = false
              await this.getItinerary(this.sessionUUID, userId)
              await this.getUserStatus()
            }else if(this.meetStatus == 'waiting'){
              this.showNewButton = false
            }else{
              this.showNewButton = true
            }
          }else{
            this.friends.push(new models.MeetStatus(obj.getID(), obj.getStatus(), obj.getCheckpoint(), obj.getTimeLeft(), this.formatTime(Number(obj.getTimeLeft().toFixed(2)))))
          }
        })
      }
    }else{
      this.isParty = false
      this.showNewButton = true
      this.showStatus = false
      this.noPartyMsg = result
    }
  }

  async createNew(){
    const loading = await this.loadingController.create();
    await loading.present();
    let user_id = await this.api.getID()
    this.selectedUsers.push(user_id)
    var meetPromise: string| Error = await this.api.createMeet(new models.MeetPayload(this.selectedUsers, this.selectedLoc))
    if (meetPromise instanceof Error) {
      this.selectedUsers = []
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: 'Meeting creation failed',
        message: "Check users and destination. If you already are in a meeting, try to quit it.",
        buttons: ['OK'],
      });

      await alert.present();
    } else {
      this.showNewForm = false
      this.showNewButton = false
      this.partyCreator = true
      this.checkParticipation()
      await loading.dismiss()
    }
  }

  async getReachableUser(){
    let user_id = await this.api.getID()
    let users = await this.api.getReachableUsers(user_id)
    if(users instanceof Error){

    }else{
      this.reachableUsers = users
    }
  }

  async getReachableLoc(){
    let user_id = await this.api.getID()
    let locs = await this.api.getReachableLoc(user_id)
    if(locs instanceof Error){

    }else{
      this.reachableLoc = locs
    }
  }

  async getItinerary(sessionUUID: string, userID: number){
    let itinerary = await this.api.getUserItinerary(sessionUUID, userID)
    if(itinerary instanceof Error){

    }else{
      let userPos= await this.api.getUserPosition(userID)
      if(userPos instanceof Error){
      }else{
        let origin = userPos.getID()
        let locs = await this.api.getLocations()
        if(locs instanceof Error){

        }else{
          this.locations = locs
          let status: models.MeetStatus
          this.status.forEach(obj => {
            if(obj.getID() == userID){
              status = obj
            }
          })
          origin = userPos.getID()
          if(!itinerary.includes(origin.toString())){
            itinerary.splice(0, 0, origin.toString())
          }
          itinerary.forEach(step =>{
            this.locations.forEach(obj => {
              if(obj.getID() == Number(step)){
                if(!this.itinerary.includes(obj.getName())){
                  this.itinerary.push(obj.getName())
                }
              }
            })
          })
        }
      }
    }
  }

  async getUserStatus(){
    let userID: number = await this.api.getID();
    let status: models.MeetStatus
    this.status.forEach(obj => {
      if(obj.getID() == userID){
        status = obj
      }
    })
    this.actualCheckpoint = status.getCheckpoint()
    this.lastCheckpoint = this.itinerary[status.getCheckpoint()]
    this.destinationDisplay = this.itinerary[this.itinerary.length - 1]
    let timer = Number(status.getTimeLeft().toFixed(2))
    this.timeLeft = this.formatTime(timer)
    if(this.itinerary[status.getCheckpoint()] == this.itinerary[this.itinerary.length - 1]){
      await this.api.updateMeetUser(this.sessionUUID, userID, new models.UserMeetUpdate("finished"))
      let tmpStatus = await this.api.getMeetStatus(this.sessionUUID)
      if(tmpStatus instanceof Error){
        
      }else{
        this.status = tmpStatus
        tmpStatus.forEach(obj => {
          if(obj.getID() == userID){
            this.meetStatus = obj.getStatus()
          }
        })
      }
    }
    this.updateStatusDisplay()
  }

  async leave(){
    const loading = await this.loadingController.create();
    await loading.present();
    let userID = await this.api.getID()
    var meetPromise: string| Error = await this.api.deleteMeetUser(this.sessionUUID, userID)
    if (meetPromise instanceof Error) {
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: 'Operation failed',
        message: "Something wrong happened...",
        buttons: ['OK'],
      });
      await alert.present();
    } else {
      this.showNewForm = false
      this.showNewButton = true
      this.showStatus = false
      await this.checkParticipation()
      await loading.dismiss()
    }
  }

  async accept(){
    const loading = await this.loadingController.create();
    await loading.present();
    let userID = await this.api.getID()
    var meetPromise: string| Error = await this.api.updateMeetUser(this.sessionUUID, userID, new models.UserMeetUpdate("joined"))
    if (meetPromise instanceof Error) {
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: 'Operation failed',
        message: "Something wrong happened...",
        buttons: ['OK'],
      });
      await alert.present();
    } else {
      this.showNewForm = false
      this.showNewButton = false
      this.showStatus = true
      await this.checkParticipation()
      await loading.dismiss()
    }
  }

  async start(){
    let userID = await this.api.getID()
    await this.api.updateMeetUser(this.sessionUUID, userID, new models.UserMeetUpdate("begun"))
    this.showItinerary = true
    this.showStart = false
    //await this.checkParticipation()
  }

  async nextCheckpoint(){
    let userID = await this.api.getID()
    await this.api.setUserCheckpoint(this.sessionUUID, userID, this.actualCheckpoint+1)
    this.actualCheckpoint++
    await this.checkParticipation()
  }

  async openFriendsModal(){
    const modal = await this.modalController.create({
      component: StatusPage,
      componentProps: {
        friends: this.friends
      }
    });
    return await modal.present();
  }

  updateStatusDisplay(){
    switch(this.meetStatus){
      case 'joined':
        this.displayStatus = "Starting..."
        this.showItinerary = false
        this.showStart = true
        break
      case 'finished':
        this.displayStatus = "Destination reached"
        this.showItinerary = false
        this.showStart = false
        this.timeLeft = "00:00:00"
        break
      case 'begun':
        this.displayStatus = "On the road !"
        this.showItinerary = true
        this.showStart = false
        break
      default:
        this.displayStatus = "Loading..."
        break
    }
  }

  formatTime(timer: number): string{
    let sec = timer.toString().split('.', 2)[1].substring(0,2)
    sec = Math.round((Number('0.'.concat(sec)) * 60)).toString()
    let min = timer.toString().split('.', 2)[0]
    let hour = ((Number(min) - (Number(min) % 60))/60).toString()
    min = (Number(min) - Number(hour)*60).toString()
    return (hour.length < 2 ? '0'.concat(hour) : hour).concat(
      ':',
      min.length < 2 ? '0'.concat(min) : min, 
      ':', 
      sec.length < 2 ? '0'.concat(sec) : sec
    )
  }

  refresh(event){
    this.checkParticipation()
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  showCreationForm(){
    this.showNewForm = !this.showNewForm
    this.showNewButton = !this.showNewButton
  }

  showMeetStatus(){
    this.showStatus = !this.showStatus
  }

  openMeetStatus(){
    this.showMeetStatus()
    this.getUserStatus()
  }

}
