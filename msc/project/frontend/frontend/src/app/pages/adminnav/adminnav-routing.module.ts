import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminnavPage } from './adminnav.page';

const routes: Routes = [
  {
    path: '',
    component: AdminnavPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminnavPageRoutingModule {}
