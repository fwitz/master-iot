import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminnavPageRoutingModule } from './adminnav-routing.module';

import { AdminnavPage } from './adminnav.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminnavPageRoutingModule
  ],
  declarations: [AdminnavPage]
})
export class AdminnavPageModule {}
