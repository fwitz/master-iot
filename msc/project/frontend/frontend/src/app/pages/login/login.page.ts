import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AlertController, LoadingController } from '@ionic/angular';

import { ApiRequestService } from 'src/app/services/api/api-request.service';

import * as models from '../../models';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials: FormGroup;

  constructor(
    private fb: FormBuilder,
    private api: ApiRequestService,
    private alertController: AlertController,
    private router: Router,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.credentials = this.fb.group({
      username: [],
      password: [],
    });
  }
 
  async login() {
    const loading = await this.loadingController.create();
    await loading.present();
    var loginPromise: string| Error = await this.api.loginRequest(new models.UserPayload(this.credentials.get("username").value, this.credentials.get("password").value))
    if (loginPromise instanceof Error) {
      await loading.dismiss();
      const alert = await this.alertController.create({
        header: 'L\'authentification a échouée',
        message: "Nom d'utilisateur ou mot de passe incorrect",
        buttons: ['OK'],
      });

      await alert.present();
    } else {
      await loading.dismiss();        
      this.router.navigateByUrl('/index/meet', { replaceUrl: true });
    }
  }
 
  // Easy access for form fields
  get username() {
    return this.credentials.get('username');
  }
  
  get password() {
    return this.credentials.get('password');
  }

}
