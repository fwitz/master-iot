export class MeetPayload {
    /**
     * Class encapsulating meeting creation informations1
     */
    private users: Array<number>
    private destination: number

    constructor(users: Array<number>, destination: number){
        this.users = users
        this.destination = destination
    }
}

export class Meet{
    /**
     * Class encapsulating meeting informations
     */
    private users: Array<number>
    private destination: number
    private session_uuid: string

    constructor(users: Array<number>, destination: number, session_uuid: string){
        this.users = users
        this.destination = destination
        this.session_uuid = session_uuid
    }

    public getDisplaySessionUUID(): string{
        return this.session_uuid.substring(0, 5)
    }

    public getSessionUUID(): string{
        return this.session_uuid
    }

    public getDestination(): number{
        return this.destination;
    }
}

export class UserMeetUpdate{
    /**
     * Class encapsulating to update meet users' informations
    */

    private status: string

    constructor(status?: string){
        this.status = status
    }

}

export class MeetStatus{
    /**
     * Class encapsulating user's meet status informations
     */
    private userID: number
    private status: string
    private checkpoint: number
    private timeLeft: number
    private timeString: string

    constructor(userID: number, status: string, checkpoint: number, timeLeft: number, timeString?: string){
        this.userID = userID
        this.status = status
        this.checkpoint = checkpoint
        this.timeLeft = timeLeft
        this.timeString = timeString
    }

    public getStatus(): string{
        return this.status
    }

    public getID(): number{
        return this.userID
    }

    public getCheckpoint(): number{
        return this.checkpoint
    }

    public getTimeLeft(): number{
        return this.timeLeft
    }
}



