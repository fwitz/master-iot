export class UserLogged {
    /**
     * Class encapsulating user's logged informations
     */
    private auth_token: string
    private is_admin: boolean
    private user_id: number

    constructor(auth_token: string, is_admin: boolean, user_id: number){
        this.auth_token = auth_token
        this.is_admin = is_admin
        this.user_id = user_id
    }

    public getAuthToken(){
        return this.auth_token
    }

    public isAdmin(){
        return this.is_admin
    }

    public getID(){
        return this.user_id
    }
}

export class UserPayload {
    /**
     * Class encapsulating user creation/auth informations
     */
    private username: string
    private password: string

    public constructor(username: string, password: string){
        this.username = username
        this.password = password
    }
}

export class User {
    /**
     * Class encapsulating returned user informations
     */
    private id: number
    private username: string

    constructor(id: number, username: string){
        this.id = id
        this.username = username
    }

    public getID(){
        return this.id
    }

    public getName(){
        return this.username
    }

}

export class UserUpdate {
    /**
     * Class encapsulating user update informations
     */
    private username: string
    private is_admin: boolean
    private psswd: string

    public constructor(
        username?: string,
        is_admin?: boolean,
        psswd?: string
        ){
            this.username = username
            this.is_admin = is_admin
            this.psswd = psswd
    }
}


