export class ResponseMessage {
    private message: string
    private is_error: boolean

    constructor(message: string, is_error: boolean){
        this.message = message
        this.is_error = is_error
    }

    public toString(){
        return this.message
    }

    public isError(){
        return this.is_error
    }
}

export class Response {
    /**
     * Class representing an http response from the server
     */
    private code: number
    private title: string
    private details: string

    constructor(code: number, title: string, details: string){
        this.code = code
        this.title = title
        this.details = details
    }

    public getCode(){
        return this.code
    }

    public getResponseMessage(){
        switch(this.code){
            case 200:
            case 201:
                return new ResponseMessage("Opération effectuée avec succès !", false)
            case 401:
                return new ResponseMessage("Il faut être connecté pour accéder à cette fonctionnalitée !", true)
            case 403:
                return new ResponseMessage("Ohoh, il semblerait que tu ne sois pas autorisé à accéder à cette fonctionnalitée !", true)
            case 404:
                return new ResponseMessage("Ooops ! Je n'ai pas trouvé ce que tu cherches...", true)
            case 409:
                return new ResponseMessage("Il semblerait qu'il soit impossible d'effectuer cette action...", true)
            case 502:
                return new ResponseMessage("Une erreur serveur s'est produite...", true)
            default:
                return new ResponseMessage("Une erreur inconnue s'est produite...", true)

        }
    }

}