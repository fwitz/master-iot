export class LocationPayload{
    /**
     * Class encapsulating location payload informations
     */
    private name: string
    private indoor: boolean
    private longitude: number
    private latitude: number

    constructor(name?: string, indoor?: boolean, longitude?: number, latitude?: number){
        this.name = name
        this.indoor = indoor
        this.longitude = longitude
        this.latitude = latitude
    }
}

export class Location{
    /**
    * Class encapsulating location informations
    */
    private id: number
    private name: string
    private indoor: boolean
    private longitude: number
    private latitude: number
  
    constructor(id: number, name: string, indoor: boolean, longitude: number, latitude: number){
        this.id = id
        this.name = name
        this.indoor = indoor
        this.longitude = longitude
        this.latitude = latitude
    }
    
    public getCoordinates(): [number, number]{
        return [this.latitude, this.longitude]
    }

    public getName(): string{
        return this.name
    }

    public getID(): number{
        return this.id
    }

}