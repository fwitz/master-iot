import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import * as models from '../../models';

@Component({
  selector: 'friends-status',
  templateUrl: './status.page.html',
  styleUrls: ['./status.page.scss'],
})
export class StatusPage implements OnInit{

   // Data passed in by componentProps
   @Input() friends: Array<models.MeetStatus>;

  constructor(private modalController: ModalController) { 
    
  }

  ngOnInit(){
    console.log(this.friends)
  }


}
