from logging import currentframe
import werkzeug

werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import Api
from flask import Blueprint

from api.main.controller.auth_controller import auth_api
from api.main.controller.user_controller import user_api
from api.main.controller.location_controller import location_api
from api.main.controller.meet_controller import meet_api

blueprint = Blueprint("api", __name__)

api = Api(
    blueprint,
    title="MSC Project",
    version="1.0",
    description="Project made in the msc project course situation",
)

api.add_namespace(auth_api, path="/auth")
api.add_namespace(user_api, path="/user")
api.add_namespace(location_api, path="/location")
api.add_namespace(meet_api, path="/meet")
