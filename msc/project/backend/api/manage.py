import os
import pytest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from flask_cors import CORS

from api.main import create_app, db

from api import blueprint

app = create_app(os.getenv("APP_ENV") or "dev")
app.register_blueprint(blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command("db", MigrateCommand)

cors = CORS(app, resources={r"*": {"origins": "*"}}, supports_credentials=True)


@manager.command
def run():
    app.run(host='0.0.0.0', port='5000')


@manager.command
def test():
    """Runs the unit tests."""
    pytest.main(["-x", "./api/test"])


if __name__ == "__main__":
    manager.run()
