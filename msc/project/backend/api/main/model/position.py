import datetime
import os

from api.main import db


class Position(db.Model):
    """
    Position Model for storing user position related details
    """

    __tablename__ = "position"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    location_id = db.Column(db.Integer, db.ForeignKey("location.id"), nullable=False)

    @staticmethod
    def get_by_id(id):
        """
        Method that return the user with the given id
        """
        return Position.query.filter_by(id=id).first()

    def as_dict(self):
        """
        Method that return the object as a python dict
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        """
        Method returning an object's formatted string representation
        """
        return "<Position: {} ({}, {}, {})".format(self.surname, self.name)
