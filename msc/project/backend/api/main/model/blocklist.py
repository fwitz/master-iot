from api.main import db


class BlockedToken(db.Model):
    """
    Token Model for storing blocked JWT tokens
    """

    __tablename__ = "blocklist"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(100), unique=True, nullable=False)
    blocklisted_on = db.Column(db.DateTime, nullable=False)

    @staticmethod
    def check_blocklist(auth_token):
        """
        Method that check if the given auth token is blocked or not
        """
        res = BlockedToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False

    def __repr__(self):
        """
        Method returning an object's formatted string representation
        """
        return "<Token: {}".format(self.token)
