import datetime
import jwt
import os

from api.main.config import key_by_name
from api.main import db, flask_bcrypt
from api.main.model.blocklist import BlockedToken


class User(db.Model):
    """
    User Model for storing user related details
    """

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(50), nullable=False, unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    password_hash = db.Column(db.String(100), nullable=False)

    @property
    def password(self):
        raise AttributeError("password: write-only field")

    @password.setter
    def password(self, password):
        """
        Password setter
        """
        self.password_hash = flask_bcrypt.generate_password_hash(password).decode(
            "utf-8"
        )

    def check_password(self, password):
        """
        Method that check the entered password to the stored password hash
        """
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    @staticmethod
    def get_by_id(id):
        """
        Method that return the user with the given id
        """
        return User.query.filter_by(id=id).first()

    def encode_auth_token(self, user_id):
        """
        Method that generates an auth token for the user
        """
        try:
            # Creating a the payload. The JWT is only valid for two days
            payload = {
                "exp": datetime.datetime.utcnow() + datetime.timedelta(days=2),
                "iat": datetime.datetime.utcnow(),
                "sub": user_id,
            }
            key = key_by_name[os.getenv("APP_ENV") or "dev_key"]
            return jwt.encode(payload, key, algorithm="HS256")
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Method that decodes an auth token
        """
        try:
            key = key_by_name[os.getenv("APP_ENV") or "dev_key"]
            payload = jwt.decode(
                auth_token, key, options={"require": ["exp", "iat", "sub"]}
            )  # Decode the token as a json object
            # Checking if the token isn't already blocked
            is_blocked_token = BlockedToken.check_blocklist(auth_token)
            if is_blocked_token:
                return "Token blocklisted"
            else:
                return payload["sub"]
        except jwt.ExpiredSignatureError:
            return "Signature expired"
        except jwt.InvalidTokenError:
            return "Invalid token"

    def as_dict(self):
        """
        Method that return the object as a python dict
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        """
        Method returning an object's formatted string representation
        """
        return "<User: {}".format(self.username)
