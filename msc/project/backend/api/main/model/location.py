from api.main import db


class Location(db.Model):
    """
    Location Model for storing possible user positions related details
    """

    __tablename__ = "location"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    indoor = db.Column(db.Boolean, nullable=False, default=False)
    longitude = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)

    @staticmethod
    def get_by_id(id):
        """
        Method that return the user with the given id
        """
        return Location.query.filter_by(id=id).first()

    def as_dict(self):
        """
        Method that return the object as a python dict
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        """
        Method returning an object's formatted string representation
        """
        return "<Position: {} ({}, {})".format(self.name, self.latitude, self.longitude)
