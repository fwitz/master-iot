import datetime
import os

from api.main import db


class Meet(db.Model):
    """
    Meet Model for storing differents meet up
    """

    __tablename__ = "meet"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    session_uuid = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    itinerary = db.Column(db.String, nullable=False)
    checkpoint = db.Column(db.Integer, nullable=False, default=0)
    status = db.Column(db.String, nullable=False, default="waiting")
    destination = db.Column(db.Integer, db.ForeignKey("location.id"), nullable=False)

    @staticmethod
    def get_by_uuid(uuid):
        """
        Method that return all the meet session
        """
        return Meet.query.filter_by(uuid=uuid).all()

    def as_dict(self):
        """
        Method that return the object as a python dict
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def __repr__(self):
        """
        Method returning an object's formatted string representation
        """
        return "<Meet session: {} on itinerary {} for user {}. Steps taken: {})".format(
            self.session_uuid, self.itinerary, self.user_id, self.checkpoint
        )
