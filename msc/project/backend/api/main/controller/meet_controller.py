from flask import request
from flask_restplus import Resource

from api.main.util.dto import MeetDto
from api.main.util.dto import LocationDto
from api.main.util.decorator import token_required

from api.main.service.meet_service import *

meet_api = MeetDto.api
_meet_creation = MeetDto.meet_creation
_meet_get = MeetDto.meet_get
_user_meet_update = MeetDto.user_meet_update
_meet_uuid = MeetDto.meet_uuid


@meet_api.route("/", methods=["GET", "POST", "DELETE"])
class Meet(Resource):
    @token_required
    @meet_api.expect(_meet_creation, validate=True)
    def post(self):
        """Create a new meeting party"""
        data = request.json
        return create_meet(data=data)

    @token_required
    @meet_api.marshal_with(_meet_get, envelope="result")
    def get(self, meet_uuid: str):
        """Get a meeting party infos"""
        return get_meet(meet_uuid=meet_uuid)

    @token_required
    def delete(self, meet_uuid: str):
        """Delete a meeting party"""
        return delete_meet(meet_uuid=meet_uuid)


@meet_api.route("/search/<int:user_id>", methods=["GET"])
class MeetSearch(Resource):
    @token_required
    def get(self, user_id: int):
        """Check if the user is involved in a meeting"""
        return meet_search(user_id=user_id)


@meet_api.route("/<meet_uuid>/<int:user_id>", methods=["GET", "DELETE", "PATCH"])
class UserMeet(Resource):
    @token_required
    @meet_api.expect(_user_meet_update, validate=True)
    def patch(self, meet_uuid: str, user_id: int):
        """Update user's info in a specific meeting party"""
        data = request.json
        return update_meet_user(meet_uuid=meet_uuid, user_id=user_id, data=data)

    @token_required
    def get(self, meet_uuid: str, user_id: int):
        """Get the itinerary of a user"""
        return get_itinerary(meet_uuid=meet_uuid, user_id=user_id)

    @token_required
    def delete(self, meet_uuid: str, user_id: int):
        """Remove a specific user from a meeting party"""
        return remove_user(meet_uuid=meet_uuid, user_id=user_id)


@meet_api.route("/<meet_uuid>/<int:user_id>/checkpoint", methods=["PATCH"])
class UserMeet(Resource):
    @token_required
    def patch(self, meet_uuid: str, user_id: int):
        """ Update user checkpoint position """
        data = request.json
        return set_user_checkpoint(meet_uuid=meet_uuid, user_id=user_id, checkpoint_id=data['checkpoint_id'])

@meet_api.route("/<meet_uuid>/status", methods=["GET"])
class MeetStatus(Resource):
    @token_required
    def get(self, meet_uuid: str):
        """Get the status of a specific meeting"""
        return get_meet_status(meet_uuid=meet_uuid)
