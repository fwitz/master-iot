from flask import request
from flask_restplus import Resource

from api.main.util.dto import LocationDto
from api.main.util.decorator import token_required, admin_token_required

from api.main.service.location_service import *

location_api = LocationDto.api
_location_payload = LocationDto.location_payload
_location_get = LocationDto.location_get
_location_id = LocationDto.location_id


@location_api.route("/", methods=["GET", "POST"])
class Locations(Resource):
    @admin_token_required
    @location_api.expect(_location_payload, validate=True)
    def post(self):
        """Create a new location"""
        data = request.json
        return create_location(data=data)

    @token_required
    def get(self):
        """Get all existing locations"""
        return get_all_locations()


@location_api.route("/<int:loc_id>", methods=["GET", "PATCH", "DELETE"])
class Location(Resource):
    @admin_token_required
    @location_api.marshal_with(_location_get, envelope="result")
    def get(self, loc_id: int):
        """Get a specific location"""
        return get_location(loc_id=loc_id)

    @admin_token_required
    @location_api.expect(_location_payload, validate=True)
    def patch(self, loc_id: int):
        """Update user's info in a specific meeting party"""
        data = request.json
        return update_location(loc_id=loc_id, data=data)

    @admin_token_required
    def delete(self, loc_id: int):
        """Remove a specific user from a meeting party"""
        return delete_location(loc_id=loc_id)


@location_api.route("/reach/<int:user_id>", methods=["GET"])
class ReachableLocation(Resource):
    @token_required
    def get(self, user_id: int):
        """ Get reachable destination from user position """
        return get_reach(user_id=user_id)
