from flask import request
from flask_restplus import Resource

from api.main.service.auth_service import *

from api.main.util.decorator import token_required
from api.main.util.dto import AuthDto

auth_api = AuthDto.api
_user_auth = AuthDto.user_auth


@auth_api.route("/login", methods=["POST"])
class Login(Resource):
    @auth_api.expect(_user_auth, validate=True)
    def post(self):
        """Login user"""
        data = request.json
        return login_user(data=data)


@auth_api.route("/logout", methods=["POST"])
class Logout(Resource):
    @token_required
    def post(self):
        """Logout user"""
        auth_header = request.headers.get("Authorization")
        return logout_user(data=auth_header)


@auth_api.route("/check", methods=["POST"])
class TokenCheck(Resource):
    def post(self):
        """Token validity check"""
        token = request.headers.get("Authorization")
        return is_token_valid(token)
