from flask import request
from flask_restplus import Resource

from api.main.util.dto import UserDto
from api.main.util.dto import LocationDto
from api.main.util.decorator import (
    admin_token_required,
    token_required,
)

from api.main.service.user_service import *

user_api = UserDto.api
_user_creation = UserDto.user_creation
_user_get = UserDto.user_get
_user_update = UserDto.user_update
_location_get = LocationDto.location_get
_location_id = LocationDto.location_id


@user_api.route("/", methods=["GET", "POST"])
class Users(Resource):
    @admin_token_required
    @user_api.expect(_user_creation, validate=True)
    def post(self):
        """Create a new user"""
        data = request.json
        return create_user(data=data)

    @admin_token_required
    def get(self):
        """Get the list of existing users"""
        return get_all_users()


@user_api.route("/reachable/<int:user_id>", methods=["GET"])
class ReachableUsers(Resource):
    @token_required
    def get(self, user_id: int):
        """Get all users in a 3km radius from the given user"""
        return get_users_in_radius(user_id=user_id)


@user_api.route("/<int:user_id>", methods=["GET", "PATCH", "DELETE"])
class User(Resource):
    @token_required
    @user_api.marshal_with(_user_get, envelope="result")
    def get(self, user_id: int):
        """Get a specific user"""
        return get_user(user_id=user_id)

    @admin_token_required
    @token_required
    @user_api.expect(_user_update, validate=True)
    def patch(self, user_id: int):
        """Edit a specific user"""
        data = request.json
        return update_user(user_id=user_id, data=data)

    @admin_token_required
    def delete(self, user_id: int):
        """Delete a specific user"""
        return delete_user(user_id=user_id)


@user_api.route("/<int:user_id>/position", methods=["GET", "PATCH"])
class UserPosition(Resource):
    @token_required
    #@user_api.marshal_with(_location_get, envelope="data")
    def get(self, user_id: int):
        """Get a specific user"""
        return get_position(user_id=user_id)

    @token_required
    @user_api.expect(_location_id, validate=True)
    def patch(self, user_id: int):
        """Edit a specific user"""
        data = request.json
        return update_position(user_id=user_id, data=data)
