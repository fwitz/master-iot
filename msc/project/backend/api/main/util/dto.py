from flask_restplus import Namespace, fields


class AuthDto:
    api = Namespace("Auth", description="Authentication related operations")
    user_auth = api.model(
        "auth_details",
        {
            "username": fields.String(required=True, description="User's username"),
            "password": fields.String(required=True, description="User's password "),
        },
    )


class UserDto:
    api = Namespace("User", description="User related operations")
    user_creation = api.model(
        "user_creation",
        {
            "username": fields.String(required=True, description="User's username"),
            "is_admin": fields.Boolean(required=True, description="User's admin state"),
            "password": fields.String(required=True, description="User's password"),
        },
    )
    user_get = api.model(
        "user_get",
        {
            "id": fields.Integer(required=True, description="User's id"),
            "username": fields.String(required=True, description="User's username"),
        },
    )
    user_update = api.model(
        "user_update",
        {
            "username": fields.String(description="User's username"),
            "is_admin": fields.Boolean(description="User's admin state"),
            "password": fields.String(description="User's new password"),
        },
    )
    user_id = api.model(
        "user_id", {"id": fields.Integer(required=True, description="User's id")}
    )


class LocationDto:
    api = Namespace("Location", description="Location related operations")
    location_payload = api.model(
        "location_creation",
        {
            "name": fields.String(description="Location's name"),
            "indoor": fields.Boolean(
                description="Location's indoor state"
            ),
            "longitude": fields.String(
                description="Location's longitude"
            ),
            "latitude": fields.String(description="Location's latitude"),
        },
    )
    location_get = api.model(
        "location_get",
        {
            "id": fields.Integer(required=True, description="Location's id"),
            "name": fields.String(required=True, description="Location's name"),
            "indoor": fields.Boolean(
                required=True, description="Location's indoor state"
            ),
            "longitude": fields.String(
                required=True, description="Location's longitude"
            ),
            "latitude": fields.String(required=True, description="Location's latitude"),
        },
    )
    location_id = api.model(
        "location_id",
        {"id": fields.Integer(required=True, description="Location's id")},
    )


class MeetDto:
    api = Namespace("Meet", description="Meetings related operations")
    meet_get = api.model(
        "meet_get",
        {
            "session_uuid": fields.String(
                required=True, description="Meeting's uuid string"
            ),
            "users": fields.List(
                fields.Integer(), required=True, description="List of involved users id"
            ),
            "destination": fields.Integer(
                required=True, description="User actual checkpoint"
            ),
        },
    )
    meet_creation = api.model(
        "meet_create",
        {
            "users": fields.List(
                fields.Integer(), required=True, description="List of involved users id"
            ),
            "destination": fields.Integer(
                required=True, description="User actual checkpoint"
            ),
        },
    )
    user_meet_update = api.model(
        "user_meet_update",
        {
            "status": fields.String(description="Meeting's uuid string"),
        },
    )
    meet_uuid = api.model(
        "meet_uuid",
        {
            "session_uuid": fields.String(
                required=True, description="Meeting's uuid string"
            )
        },
    )
