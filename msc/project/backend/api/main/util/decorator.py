from functools import wraps
from flask import request

from api.main.service.auth_service import *

from api.main.util.response import Response


def token_required(f):
    """
    Method wrapper checking if the user making the request is logged
    """

    @wraps(f)
    def decorated(*args, **kwargs):

        res = get_logged_user(request)
        data = res[0]["data"]

        if data == {}:
            return Response(
                401, "Unauthorized", "User not authenticated, unable to continue"
            ).send()

        return f(*args, **kwargs)

    return decorated


def admin_token_required(f):
    """
    Method wrapper checking if the user making the request is admin
    """

    @wraps(f)
    def decorated(*args, **kwargs):

        res = get_logged_user(request)
        data = res[0]["data"]

        if data == {}:
            return Response(
                401, "Unauthorized", "User not authenticated, unable to continue"
            ).send()

        if data['data']["is_admin"] == False:
            return Response(
                403,
                "Forbidden",
                "User has no admin rights, unable to access the resource",
            ).send()

        return f(*args, **kwargs)

    return decorated
