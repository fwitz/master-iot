import os

from datetime import datetime, date

from api.main.config import basedir

from api.main.util.file_manager import write_to_file, append_to_file


class Log:
    """Class representing a log line"""

    def __init__(self, request: str, response: dict):
        self._request = request
        self._response = response

    def to_str(self):
        """Method to return the log object as a parsable string"""
        log_line: str = (
            datetime.now().strftime("%m/%d/%Y - %H:%M:%S ")
            + self._request
            + " "
            + self._response["code"]
            + " "
            + self._response["title"]
            + ": "
            + self._response["details"]
        )
        return log_line


def add_log(request: str, response: dict):
    """Method to add a response to the day log"""
    log_dir = os.path.join(basedir, "log")
    if os.path.isdir(log_dir):
        log_path = os.path.join(log_dir, date.today().strftime("%m/%d/%Y") + ".log")
        # Creating log
        log_line: str = Log(request=request, response=response).to_str()
        # Adding it to the log file. If it doesn't exists, we create it
        if os.path.exists(log_path):
            append_to_file(path=log_path, data=log_line)
        else:
            write_to_file(path=log_path, data=log_line)


def get_log(date: str) -> list[str]:
    """Method to get logs of a specific day"""
    log_file = os.path.join(os.path.join(basedir, "log"), date + ".log")
    if os.path.exists(log_file):
        log = open(log_file, "r")
        return log.readlines()
