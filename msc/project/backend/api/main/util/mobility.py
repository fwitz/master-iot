import math
import numpy as np
from haversine import haversine, Unit

from collections import defaultdict

from api.main.model.location import Location


def compute_distance(origin: tuple, destination: tuple) -> float:
    """
    Method that compute the distance in km between two coordinates points formated as (lat, long) using havesine formula
    """
    return haversine(origin, destination)


def find_path(start: int, end: int, locations: list[Location]) -> list:
    """
    Method that applies Djisktra's algorithm on the od matrix to find the shortest path from origin to destination
    """
    od_matrix = get_od_matrix(locations)
    coordinates_list = [(l.latitude, l.longitude) for l in locations]
    od_size = od_matrix.shape[0]
    # We always need to visit the start
    nodes_to_visit = {start}
    visited_nodes = set()
    distance_from_start = defaultdict(lambda: float("inf"))
    # Distance from start to start is 0
    distance_from_start[start] = 0
    tentative_parents = {}
    path = []
    while nodes_to_visit:
        # The next node should be the one with the smallest weight
        current = min([(distance_from_start[node], node) for node in nodes_to_visit])[1]

        # The end was reached
        if current == end:
            break

        nodes_to_visit.discard(current)
        visited_nodes.add(current)

        for i in range(od_size):
            if od_matrix[i, current] != -1.0 and i not in visited_nodes:
                neighbour_distance = (
                    distance_from_start[current] + od_matrix[i, current]
                )
                if neighbour_distance < distance_from_start[i]:
                    distance_from_start[i] = neighbour_distance
                    tentative_parents[i] = current
                    nodes_to_visit.add(i)

    if len(tentative_parents) == 0:
        return []
    cursor = end
    while cursor:
        path.append(cursor)
        cursor = tentative_parents.get(cursor)
    return list(reversed(path))


def time_left(origin: tuple, destination: tuple) -> float:
    """
    Method that compute the time in minutes needed to go from origin to destination
    """
    if destination != (0, 0):
        d = compute_distance(origin, destination)
        return (d / 5.6) * 60
    else:
        return 9999.99


def get_od_matrix(locations: list[Location]):
    """
    Method that return the od matrix based on the given locations
    """
    coordinates_list = [(l.latitude, l.longitude) for l in locations]
    od_size = len(coordinates_list)
    od = np.zeros((od_size, od_size), dtype="float")
    for i in range(od_size):
        for j in range(od_size):
            if i != j:
                d = compute_distance(coordinates_list[i], coordinates_list[j])
                if d <= 3.0:
                    od[i, j] = d
                else:
                    od[i, j] = -1.0
            else:
                od[i, j] = -1.0
    return od
