from api.main import db


def db_add(data):
    """Method that add data to db"""
    db.session.add(data)
    db.session.commit()


def db_remove(data):
    """Method that remove data from the db"""
    db.session.delete(data)
    db.session.commit()


def db_update(obj, data: dict):
    """
    Method that update an object on the db
    """
    for key, value in data.items():
        setattr(obj, key, value)
    db.session.commit()
