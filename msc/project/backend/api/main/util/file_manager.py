import os
import json
import base64
from typing import Union

from api.main.util.response import InternalResponse


def write_to_file(path: str, data: str) -> dict:
    """Method to create a text file and write to it"""
    try:
        with open(path, "w") as file:
            file.write(data)
    except Exception as e:
        return InternalResponse(code=5, title=type(e).__name__, details=str(e)).get()
    finally:
        file.close()
    return InternalResponse(
        code=1, title="Writed", details="Wrote to file successfully"
    ).get()


def append_to_file(path: str, data: str):
    """Method to append data to an exisitng text file"""
    try:
        with open(path, "a") as file:
            file.write(data)
    except Exception as e:
        return InternalResponse(code=5, title=type(e).__name__, details=str(e)).get()
    finally:
        file.close()
    return InternalResponse(
        code=1, title="Appended", details="Appended to file successfully"
    )
