class Response:
    """Class used for HTTP response handling"""

    def __init__(self, code: int, title: str, details: str, **kwargs):
        self._code = code
        self._title = title
        self._details = details
        self._data = kwargs

    def get(self) -> dict:
        """Method to get the response object"""
        return {
            "code": self._code,
            "title": self._title,
            "details": self._details,
            "data": self._data,
        }

    def send(self) -> tuple[dict, int]:
        """
        Method to return the response object as json
        object alongside http response code
        """
        return {
            "code": self._code,
            "title": self._title,
            "details": self._details,
            "data": self._data,
        }, self._code


class InternalResponse:
    """Class used for internal response handling"""

    def __init__(self, code: int, title: str, details: str):
        self._code = code
        self._title = title
        self._details = details

    def get(self) -> dict:
        """Method to get the response object"""
        return {
            "code": self._code,
            "title": self._title,
            "details": self._details,
        }

    @property
    def get_code(self):
        """Getter to retrieve response code"""
        return self._code
