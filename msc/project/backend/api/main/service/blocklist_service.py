from api.main.util.response import Response
from datetime import datetime
from warnings import resetwarnings

from api.main import db
from api.main.model.blocklist import BlockedToken
from api.main.util.db_operation import db_add


def save_token(token: str) -> tuple[dict, int]:
    """
    Method that add a Token on the blocklist
    """
    blocked = BlockedToken.query.filter(
        BlockedToken.token == token,
    ).first()
    if not blocked:
        blocked_token = BlockedToken(token=token, blocklisted_on=datetime.now())
        db_add(blocked_token)
        return Response(
            code=200, title="Success", details="Token successfully blocklisted"
        ).send()
    else:
        return Response(
            code=409, title="Conflict", details="Token already blocklisted"
        ).send()
