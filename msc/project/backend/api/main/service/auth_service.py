import datetime

from api.main.model.user import User
from api.main.service.blocklist_service import save_token
from api.main.service.user_service import is_admin

from api.main.util.response import Response
from api.main.util.db_operation import db_update


def login_user(data) -> tuple[dict, int]:
    """
    Method to login a user and generate a valid jwt
    """
    # Checking if user exists first
    user: User = User.query.filter(User.username == data["username"]).first()
    # If user exists and the entered password is correct
    if user and user.check_password(data["password"]):
        # Generating auth token
        auth_token = user.encode_auth_token(user.id)
        if auth_token:
            # Returning auth token and user role in the http response
            return Response(
                code=200,
                title="Success",
                details="User successfully logged in",
                data={
                    "auth_token": auth_token.decode('utf-8'),
                    "user_id": user.id,
                    "is_admin": is_admin(user.id),
                },
            ).send()
    else:
        return Response(
            code=401, title="Unauthorized", details="USername or password incorrect"
        ).send()


def logout_user(data) -> tuple[dict, int]:
    """
    Method to logout a user and block the previous jwt
    """
    if data:
        auth_token = data
    else:
        auth_token = ""
    # Checking if auth_token exists, if not we return a bad request response
    if auth_token:
        resp = User.decode_auth_token(auth_token)  # Decode token
        # If decoded token isn't a json object, we add it to the blocklist
        if not isinstance(resp, str):
            return save_token(token=auth_token)
        else:
            return Response(
                code=401,
                title="Unauthorized",
                details="Unvalid token (expired or already blacklisted)",
            ).send()
    else:
        return Response(
            code=401,
            title="Unauthorized",
            details="No auth token given in request's header",
        ).send()


def get_logged_user(new_request) -> tuple[dict, int]:
    auth_token = new_request.headers.get("Authorization")
    if auth_token:
        resp = User.decode_auth_token(auth_token)
        # If decoded token is a json object we process it
        if not isinstance(resp, str):
            user: User = User.query.filter_by(
                id=resp
            ).first()  # Getting user corresponding to id get by token decoding
            if user:
                # Returning user role in the response object
                return Response(
                    code=200,
                    title="Success",
                    details="Logged user successfully retrieved",
                    data={"is_admin": is_admin(user.id)},
                ).send()
        else:
            return Response(
                code=401,
                title="Unauthorized",
                details="Unvalid token (expired or already blacklisted)",
            ).send()
    else:
        return Response(
            code=401,
            title="Unauthorized",
            details="No auth token given in request's header",
        ).send()


def is_token_valid(token) -> tuple[dict, int]:
    resp = User.decode_auth_token(token)
    # If decoded token is a json object we process it
    if not isinstance(resp, str):
        return Response(code=200, title="Success", details="Token is valid").send()
    else:
        return Response(
            code=401,
            title="Unauthorized",
            details="Unvalid token (expired or already blacklisted)",
        ).send()
