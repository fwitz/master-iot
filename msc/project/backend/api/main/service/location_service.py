from api.main.model.location import Location
from api.main.model.position import Position
from api.main.util.mobility import compute_distance

from api.main.util.response import Response
from api.main.util.db_operation import *


def get_all_locations() -> tuple[dict, int]:
    """
    Method that return a list of all existing locations
    """
    locs: list[Location] = Location.query.all()
    json_locs: list[dict] = []
    for l in locs:
        json_locs.append(l.as_dict())
    return Response(
        code=200,
        title="Success",
        details="List of locations retrieved successfully",
        data=json_locs,
    ).send()


def get_location(loc_id: int) -> tuple[dict, int]:
    """Method that get a specific location"""
    loc: Location = Location.get_by_id(loc_id)
    if loc:
        return Response(
            code=200,
            title="Success",
            details="Location retrieved successfully",
            data=loc.as_dict(),
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="Location doesn't exists, unable to retrieve it",
        ).send()


def create_location(data: dict) -> tuple[dict, int]:
    """Method to create a new location"""
    loc: Location = Location.query.filter_by(Location.name == data["name"]).first()
    if not loc:
        new_loc: Location = Location(
            name=data["name"],
            indoor=data["indoor"],
            longitude=data["longitude"],
            latitude=data["latitude"],
        )
        db_add(new_loc)
        return Response(
            code=201, title="Created", details="Location created successfully"
        ).send()
    else:
        return Response(
            code=409,
            title="Conflict",
            details="Location name already used, unable to create new location with it",
        ).send()


def delete_location(loc_id: int) -> tuple[dict, int]:
    """Method to delete a location"""
    loc: Location = Location.get_by_id(loc_id)
    if loc:
        db_remove(loc)
        return Response(
            code=200, title="Deleted", details="Location successfully deleted"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="Location doesn't exists, unable to delete it",
        ).send()


def update_location(loc_id: int, data: dict) -> tuple[dict, int]:
    """Method that update a specific location"""
    loc: Location = Location.get_by_id(loc_id)
    if loc:
        db_update(loc, data)
        return Response(
            code=200, title="Updated", details="Location successfully updated"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="Location doesn't exists, unable to update it",
        ).send()


def get_reach(user_id: int) -> tuple[dict, int]:
    """ Method that returns a list of location in a radius of 3 km of the specified user """
    user_pos: Position = Position.query.filter(Position.user_id == user_id).first()
    user_loc: Location = Location.get_by_id(user_pos.location_id)
    if user_loc:
        all_loc: list[Location] = Location.query.all()
        all_loc.remove(user_loc)
        in_range = []
        for l in all_loc:
            if compute_distance((l.latitude, l.longitude), (user_loc.latitude, user_loc.longitude)) <= 3.0:
                in_range.append(l.as_dict())
        return Response(
            code=200,
            title="Success",
            details="In range locations retrieved successfully",
            data = in_range
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User position doesn't exists, unabel to retrieve in range locations",
        ).send() 

