from api.main.model.user import User
from api.main.model.location import Location
from api.main.model.position import Position

from api.main.util.response import Response
from api.main.util.db_operation import *
from api.main.util.mobility import compute_distance


def get_all_users() -> tuple[dict, int]:
    """
    Method that return a list of all existing users
    """
    users: list[User] = User.query.all()
    json_users: list[dict] = []
    for u in users:
        json_users.append(u.as_dict())
    return Response(
        code=200,
        title="Success",
        details="List of users retrieved successfully",
        data=json_users,
    ).send()


def get_user(user_id: int) -> tuple[dict, int]:
    """Method that get a specific user"""
    user: User = User.get_by_id(user_id)
    if user:
        return Response(
            code=200,
            title="Success",
            details="User retrieved successfully",
            data=user.as_dict(),
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to retrieve it",
        ).send()


def create_user(data: dict) -> tuple[dict, int]:
    """Method to create a new user"""
    user: User = User.query.filter(User.username == data["username"]).first()
    if not user:
        new_user: User = User(
            username=data["username"],
            is_admin=data["is_admin"],
            password=data["password"],
        )
        db_add(new_user)
        new_user_loc: Position = Position(user_id=new_user.id, location_id=0)
        db_add(new_user_loc)
        return Response(
            code=201, title="Created", details="User created successfully"
        ).send()
    else:
        return Response(
            code=409,
            title="Conflict",
            details="Username already used, unable to create new user with it",
        ).send()


def delete_user(user_id: int) -> tuple[dict, int]:
    """Method to delete a user"""
    user: User = User.get_by_id(user_id)
    if user:
        db_remove(user)
        return Response(
            code=200, title="Deleted", details="User successfully deleted"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to delete it",
        ).send()


def update_user(user_id: int, data: dict) -> tuple[dict, int]:
    """Method that update a specific user"""
    user: User = User.get_by_id(user_id)
    if user:
        db_update(user, data)
        return Response(
            code=200, title="Updated", details="User successfully updated"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to update it",
        ).send()


def is_admin(user_id: int) -> tuple[dict, int]:
    """Method that return if a specific user is admin"""
    user: User = User.get_by_id(user_id)
    if user:
       return user.is_admin
    else:
        return False


def get_position(user_id: int) -> tuple[dict, int]:
    """Method that return the specific user's position"""
    user: User = User.get_by_id(user_id)
    if user:
        pos: Position = Position.query.filter(Position.user_id == user_id).first()
        if pos:
            location: Location = Location.get_by_id(pos.location_id)
            if location:
                return Response(
                    code=200,
                    title="Success",
                    details="User location retrieved successfully",
                    data = location.as_dict()
                ).send()
            else:
                return Response(
                    code=404,
                    title="Not Found",
                    details="Location doesn't exists, unbale to locate user",
                ).send()
        else:
            return Response(
                code=404,
                title="Not Found",
                details="User position not registered, unable to retrieve it",
            ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to retrieve its position",
        ).send()


def update_position(user_id: int, data: dict) -> tuple[dict, int]:
    """Method that update a specific user's position"""
    user: User = User.get_by_id(user_id)
    if user:
        pos: Position = Position.query.filter(Position.user_id == user_id).first()
        if pos:
            db_update(pos, data)
            return Response(
                code=200, title="Updated", details="User successfully updated"
            ).send()
        else:
            return Response(
                code=404,
                title="Not Found",
                details="User position not registered, unable to update it",
            ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to update its position",
        ).send()


def get_users_in_radius(user_id: int) -> tuple[dict, int]:
    """
    Method that return a list of users that are in a radius of 3km
    """
    user: User = User.get_by_id(user_id)
    if user:
        all_pos: list[Position] = Position.query.all()
        pos: Position = Position.query.filter(Position.user_id == user_id).first()
        if pos:
            user_loc: Location = Location.get_by_id(pos.location_id)
            if user_loc:
                # Removing from all position the user position
                all_pos.remove(pos)
                # Checking which user are the nearests from the user
                nearest: list[User] = []
                for p in all_pos:
                    loc: Location = Location.get_by_id(p.location_id)
                    if loc:
                        if (
                            compute_distance(
                                (user_loc.latitude, user_loc.longitude),
                                (loc.latitude, loc.longitude),
                            )
                            <= 3.0
                        ):
                            nearest.append(p.user_id)
                # Make a json dict of user information
                json_users: list[dict] = []
                for u in nearest:
                    json_users.append(User.get_by_id(u).as_dict())
                return Response(
                    code=200,
                    title="Success",
                    details="List of nearests users retrieved successfully",
                    data=json_users,
                ).send()
            else:
                return Response(
                    code=404,
                    title="Not Found",
                    details="User specified location not found, unable to retrieve the nearests users",
                ).send()
        else:
            return Response(
                code=404,
                title="Not Found",
                details="User position not registered, unable to retrieve the nearests users",
            ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User doesn't exists, unable to retrieve the nearest other users",
        ).send()
