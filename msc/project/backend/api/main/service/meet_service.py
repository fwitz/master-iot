import uuid

from sqlalchemy import and_

from api.main.model.user import User
from api.main.model.location import Location
from api.main.model.position import Position
from api.main.model.meet import Meet

from api.main.util.response import Response
from api.main.util.dto import MeetDto, LocationDto
from api.main.util.mobility import *
from api.main.util.db_operation import *


def get_meet(meet_uuid: str) -> tuple[dict, int]:
    """
    Method that return the users involved and the meet place
    """
    meet_party: list[Meet] = Meet.query.filter(Meet.session_uuid == meet_uuid).all()
    users: list[User] = []
    for m in meet_party:
        user: User = User.get_by_id(m.user_id)
        if user:
            users.append(user.as_dict())
    data = {
        "session_uuid": meet_party[0].session_uuid, 
        "users": users, 
        "destination": meet_party[0].destination
        }
    return Response(
        code=200,
        title="Success",
        details="Meeting details retrieved successfully",
        data=data,
    ).send()


def create_meet(data: dict) -> tuple[dict, int]:
    """
    Method that create a new meeting party
    """
    session_id: str = str(uuid.uuid4())
    for u in data["users"]:
        user_pos: Position = Position.query.filter(Position.user_id == u).first()
        check_user: Meet = Meet.query.filter(Meet.user_id == u).first()
        if not check_user:
            new_meet: Meet = Meet(
                session_uuid=session_id,
                user_id=u,
                itinerary=",".join(
                    [
                        str(e)
                        for e in find_path(
                            user_pos.location_id - 1,
                            data["destination"],
                            Location.query.all(),
                        )
                    ]
                ),
                status="waiting",
                destination=data["destination"],
            )
            db_add(new_meet)
        else:
            meet_party: list[Meet] = Meet.query.filter(Meet.session_uuid == session_id).all()
            for m in meet_party:
                db_remove(m)
            return Response(
                code=409,
                title="Conflict",
                details="A user is already involved in another meet party, unable to create the party",
            ).send()
    return Response(
        code=201, title="Created", details="Meet party created successfully"
    ).send()


def delete_meet(meet_uuid: str) -> tuple[dict, int]:
    """
    Method that delete a meeting party
    """
    meet_party: list[Meet] = Meet.query.filter(Meet.session_uuid == meet_uuid).all()
    if meet_party:
        for m in meet_party:
            db_remove(m)
        return Response(
            code=200, title="Deleted", details="Meet party successfully deleted"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="Meet party doesn't exists, unable to delete it",
        ).send()


def remove_user(meet_uuid: str, user_id: int) -> tuple[dict, int]:
    """
    Method that remove a user from a meeting party
    """
    meet_user: Meet = Meet.query.filter(
        and_(Meet.session_uuid == meet_uuid, Meet.user_id == user_id)
    ).first()
    if meet_user:
        db_remove(meet_user)
        return Response(
            code=200,
            title="Deleted",
            details="User successfully removed from the party",
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User not in the party, unable to remove it",
        ).send()


def update_meet_user(meet_uuid: str, user_id: int, data: dict) -> tuple[dict, int]:
    """
    Method to update a meeting party user's infos
    """
    meet_user: Meet = Meet.query.filter(
        and_(Meet.session_uuid == meet_uuid, Meet.user_id == user_id)
    ).first()
    if meet_user:
        db_update(meet_user, data)
        return Response(
            code=200, title="Updated", details="Meet party user successfully updated"
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User not in the meet party, unable to update it",
        ).send()


def get_meet_status(meet_uuid: str) -> tuple[dict, int]:
    """
    Method that retrieve the travel of all the meeting's participant
    """
    meet_party: list[Meet] = Meet.query.filter(Meet.session_uuid == meet_uuid).all()
    status: list[dict] = []
    for m in meet_party:
        if m.checkpoint == 0:
            pos: Position = Position.query.filter(Position.user_id == m.user_id).first()
            user_loc: Location = Location.get_by_id(pos.location_id)
        else:
            user_loc: Location = Location.get_by_id(m.checkpoint)
        destination_coord = Location.get_by_id(m.destination)
        status.append(
            {
                "user_id": m.user_id,
                "status": m.status,
                "checkpoint": m.checkpoint,
                "time_left": time_left(
                    (user_loc.latitude, user_loc.longitude),
                    (destination_coord.latitude, destination_coord.longitude),
                ),
            }
        )
    return Response(
        code=200,
        title="Success",
        details="Meeting status retrieved successfully",
        data=status,
    ).send()


def get_itinerary(meet_uuid: str, user_id: int) -> tuple[dict, int]:
    """
    Method that return the specified user's itinerary for the specified meeting
    """
    meet_user: Meet = Meet.query.filter(
        and_(Meet.session_uuid == meet_uuid, Meet.user_id == user_id)
    ).first()
    if meet_user:
        itinerary: list[str] = meet_user.itinerary.split(",")
        return Response(
            code=200,
            title="Success",
            details="User itinerary successfully retrieved",
            data={"itinerary": itinerary},
        ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User not in the meet party, unable to update it",
        ).send()


def set_user_checkpoint(meet_uuid: str, user_id: int, checkpoint_id: int):
    """
    Method that set the checkpoint for the specified user in the meet party
    """
    meet_user: Meet = Meet.query.filter(
        and_(Meet.session_uuid == meet_uuid, Meet.user_id == user_id)
    ).first()
    if meet_user:
        user_pos: Position = Position.query.filter(Position.user_id == user_id)
        if user_pos:
            db_update(user_pos, {"location_id": checkpoint_id})
            db_update(meet_user, {"checkpoint": checkpoint_id})
            return Response(
                code=200,
                title="Success",
                details="Checkpoint successfully set"
            ).send()
        else:
            return Response(
                code=404,
                title="Not Found",
                details="User position not found, unable to update it",
            ).send()
    else:
        return Response(
            code=404,
            title="Not Found",
            details="User not in the meet party, unable to update it",
        ).send()


def meet_search(user_id: int) -> tuple[dict, int]:
    """
    Method that check if the specified user is involved in a meeting
    """
    meet_user: Meet = Meet.query.filter(Meet.user_id == user_id).first()
    if meet_user:
        return get_meet(meet_user.session_uuid)
    else:
        return Response(
            code=200,
            title="Success",
            details="User not involved in any meet party",
        ).send()
