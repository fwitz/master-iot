import os

basedir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))


class Config:
    SECRET_KEY_DEV = ".J0|8+;zZZy?xmQay:4n>~3Ewxo'1Q"
    SECRET_KEY = os.getenv("SECRET_KEY")
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "db/main.db")
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(basedir, "db/test.db")
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "postgresql://localhost/IntegAppMAin"


config_by_name = dict(dev=DevelopmentConfig, test=TestingConfig, prod=ProductionConfig)
key_by_name = dict(dev_key=Config.SECRET_KEY_DEV, key=Config.SECRET_KEY)
