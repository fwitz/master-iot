CYAN=`tput setaf 6`
GREEN=`tput setaf 2`
NC=`tput sgr0`

echo "${CYAN}Creating dev databases..${NC}"	
if [ ! -d api/db ] ; then
	mkdir api/db
fi
sqlite3 -batch api/db/main.db <<"EOF"
.databases
.quit
EOF
echo "${CYAN}Migrating dev databases..${NC}"	
python api/manage.py db init
python api/manage.py db migrate --message 'initial database migration'
python api/manage.py db upgrade
echo "${CYAN}Populating dev databases..${NC}"
sqlite3 -batch api/db/main.db <<"EOF"
INSERT INTO user (username, is_admin, password_hash) VALUES ('root', 1, '$2b$12$rXyDJtfXzHbPlo91v5YsJeihK6JhICAEElPYRJRT7EDkJ7LdKKS2i');
INSERT INTO user (username, is_admin, password_hash) VALUES ('user1', 0, '$2b$12$gUU2A6HakAE19P3QueCLxuci9VKl/VHn4v10mLg7Cn48bqz8lRwwS');
INSERT INTO user (username, is_admin, password_hash) VALUES ('user2', 0, '$2b$12$N5hU7gFiDfwt71zIiGoa2uIYPN.3QkJQ1MgC2KGG6tIQJzFb9IsSO');
INSERT INTO user (username, is_admin, password_hash) VALUES ('user3', 0, '$2b$12$xBVtTFV1eWEXDofW42ywXe8f1omKBb63IwhINQS/i4Km2ppTRwlTG');
INSERT INTO user (username, is_admin, password_hash) VALUES ('user4', 0, '$2b$12$n2a75MFT8pAxEtJgXF6OOOtIX5WWIkhsXs1dQDlQOc0bzsc26bfD.');
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('Crous', 0, 47.495908, 6.803837);
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('Batiment C', 0, 47.495408, 6.804651);
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('Parc Près la Rose', 0, 47.505776, 6.803153);
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('Gare', 0, 47.510445, 6.801154);
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('Tacos', 0, 47.511527, 6.796582);
INSERT INTO location (name, indoor, longitude, latitude) VALUES ('BU', 0, 47.495581, 6.803780);
INSERT INTO position (user_id, location_id) VALUES (1, 1);
INSERT INTO position (user_id, location_id) VALUES (2, 2);
INSERT INTO position (user_id, location_id) VALUES (3, 4);
INSERT INTO position (user_id, location_id) VALUES (4, 6);
INSERT INTO position (user_id, location_id) VALUES (5, 6);

EOF

echo "${GREEN} Setup complete !${NC}"
