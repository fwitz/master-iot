CYAN=`tput setaf 6`
GREEN=`tput setaf 2`
RED=`tput setaf 1`
NC=`tput sgr0`

template='{"ip":"%s","port":"%s"}'

if [ $# == 2 ] ; then
	echo "${CYAN}Applying settings...${NC}"
    json_string=$(printf "$template" "$1" "$2")
    echo "$json_string" >> ../frontend/frontend/network_config.json
    echo "${GREEN}Settings applied !${NC}"
else
    echo "${RED}The script needs two args. Help: bash set_api_adress <ip> <port>${NC}"	
fi