import pandas as pd
import datetime
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('TkAgg')
import numpy as np

def preprocess():
    """ Method that do all the pre processing of the csv file """
    # First we read the data file using pandas
    print("➡ Opening data file...")
    data = pd.read_csv('Porto_taxi_raw_data_training.csv', engine='python', encoding='utf-8', on_bad_lines='skip')
    # Converting Unix timestamp to readable datetime
    print("➡ Converting timestamps to correct datetime format...")
    for i in range(len(data['TIMESTAMP'])):
        if(type(data['TIMESTAMP'].iloc[i]) != np.int64 and type(data['TIMESTAMP'].iloc[i]) != int):
            d = int(datetime.datetime.timestamp(datetime.datetime.strptime(str(data["TIMESTAMP"].iloc[i]), "%Y-%m-%d %H:%M:%S")))
        else:
            d = int(data['TIMESTAMP'].iloc[i])
        data['TIMESTAMP'] = data['TIMESTAMP'].replace(data['TIMESTAMP'].iloc[i], datetime.datetime.fromtimestamp(d))
    print(data.head())
    print("Shape: ", data.shape)
    print("➡ Cleaning data...")
    to_remove: list[int] = []
    # Finding missing data
    for i in range(len(data['MISSING_DATA'])):
        if(data['MISSING_DATA'].iloc[i] == True):
            to_remove.append(i)
    # Finding rows with less than 4 coordinates in the POLYLINE column
    for i in range(len(data['POLYLINE'])):
        if(len(data["POLYLINE"].iloc[i]) < 4):
            to_remove.append(i)
    # Removing wrong data from the dataset
    for r in to_remove:
        data.drop(r, inplace=True)
    print(data.head())
    print("Shape: ", data.shape)
    print("➡ Selecting trips on July 1st, 2013...")
    to_copy = []
    subdata = []
    # Copying data with the corresponding timestamp
    for i in range(len(data['TIMESTAMP'])):
        if(data["TIMESTAMP"].iloc[i].date() == datetime.date(2013, 7, 1)):
            to_copy.append(i)
    for c in to_copy:
        subdata.append(data.iloc[c])
    # Recreate a dataframe using the created list
    subdata = pd.DataFrame(subdata, columns=data.columns)
    print(subdata.head())
    print("Shape: ", subdata.shape)
    print("➡ Displaying trips hour by hour...")
    # Sorting the data by increasing time to more easily count the number of trips
    subdata.sort_values(["TIMESTAMP"], axis=0, ascending=[True], inplace=True)
    y_axis = np.zeros(24, dtype=int)
    x_axis = [x for x in range(0, 24)]
    j: int = 0
    for i in range(len(subdata['TIMESTAMP'])):
        y_axis[subdata["TIMESTAMP"].iloc[i].time().hour] += 1
    fig = plt.figure()
    bar_width = 1
    plt.bar(x_axis,y_axis, width=bar_width, edgecolor = ['blue' for i in y_axis], linewidth = 1)
    plt.xticks([r-0.5 for r in range(len(x_axis))], ['0' + str(x) + 'h' if x < 10 else str(x) + 'h' for x in x_axis], rotation=45)
    plt.show()
    print("➡ Selecting only the trips between 11:00 am and 11:59 am...")
    selection = []
    to_copy = []
    for i in range(len(subdata['TIMESTAMP'])):
        if(subdata["TIMESTAMP"].iloc[i].time().hour == 11):
            to_copy.append(i)
    for c in to_copy:        
        selection.append(subdata.iloc[c])
    selection = pd.DataFrame(selection, columns=data.columns)
    print(selection.head())
    print("Shape: ", selection.shape)
    print("➡ Extracting origins and destination coordinate...")
    # Creating the final dataframe
    origins_destination: list[list[float]] = []
    # Going through each polyline
    for i in range(len(selection["POLYLINE"])):
        coordinates: list[float] = []
        index: list[int] = [0,1,-2,-1] # Defining some index for polyline parsing
        # Remove the opening and closing bracket and split the line
        polyline = selection["POLYLINE"].iloc[i]
        polyline: list[str] = polyline[1:-1].split(',')
        # Getting origin and destination coordinates
        for i in index:
            coo: str = polyline[i].strip() # Remove useless spaces
            # Remove remaining opening or closing bracket to get the value
            if(coo[0] == '['):
                coo = coo[1:]
            elif(coo[-1] == ']'):
                coo = coo[:-1]
            coordinates.append(coo)
        origins_destination.append(coordinates)
    # Creating a dataframe
    col_name = ["ORIGIN_LONGITUDE", "ORIGIN_LATITUDE", "DESTINATION_LONGITUDE", "DESTINATION_LATITUDE"]
    final_dataframe = pd.DataFrame(origins_destination, columns=col_name)
    print(final_dataframe.head())
    print("Shape: ", final_dataframe.shape)
    print("➡ Exporting data as csv file...")
    final_dataframe.to_csv("origins_destinations.csv", index=False)

if __name__ == "__main__":
    """ Main script """
    preprocess()