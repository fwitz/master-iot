import pandas as pd
import numpy as np
import pyproj as pp
from sklearn import cluster

def process():
    """ Method that compute a Kmean clustering for the origins_destinations.csv file"""
    # First we read the data file using pandas
    print("➡ Opening data file...")
    data = pd.read_csv('origins_destinations.csv', engine='python', encoding='utf-8', on_bad_lines='skip')
    print(data.head())
    # Computing cartesian coordinates of the coordinates in data
    print("➡ Converting coordinates...")
    col_name = {'ORIGIN_X', 'ORIGIN_Y', 'DESTINATION_X', 'DESTINATION_Y'} # New columns names
    # Defining coordinates system for the conversion
    epsg4326 = pp.CRS('EPSG:4326')
    epsg5018 = pp.CRS('EPSG:5018')
    # Computing the coordinates transformation
    origin_x, origin_y = pp.transform(epsg4326, epsg5018, data['ORIGIN_LATITUDE'], data['ORIGIN_LONGITUDE'])
    destination_x, destination_y = pp.transform(epsg4326, epsg5018, data['DESTINATION_LATITUDE'], data['DESTINATION_LONGITUDE'])
    # Appending data to dataframe
    columns_name = ['ORIGIN_X', 'ORIGIN_Y', 'DESTINATION_X', 'DESTINATION_Y']
    columns_data = []
    for i in range(len(origin_x)):
        columns_data.append([origin_x[i], origin_y[i], destination_x[i], destination_y[i]])
    new = pd.DataFrame(columns_data, columns=columns_name)
    data = pd.concat([data, new], axis=1)
    print(data.head())
    print("➡ Computing clustering...")
    origins = pd.concat([data['ORIGIN_X'], data['ORIGIN_Y']], axis=1, keys=['ORIGIN_X', 'ORIGIN_Y'])
    destinations = pd.concat([data['DESTINATION_X'], data['DESTINATION_Y']], axis=1, keys=['DESTINATION_X', 'DESTINATION_Y'])
    kmeans = cluster.KMeans(n_clusters=14, init='random', n_init=100, max_iter=300)
    origin_clusters = origins
    origin_clusters["cluster_ID"] = kmeans.fit_predict(origins)
    destination_clusters = destinations
    destination_clusters["cluster_ID"] = kmeans.fit_predict(destinations)
    print("➡ Saving to csv...")
    origin_clusters.to_csv("origin_clusters.csv", index = False)
    destination_clusters.to_csv("destination_clusters.csv", index = False)


if __name__ == "__main__":
    """ Main method """
    process()