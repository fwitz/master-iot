#include <SPI.h>
#include <RH_RF95.h>
#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 3
RH_RF95 rf95(RFM95_CS, RFM95_INT);

uint8_t buf[225];
uint8_t len = sizeof(buf);
String message = "";
int i = 0;

void config(int power, float freq, float bandwidth, int spreadingFactor, int codeRate) {
  // Info http://www.semtech.com/images/datasheet/an1200.23.pdf
  // http://www.hoperf.com/upload/rf/RFM95_96_97_98W.pdf
  char reg0x1D, reg0x1E, reg0x26;

  if (!rf95.setFrequency(freq)) {
    Serial.println("setFrequency failed");
    while (1);
  }

  // PA_Boost false (+5 a +23dBm) et RFO true (-1 a +14dBm)
  if (power >= 5) rf95.setTxPower(power, false);
  else rf95.setTxPower(power, true);

  // 7,8 kHz; 10,4 kHz; 15,6 kHz; 20,8 kHz; 31,25 kHz; 41,7 kHz; 62,5 kHz; 125 kHz; 250 kHz; 500 kHz
  if (bandwidth == 500) reg0x1D = 0x90;
  else if (bandwidth == 250) reg0x1D = 0x80;
  else if (bandwidth == 125) reg0x1D = 0x70;
  else if (bandwidth == 62.5) reg0x1D = 0x60;
  else if (bandwidth == 41.7) reg0x1D = 0x50;
  else if (bandwidth == 31.25) reg0x1D = 0x40;
  else if (bandwidth == 20.8) reg0x1D = 0x30;
  else if (bandwidth == 15.6) reg0x1D = 0x20;
  else if (bandwidth == 10.4) reg0x1D = 0x10;
  else if (bandwidth == 7.8) reg0x1D = 0x00;
  else reg0x1D = 0x70; // BW=125kHz

  if (codeRate == 5) reg0x1D = reg0x1D | 2;
  else if (codeRate == 8) reg0x1D = reg0x1D | 8;
  else reg0x1D = reg0x1D || 2; // codeRate 4/5

  if (spreadingFactor >= 7) reg0x1E = spreadingFactor * 0x10 | 0x04; // CRC Enable
  else reg0x1E = 0x74; // SpreadingFactor=7

  if (spreadingFactor <= 10) reg0x26 = 0x00;
  else if (spreadingFactor <= 12) reg0x26 = 0x0c;
  else reg0x26 = 0x00; // Low SF without AGC (Receiver Automatic Gain control)

  Serial.println(reg0x1D);
  RH_RF95::ModemConfig modem_config = {
    reg0x1D, reg0x1E, reg0x26
  };
  //  RH_RF95::ModemConfig modem_config = {
  //    0x72, 0x74, 0x00
  //  };
  rf95.setModemRegisters(&modem_config);
  
}
void setup() {
  delay(1000);
  Serial.begin(115200); 
  while (!Serial);
  Serial.println("start");
  if(!rf95.init()) Serial.println("not working");
  //rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);
  //rf95.setFrequency(868.1);
  //rf95.setTxPower(10,false);
  config (8, 868.1, 125.0, 12, 5);

}


void loop() {
 message="LoRaPractice#azertyuiopqsdfg";
 message+=i;
 i++;
 message.toCharArray((char*)buf, message.length()+1);
 buf[message.length()+1] = 0;
 digitalWrite(LED_BUILTIN, HIGH);
 int start=millis();  
 rf95.send((uint8_t *)buf, message.length()+2);
 rf95.waitPacketSent();
 int stop=millis();
 Serial.print(stop-start);
 Serial.println(" ms");
 digitalWrite(LED_BUILTIN, LOW);
 Serial.print("Sent Message:");
 Serial.println(message);
 delay(2000);
}
