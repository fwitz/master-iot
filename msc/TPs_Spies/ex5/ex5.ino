#include <RTCZero.h>
RTCZero rtc;

void setup(){
  Serial.begin(115200);
  rtc.begin();
  rtc.enableAlarm(rtc.MATCH_SS); // Match seconds only
  rtc.attachInterrupt(alarmMatch); // Attach function to interrupt
  rf95.init();
  rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);
  //rf95.setModemConfig(RH_RF95::Bw125Cr48Sf4096); 
  // Bandwidth=125kHz Code rate 4/5 Spreading factor =2^7
  rf95.setFrequency(868.1); // 868.1, 868.2 or 868.3
}

void loop(){
  for(int i=0; i<2; i++){
    digitalWrite(LED_BUILTIN, HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN, LOW);
  }
  message="LoRa Practice #";
  message+=i;
  i++;
  message.toCharArray((char*)buf, message.length()+1);
  buf[message.length()+1] = 0;
  rf95.send((uint8_t *)buf, message.length()+2);
  rf95.waitPacketSent();
  Serial.print("Sent Message:");
  Serial.println(message);
  rtc.setAlarmSeconds((rtc.getSeconds()+5)%60); 
  rtc.standbyMode();    // Sleep until next alarm match
}
