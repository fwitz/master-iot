#include "BFSCode.hpp"

BFSCode::BFSCode(BlinkyBlocksBlock *host):BlinkyBlocksBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

    // Registers a callback (myGOFunc) to the message of type O
    addMessageEventFunc2(GO_MSG_ID,
                       std::bind(&BFSCode::myGOFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBACKFunc) to the message of type C
    addMessageEventFunc2(BACK_MSG_ID,
                       std::bind(&BFSCode::myBACKFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myNEIGHBOURSFunc) to the message of type C
    addMessageEventFunc2(NEIGHBOURS_MSG_ID,
                       std::bind(&BFSCode::myNEIGHBOURSFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

}

void BFSCode::startup() {
   console << "start " << module->blockId << "\n";
    if (isLeader) {
        module->setColor(RED);
        int id = module->blockId; //Getting the actual block id
        parent = id;
        // Send a GO message to all leader's neigbours to begin the spanning tree construction by distance.
        P2PNetworkInterface *neighbour;
        // We iterate through the interfaces of the block
        for (int i=0; i<hostBlock->getNbInterfaces();i++){
            neighbour=hostBlock->getInterface(i);
            // If the interface is connected, we add it to the toSend array and send to it the actual distance
            if(neighbour->connectedInterface){
                toSend.push_back(neighbour->getConnectedBlockId()); //Adding the neighbour id to the list of destination
                sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(0, id)), hostBlock->getInterface(i), 1000,100);
            }
        }
        std::copy(toSend.begin(), toSend.end(), std::back_inserter(waitingFrom)); //Waiting the same amount of message back as we sent
  	}
}
void BFSCode::myGOFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {

    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    P2PNetworkInterface *neighbour;
    int id = module->blockId;
    int d = msgData.first;
    // If the block has no parent
    if(parent == 0){
        parent = msgData.second;
        distance = d+1; // Increasing distance
        cout << "distance of block " << id << " from root is " << distance << "\n";
        module->setColor(distance);
        // Iterating through neighbours to know how many messages we have to send
        for(int i=0;i<hostBlock->getNbInterfaces();i++){
            neighbour=hostBlock->getInterface(i);
            if((neighbour->connectedInterface) && (neighbour != sender)){
                toSend.push_back(neighbour->getConnectedBlockId()); //Adding the neighbour id to the list of destination 
                std::cout << "Block " << id << " needs to send to " << neighbour->getConnectedBlockId() << "\n";
            }
        }
        // If their is no more message to send, we send BACK(stop) messagen, else we send BACK(continue)
        if(toSend.size() == 0){
            sendMessage("Back stop", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(0, id)), sender, 1000, 100);
        }else{
            sendMessage("Back continue", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(1, id)), sender, 1000, 100);
        }
    }else{
        // If the block already has a parent and it's the sender
        if(parent == msgData.second){
            // Sending a GO message to all neighbours of this block
            for(int i=0;i<toSend.size();i++){
                for(int j=0;j<hostBlock->getNbInterfaces();j++){
                    neighbour=hostBlock->getInterface(j);
                    if(neighbour->connectedInterface && neighbour->getConnectedBlockId() == toSend[i]){
                        sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(distance, id)), neighbour, 1000,100);
                    }
                }
            }
            std::copy(toSend.begin(), toSend.end(), std::back_inserter(waitingFrom)); // Waiting the same amount of messages as we sent
        }else{
            // Else we send BACK(no)
            sendMessage("Back no", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(2, id)), sender, 1000, 100);
        }
    }
}

void BFSCode::myBACKFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    P2PNetworkInterface *neighbour;
    int id = module->blockId;
    std::vector<int>::iterator position = std::find(waitingFrom.begin(), waitingFrom.end(), msgData.second);
    // If the element was found we erase it from the list
    if (position != waitingFrom.end()){
        waitingFrom.erase(position);
    }
    // If we receive a stop or a continue, we add the sender to the childs
    if(msgData.first == 0 || msgData.first == 1){
        cout << "Back message received on block " << id << " sent by block " << msgData.second << "\n";
        childs.push_back(msgData.second);
    }
    // If we receive a stop or a no, we remove the sender from the toSend list to ignore it
    if(msgData.first == 0 || msgData.first == 2){
        cout << "Block " << id << " need to send " << toSend.size() << " more messages \n";
        if(toSend.size() != 0){
            std::vector<int>::iterator position = std::find(toSend.begin(), toSend.end(), msgData.second);
            // If the element was found we erase it from the list
            if (position != toSend.end()){
                toSend.erase(position);
            }
        }
    }
    // If their is no more message to send
    if(toSend.size() == 0){
        // If the actual block is not the root, we send to all neighbours a stop message
        // With this method, the process will end when the root block receive a stop
        if(parent != id){
            for(int i=0;i<hostBlock->getNbInterfaces();i++){
                neighbour=hostBlock->getInterface(i);
                if((neighbour->connectedInterface) && (neighbour->getConnectedBlockId() == parent)){
                    sendMessage("Back stop", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(0, id)), neighbour, 1000, 100);
                }
            }
        }
    }else{
        cout << "Block " << id << " is waiting for " << waitingFrom.size() << " answers" << "\n";
        // If the block isn't waiting any other message
        if(waitingFrom.size() == 0){
            // If we're on the root block, we send a go to all neighbours
            if(parent == id){
                for(int i=0;i<toSend.size();i++){
                    for(int j=0;j<hostBlock->getNbInterfaces();j++){
                        neighbour=hostBlock->getInterface(j);
                        if(neighbour->connectedInterface && neighbour->getConnectedBlockId() == toSend[i]){
                            sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(distance, id)), neighbour, 1000,100);
                        }
                    }
                }
                std::copy(toSend.begin(), toSend.end(), std::back_inserter(waitingFrom));// Waiting the same amount of messages as we sent
            }else{
                // Else we send a BACK(continue) to all neighbours
                for(int j=0;j<hostBlock->getNbInterfaces();j++){
                    neighbour=hostBlock->getInterface(j);
                    if(neighbour->connectedInterface && neighbour->getConnectedBlockId() == parent){
                        sendMessage("Back continue", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(1, id)), neighbour, 1000, 100);
                    }
                }
            }
        }
    }
    
}

void BFSCode::myNEIGHBOURSFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    // This function does nothing but count the number of neighbours around the leader
}


void BFSCode::parseUserBlockElements(TiXmlElement *config) {
    const char *attr = config->Attribute("leader");
    if (attr!=nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl;
        isLeader = true;
    }
}

void BFSCode::onUserKeyPressed(unsigned char c, int x, int y) {
    switch (c) {
        case 'a' : // update with your code
        break;
    }
}

void BFSCode::onTap(int face) {
    std::cout << "Block 'tapped':" << module->blockId << std::endl; // complete with your code
}
