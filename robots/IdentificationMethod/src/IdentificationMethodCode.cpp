#include "IdentificationMethodCode.hpp"

IdentificationMethodCode::IdentificationMethodCode(BlinkyBlocksBlock *host):BlinkyBlocksBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

    // Registers a callback (myMESSAGEFunc) to the message of type E
    addMessageEventFunc2(MESSAGE_MSG_ID,
                       std::bind(&IdentificationMethodCode::myMESSAGEFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myUPDATEFunc) to the message of type E
    addMessageEventFunc2(UPDATE_MSG_ID,
                       std::bind(&IdentificationMethodCode::myUPDATEFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

}

void IdentificationMethodCode::startup() {
    console << "start " << module->blockId << "\n";
    P2PNetworkInterface *interface;
    // We iterate through the interfaces of the block to make the neighbours list
    for (int i=0; i<hostBlock->getNbInterfaces();i++){
        interface=hostBlock->getInterface(i);
        if(interface->connectedInterface){
            neighbours.push_back(interface);
        }
    }
    // Begining identification process by sending type 1 message
    if (isLeader && !isDiscovered) {
        for(auto &neighbour : neighbours ){
            sendMessage("Type 1",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(1, 0)), neighbour, 1000,100);
        }   
    }
}
void IdentificationMethodCode::myMESSAGEFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface *sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    if(msgData.first == 1){
        // If the block is already discovererd we sent a type 3 message to the sender
        if(isDiscovered == true){
            sendMessage("Type 3",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(3, 0)), sender, 1000,100);
        }else{
            // If not, we mark the block as discovered and save the parent, remove the sender from neighbours and send a type 2
            // message to the sender
            isDiscovered = true;
            parent = sender;
            removeNeighbour(sender);
            sendMessage("Type 2",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(2, 0)), sender, 1000,100);
            //If their is no more neighbours, we use the check() method for the subtreeSize. In the other case, we send a type 1 message
            //to all neighbours left
            if(neighbours.size() == 0 && !subtreeSizeSent){
                check();
            }else{
                for(auto &neighbour : neighbours ){
                    sendMessage("Type 1",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(1, 0)), neighbour, 1000,100);
                }
            }
        }
    }else if(msgData.first == 2){
        // Removing sender from neighbours and adding it to the childrens vector
        removeNeighbour(sender);
        children.push_back(sender);
    }else if(msgData.first == 3){
        // Remove the sender from the neighbours
        removeNeighbour(sender);
        // If their is no more neighbours check() for the subtreeSize
        if(neighbours.size() == 0 && subtreeSizeSent==false){
            check();
        }
    }else if(msgData.first == 4){
        // Updating child subtreeSize
        sendMessage("Update subtreeSize",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(1, msgData.second)), sender, 1000,100);
        // Updating actual block subtreeSize
        subtreeSize += msgData.second;
        // If their is no more neighbours check() for the subtreeSize
        if(neighbours.size() == 0 && subtreeSizeSent==false){
            check();
        }
    }else if(msgData.first == 5){
        uniqueID = msgData.second; // Setting block uniqueID
        int next_id = uniqueID + 1;
        // Propagating id's trough the tree
        for(auto &child : children ){
            sendMessage("Type 5",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(5, next_id)), child, 1000,100);
            next_id += getChildSubtreeSize(child);
        }
    }
}

void IdentificationMethodCode::myUPDATEFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface *sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    if(msgData.first == 1){
        subtreeSize = msgData.second;
    }else if(msgData.first == 2){
        sendMessage("Return subtree size",new MessageOf<pair<int,int>>(UPDATE_MSG_ID,make_pair(3, subtreeSize)), sender, 1000,100);
    }else if(msgData.first == 3){
        iChildSubtreeSize = msgData.second;
    }
}

int IdentificationMethodCode::getChildSubtreeSize(P2PNetworkInterface *child){
    sendMessage("Get subtreeSize",new MessageOf<pair<int,int>>(UPDATE_MSG_ID,make_pair(2, 0)), child, 1000,100);
    return iChildSubtreeSize;
}

void IdentificationMethodCode::removeNeighbour(P2PNetworkInterface *entity){
    auto it = std::find(neighbours.begin(), neighbours.end(), entity);
    if (it != neighbours.end()){
        neighbours.erase(it);
    }
}

void IdentificationMethodCode::check(){
    if(children.size() == 0 || receivedAll){
        if(isLeader==false){
            sendMessage("Type 4",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(4, subtreeSize)), parent, 1000,100);
            subtreeSizeSent = true;
        }else{
            uniqueID = 0;
            int next_id = 1;
            for(auto &child : children ){
                sendMessage("Type 5",new MessageOf<pair<int,int>>(MESSAGE_MSG_ID,make_pair(5, next_id)), child, 1000,100);
                next_id += getChildSubtreeSize(child);
            }
        }
    }
}

void IdentificationMethodCode::parseUserBlockElements(TiXmlElement *config) {
    const char *attr = config->Attribute("leader");
    if (attr!=nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl;
        isLeader = true;
    }
}

void IdentificationMethodCode::onUserKeyPressed(unsigned char c, int x, int y) {
    switch (c) {
        case 'a' : // update with your code
        break;
    }
}
