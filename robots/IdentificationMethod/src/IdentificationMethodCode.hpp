#ifndef IdentificationMethodCode_H_
#define IdentificationMethodCode_H_

#include "robots/blinkyBlocks/blinkyBlocksSimulator.h"
#include "robots/blinkyBlocks/blinkyBlocksWorld.h"
#include "robots/blinkyBlocks/blinkyBlocksBlockCode.h"

#include <vector>

static const int MESSAGE_MSG_ID = 1001;
static const int UPDATE_MSG_ID = 1002;

using namespace BlinkyBlocks;

class IdentificationMethodCode : public BlinkyBlocksBlockCode {
private:
	BlinkyBlocksBlock *module = nullptr;
  bool isDiscovered = false;
  bool isLeader = false;
  P2PNetworkInterface *parent;
  int uniqueID = -1;
  std::vector<P2PNetworkInterface *> children;
  std::vector<P2PNetworkInterface *> neighbours;
  int subtreeSize = 0;
  bool subtreeSizeSent = false;
  bool receivedAll = false;
  int iChildSubtreeSize = 0;
public :
	IdentificationMethodCode(BlinkyBlocksBlock *host);
	~IdentificationMethodCode() {};

/**
  * This function is called on startup of the blockCode, it can be used to perform initial
  *  configuration of the host or this instance of the program.
  * @note this can be thought of as the main function of the module
  */
    void startup() override;

/**
  * @brief Message handler for the message 'MESSAGE'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the mgetessage and that is connected to the sender
  */
   void myMESSAGEFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Message handler for the message 'UPDATE'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the mgetessage and that is connected to the sender
  */
   void myUPDATEFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

  void removeNeighbour(P2PNetworkInterface *entity);

  void check();

  int getChildSubtreeSize(P2PNetworkInterface *child);

/**
  * @brief Provides the user with a pointer to the configuration file parser, which can be used to read additional user information from each block config. Has to be overridden in the child class.
  * @param config : pointer to the TiXmlElement representing the block configuration file, all information related to concerned block have already been parsed
  *
  */
    void parseUserBlockElements(TiXmlElement *config) override;

/**
  * User-implemented keyboard handler function that gets called when
  *  a key press event could not be caught by openglViewer
  * @param c key that was pressed (see openglViewer.cpp)
  * @param x location of the pointer on the x axis
  * @param y location of the pointer on the y axis
  * @note call is made from GlutContext::keyboardFunc (openglViewer.h)
  */
    void onUserKeyPressed(unsigned char c, int x, int y) override;

/*****************************************************************************/
/** needed to associate code to module                                      **/
	static BlockCode *buildNewBlockCode(BuildingBlock *host) {
	    return(new IdentificationMethodCode((BlinkyBlocksBlock*)host));
	}
/*****************************************************************************/
};

#endif /* IdentificationMethodCode_H_ */