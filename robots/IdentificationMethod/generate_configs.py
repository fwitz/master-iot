import os
import random

class Bot:
    """ Class representing a bot instance """
    def __init__(self, x: int, y: int, z: int):
        self.__x = x
        self.__y = y
        self.__z = z
        self.__leader = False

    def to_xml_format(self) -> str:
        """ Method returning the bot position as xml string """
        return "position=\"" + str(self.__x) + "," + str(self.__y) + "," + str(self.__z) + "\""

    def make_leader(self):
        """ Method that define a bot as leader """
        self.__leader = True

    @property
    def is_leader(self) -> bool:
        """ Method that return if the bot is the leader """
        return self.__leader

def factors(n):
    """ Method returning factors of a number n """
    return [[i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0][-1]

def gen_fill(nb_bots: int) -> tuple[list[Bot], list[int]]:
    """ Method that generate a square of bots """
    bots_list: list[Bot] = []
    grid_size: list[int] = factors(nb_bots)
    leader_pos: int = random.randint(1, nb_bots) # Randomly choosing leader
    bot_id: int = 1
    # Generating a grid_size square of bot
    for i in range(grid_size[0]):
        for j in range(grid_size[1]):
            bots_list.append(Bot(i, j, 0))
            # Placing leader
            if bot_id == leader_pos:
                bots_list[-1].make_leader()
            bot_id += 1
    grid_size.append(10)
    return (bots_list, grid_size)

def gen_snake(nb_bots: int) -> tuple[list[Bot], list[int]]:
    """ Method that generate a bot snake """
    bots_list: list[Bot] = []
    factors_list: list[int] = factors(nb_bots)
    # Recomputing grid_size to match snake size
    grid_size: list[int] = [factors_list[0]-1, 2 * factors_list[1]]
    way: int = 1
    leader_pos: int = random.randint(1, nb_bots) # Randomly choosing leader
    bot_id: int = 1
    # For the number of lines
    for j in range(grid_size[1]):
        # Getting the start and stop depending on the way of the snake
        start: int = 0 if way == 1 else grid_size[0]-1
        stop: int = grid_size[0] if way == 1 else -1
        # If the line is even, creating an entire line of the snake
        # Else, just placing a single bot on the end column on the next line
        if j%2 == 0:
            for i in range(start, stop, way):
                bots_list.append(Bot(i, j, 0))
                # Placing leader
                if bot_id == leader_pos:
                    bots_list[-1].make_leader()
                bot_id += 1
        else:
            solo_index: int = [i for i in range(start, stop, way)][-1]
            bots_list.append(Bot(solo_index, j, 0))
            if bot_id == leader_pos:
                bots_list[-1].make_leader()
            bot_id += 1
            way = 1 if way == -1 else -1
    grid_size.append(10)
    return (bots_list, grid_size)

def gen_random(nb_bots: int) -> tuple[list[Bot], list[int]]:
    """ Method that generate a random shape of bots """
    bots_list: list[Bot] = []
    grid_size: list[int] = factors(nb_bots)

    return (bots_list, grid_size.append(10))

if __name__ == "__main__":
    """ Main method"""
    leader_color: str = "color=\"255,255,0\""
    leader_str: str = "leader=\"true\""
    nb_bots: list[int] = [100, 500, 1000, 5000, 10000, 25000, 40000]
    method: list[str] = ["fill", "snake"]
    for nb in nb_bots:
        for m in method:
            bots, grid_size = locals()["gen_" + m](nb)
            template: list[str] = []
            # Getting template
            with open("config_files/config_template.xml", 'r') as template_file:
                template = template_file.readlines()
            # Setting grid size in the config file
            template = [l.replace("gridSize", "gridSize=\"" + ",".join([str(i) for i in grid_size]) + "\"") if "gridSize" in l else l for l in template]
            # Adding all block to the config file
            template[4:4] = [
                "<block " + b.to_xml_format() + " " + leader_str + " " + leader_color + "/>\n" if b.is_leader
                else "<block " + b.to_xml_format() + "/>"
                for b in bots
            ]
            # Saving file
            filename: str = "config_" + m + "_" + str(nb) + ".xml"
            with open(os.path.join('config_files/', filename), 'w') as output_file:
                output_file.writelines(template)

