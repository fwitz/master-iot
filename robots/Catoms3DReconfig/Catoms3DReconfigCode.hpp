#ifndef Catoms3DReconfigCode_H_
#define Catoms3DReconfigCode_H_

#include "robots/catoms3D/catoms3DSimulator.h"
#include "robots/catoms3D/catoms3DWorld.h"
#include "robots/catoms3D/catoms3DBlockCode.h"
#include "robots/catoms3D/catoms3DMotionEngine.h"
#include "robots/catoms3D/catoms3DRotationEvents.h"

static const int GO_MSG_ID = 1001;
static const int BACK_MSG_ID = 1002;
static const int CHECKMOVES_MSG_ID = 1003;
static const int BACKCHECKMOVES_MSG_ID = 1004;
static const int MOVE_MSG_ID = 1005;
static const int BACKMOVE_MSG_ID = 1006;
static const int PATH_MSG_ID = 1007;
static const int BACKPATH_MSG_ID = 1008;
static const int GENERATETREE_MSG_ID = 1009;
static const int NEIGHBOURS_MSG_ID = 1010;

using namespace Catoms3D;

#include <vector>
#include <tuple>
#include <iostream>
#include <string>

class Catoms3DReconfigCode : public Catoms3DBlockCode {
private:
	Catoms3DBlock *module = nullptr;
  int id = 0;
  bool isLeader = false;
  int parent = 0;
  std::vector<int> childs;
  int nbWaitedAnswers = 0;
  int distanceToTarget = 0;
  int distanceToLeader = 0;
  bool canMove = false;
  bool shouldMove = false;
  bool inTarget = false;
  bool bridgeCross = false;
  int nbGeneration = 0;
  std::vector<pair<int, int>> moveOrder;
  std::vector<int> finalPath;
public :
	Catoms3DReconfigCode(Catoms3DBlock *host);
	~Catoms3DReconfigCode() {};

/**
  * This function is called on startup of the blockCode, it can be used to perform initial
  *  configuration of the host or this instance of the program.
  * @note this can be thought of as the main function of the module
  */
    void startup() override;

/**
  * @brief Message handler for the message 'Neighbours'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
  void myNeighboursFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender);

/**
  * @brief Message handler for the message 'GoTree'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myGoFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Message handler for the message 'BackTree'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myBackFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);


/**
  * @brief Message handler for the message 'CanMove'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myCheckMovesFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Message handler for the message 'BackCanMove'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myBackCheckMovesFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

   /**
  * @brief Message handler for the message 'CanMove'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myPathFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Message handler for the message 'BackCanMove'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myBackPathFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Message handler for the message 'Move'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myMoveFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender);

/**
  * @brief Message handler for the message 'GenerateTree'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myGenerateTreeFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender);

   template <typename T>
   void sendMsgToChild(char* description, int msgID, T data);

   template <typename T>
   void sendMsgTo(char* description, int msgId, int destination, T data);

   int findNeighborPort(const Catoms3DBlock *neighbor);

   void move(std::vector<int> path);

   bool isOnPath(std::vector<int> path, int next);
/**
  * @brief Provides the user with a pointer to the configuration file parser, which can be used to read additional user information from each block config. Has to be overridden in the child class.
  * @param config : pointer to the TiXmlElement representing the block configuration file, all information related to concerned block have already been parsed
  *
  */
    void parseUserBlockElements(TiXmlElement *config) override;

/**
  * @brief Callback function executed whenever the module finishes a motion
  */
    void onMotionEnd() override;

/**
  * Called by openglviewer during interface drawing phase, can be used by a user
  *  to draw a custom Gl string onto the bottom-left corner of the GUI
  * @note call is made from OpenGlViewer::drawFunc
  * @return a string (can be multi-line with `
`) to display on the GUI
  */
    string onInterfaceDraw() override;

/*****************************************************************************/
/** needed to associate code to module                                      **/
	static BlockCode *buildNewBlockCode(BuildingBlock *host) {
	    return(new Catoms3DReconfigCode((Catoms3DBlock*)host));
	}
/*****************************************************************************/
};

#endif /* 3DCatomReconfigCode_H_ */