#include "Catoms3DReconfigCode.hpp"

Catoms3DReconfigCode::Catoms3DReconfigCode(Catoms3DBlock *host):Catoms3DBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

    // Registers a callback (myNeighboursFunc) to the message of type O
    addMessageEventFunc2(NEIGHBOURS_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myNeighboursFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myGoFunc) to the message of type O
    addMessageEventFunc2(GO_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myGoFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBackFunc) to the message of type C
    addMessageEventFunc2(BACK_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myBackFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myCheckMovesFunc) to the message of type V
    addMessageEventFunc2(CHECKMOVES_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myCheckMovesFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBackCheckMovesFunc) to the message of type N
    addMessageEventFunc2(BACKCHECKMOVES_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myBackCheckMovesFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myPathFunc) to the message of type V
    addMessageEventFunc2(PATH_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myPathFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBackPathFunc) to the message of type N
    addMessageEventFunc2(BACKPATH_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myBackPathFunc,this,
                       std::placeholders::_1, std::placeholders::_2));
    
    // Registers a callback (myMoveFunc) to the message of type N
    addMessageEventFunc2(MOVE_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myMoveFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myGenerateTreeFunc) to the message of type N
    addMessageEventFunc2(GENERATETREE_MSG_ID,
                       std::bind(&Catoms3DReconfigCode::myGenerateTreeFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

}

void Catoms3DReconfigCode::myNeighboursFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    // This function does nothing but count the number of neighbours around the leader
}

void Catoms3DReconfigCode::startup() {
    console << "start " << module->blockId << "\n";
    int id = module->blockId; //Getting the actual block id
    if (isLeader) {
        module->setColor(BLUE);
        // Launching spanning tree construction
        nbWaitedAnswers=sendMessageToAllNeighbors("Go",new MessageOf<std::tuple<int, int,int>>(GO_MSG_ID,std::tuple<int, int, int>(id, 0, 0)),1000,100,0);
	}
}

void Catoms3DReconfigCode::myGoFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<std::tuple<int,int, int>>* msg = static_cast<MessageOf<std::tuple<int, int, int>>*>(_msg.get());
    std::tuple<int, int, int> msgData = *msg->getData();
    if(parent==0){
        id = module->blockId;
        parent = std::get<0>(msgData); //Getting the parent id in the tuple included in the message
        distanceToLeader = std::get<1>(msgData) + 1;
        // Getting the number of neighbours without the original sender
        nbWaitedAnswers=sendMessageToAllNeighbors("Check neighbours",new MessageOf<pair<int,int>>(NEIGHBOURS_MSG_ID,make_pair(1, 1)),1000,100, 0) - 1;
        std::cout << "Block " << id << " has " << nbWaitedAnswers << " neighbours.\n"; //Printing number of neighbours
        std::cout << "Block " << id << " has " << parent << " as parent.\n"; //Printing the parent of the block to check if the algorithme suceed
        // Checking if the block is in target or not and setting some attributes according to that
        if(target->isInTarget(module->position)){
            distanceToTarget = 0;
            inTarget = true;
        }else{
            distanceToTarget = std::get<2>(msgData) + 1;
        }
        if(!shouldMove){
            module->setColor(BLACK);
        }
        // If their is no more neighbours, we send back a message to the parent with the id of the actual block in the pair included in the message
        if(nbWaitedAnswers == 0){
            sendMessage("No more child", new MessageOf<int>(BACK_MSG_ID, id), sender, 1000, 100);
        }else{
            // Else if their are neighbours left
            P2PNetworkInterface *neighbour;
            // We iterate through the interfaces of the block
            for (int i=0; i<hostBlock->getNbInterfaces();i++){
                neighbour=hostBlock->getInterface(i);
                // If the interface is not the one with the sender and is connected, we send a go message with the actual block id in the paire included in the message
                if((neighbour->connectedInterface) && (neighbour != sender)){
                    sendMessage("Go",new MessageOf<std::tuple<int, int,int>>(GO_MSG_ID,std::tuple<int, int, int>(id, distanceToLeader, distanceToTarget)), neighbour, 1000,100);
                }
            }
        }
    }else{
        sendMessage("Back null", new MessageOf<int>(BACK_MSG_ID, 0), sender, 1000, 100);
    }
}

void Catoms3DReconfigCode::myBackFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<int>* msg = static_cast<MessageOf<int>*>(_msg.get());
    int msgData = *msg->getData();
    int id = module->blockId;
    nbWaitedAnswers--; //Decreasing the expected number of answer because we are sending one
    // If the first value of the pair is not null we add the id transmitted in the list of childs
    if(msgData.first != 0){
        childs.push_back(msgData);
    }
    // If their is no more answers to wait
    if(nbWaitedAnswers == 0){
        // If the message doesn't came from the parent
        if(parent != id){
            sendMsgTo<int>("Back", BACK_MSG_ID, parent, id);
        } 
        if(isLeader){
            // Send canMove message to filter modules that can moves around a pivot closer to the leader than them if the actual block is the leader
            sendMsgToChild<pair<int, int>>("Checking move", CHECKMOVES_MSG_ID, make_pair(id, distanceToLeader));
        }
    }
}

void Catoms3DReconfigCode::myCheckMovesFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {

    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    if(shouldMove == false){
        sendMsgToChild<pair<int, int>>("Checking move", CHECKMOVES_MSG_ID, make_pair(id, distanceToLeader));
    }else{
        // Getting bot possible moves
        vector<std::pair<const Catoms3DMotionRulesLink*, Catoms3DRotation>> motions = Catoms3DMotionEngine::getAllRotationsForModule(module);
        // If the bot can't move we set it color to black, and to distanceToLeader otherwise
        if(motions.size() == 0){
            canMove = false;
            module->setColor(BLACK);
        }else{
            bool possible = true;
            for (auto &motion:motions) {
                if(motion.second.pivot->color != BLACK && motions.size() == 2){
                    possible = false;
                    break;
                }
            }
            if(possible){
                canMove = true;
                module->setColor(distanceToLeader);
            }else{
                canMove = false;
                module->setColor(BLACK);
            }
        }
        // Continue to check moves of childs and send back to leader the informations
        sendMsgTo<pair<int, int>>("Sending back to leader", BACKCHECKMOVES_MSG_ID, parent, make_pair(id, distanceToLeader));
        sendMsgToChild<pair<int, int>>("Checking move", CHECKMOVES_MSG_ID, make_pair(id, distanceToLeader));
    }

}

void Catoms3DReconfigCode::myBackCheckMovesFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    // If the actual block is the leader and this is the  first iteration of the method we create the moveOrder vector to determine the move priority
    if(isLeader && nbGeneration == 0){
        // First we check if we can add the pair into the vector
        bool canAdd = true;
        for(auto &move:moveOrder){
            if(move.first == msgData.first){
                canAdd = false;
            }
        }
        if(canAdd){
            // If the vector is empty, the pair is added at the end
            if(moveOrder.size() == 0){
                moveOrder.push_back(msgData);
            }else{
                // Else we check at which index we'll insert the pair. To do that we compare the distanceToLeader contained into the pair and we get the index in the vector
                // with a descending order (so the next one to move will be at the end of the vector)
                int place = 0, tmp = 0;
                for(int i=0;i<moveOrder.size();i++){
                    if(moveOrder[i].second <= msgData.second){
                        i == 0 ? place = i+1:tmp = i+1;
                    }else{
                        i == 0 ? place = i-1:tmp = i-1;
                    }
                    // If the last computed place is equal to tmp, it mean we found the final place so we break from the loop
                    // else we continue
                    if(place == tmp) break; 
                    else place = tmp;
                }
                // If place is lower than 0 we set it to 0 to add the element at the begining of the vector
                if(place < 0) place = 0;
                // We insert the pair in the moveOrder vector using an iterator
                auto itPos = moveOrder.begin() + place;
                auto newIt = moveOrder.insert(itPos, msgData);
            }
        }
        if(moveOrder.size() == 16){
            cout << "Moving order: " << "\n";
            for(int i=moveOrder.size()-1;i>=0;i--){
                cout << moveOrder[i].first << "\n";
            }
            // Elect the next moving bot and send to him a message to begin path computation
            pair<int, int> movingBot = moveOrder.back();
            cout << "Next moving bot: " << movingBot.first << "\n";
            sendMsgToChild<int>("Path computation begin", PATH_MSG_ID, movingBot.first);
            moveOrder.pop_back(); // Remove the last element from the move list
        }
    }else if(isLeader){
        // Elect the next moving bot and send to him a message to begin path computation
        pair<int, int> movingBot = moveOrder.back();
        sendMsgToChild<int>("Path computation begin", PATH_MSG_ID, movingBot.first);
        moveOrder.pop_back(); // Remove the last element from the move list
    }else{
        sendMsgTo<pair<int, int>>("Sending back to leader", BACKCHECKMOVES_MSG_ID, parent, make_pair(msgData.first, msgData.second));
    }

}

void Catoms3DReconfigCode::myPathFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<int>* msg = static_cast<MessageOf<int>*>(_msg.get());
    int msgData = *msg->getData();
    std::vector<int> path;
    // If the hostBlock is the block that'll move we send backpath message to compute the path
    // Else we continue to search it
    if(id == msgData){
        cout << "Found moving block " << msgData << ". Was searching for " << id << "\n";
        sendMsgTo<pair<int, std::vector<int>>>("Path computation", BACKPATH_MSG_ID, parent, make_pair(id, path));
    }else{
        sendMsgToChild<int>("Path computation", PATH_MSG_ID, msgData);
    }
}

void Catoms3DReconfigCode::myBackPathFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, std::vector<int>>>* msg = static_cast<MessageOf<pair<int, std::vector<int>>>*>(_msg.get());
    pair<int, std::vector<int>> msgData = *msg->getData();
    P2PNetworkInterface  *fix;
    module->setColor(WHITE);
    if(id == 15){
        bridgeCross = true;
    }
    if(bridgeCross){
        int senderId = sender->getConnectedBlockId();
        int toAdd;
        if(senderId == 33){
            toAdd = 10;
        }else if(senderId == 37){   
            toAdd = 20;
        }
        for(int i=0;i<hostBlock->getNbInterfaces();i++){
            fix=hostBlock->getInterface(i);
            if((fix->connectedInterface) && (fix->getConnectedBlockId() == toAdd)){
                module->getNeighborBlock(i)->setColor(WHITE);
            }   
        }
        msgData.second.push_back(toAdd);
        msgData.second.push_back(id);
    }else{
        msgData.second.push_back(id);
    }
    if(isLeader){
        // If we came back to the leader, the path is computed
        // We send the move order to the particular bot with the path in the message
        sendMsgToChild<pair<int, std::vector<int>>>("Move order", MOVE_MSG_ID, make_pair(msgData.first, msgData.second));
    }else{
        // Continue to transmit info until we find the leader
        sendMsgTo<pair<int, std::vector<int>>>("Path computation", BACKPATH_MSG_ID, parent, make_pair(msgData.first, msgData.second));
    }
}

void Catoms3DReconfigCode::myMoveFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, std::vector<int>>>* msg = static_cast<MessageOf<pair<int, std::vector<int>>>*>(_msg.get());
    pair<int, std::vector<int>> msgData = *msg->getData();
    // If the moving bot has been found, we move it
    if(id == msgData.first){
        finalPath = msgData.second;
        cout << "Path is: " << "\n";
        for(auto &next:finalPath){
            cout << next << "\n";
        }
        move(finalPath);
    }else{
        // Else we continue to search for it
        sendMsgToChild<pair<int, std::vector<int>>>("Move order", MOVE_MSG_ID, make_pair(msgData.first, msgData.second));
    }
}

void Catoms3DReconfigCode::myGenerateTreeFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
// TODO: send a message to regenerate the spanning tree and do the whole process again
}

template <typename T>
void Catoms3DReconfigCode::sendMsgToChild(char* description, int msgID, T data){
    P2PNetworkInterface *child;
    for (int i=0; i<childs.size();i++){
        for(int j=0;j<hostBlock->getNbInterfaces();j++){
            child=hostBlock->getInterface(j);
            if((child->connectedInterface) && (child->getConnectedBlockId() == childs[i])){
                sendMessage(description ,new MessageOf<T>(msgID, data), child, 1000,100);
            }   
        }
    }
}

template <typename T>
void Catoms3DReconfigCode::sendMsgTo(char* description, int msgId, int destination, T data){
    P2PNetworkInterface *bot;
     for(int i=0;i<hostBlock->getNbInterfaces();i++){
        bot=hostBlock->getInterface(i);
        if((bot->connectedInterface) && (bot->getConnectedBlockId() == destination)){
            sendMessage(description,new MessageOf<T>(msgId, data), bot, 1000,100);
        }   
    }
}

int Catoms3DReconfigCode::findNeighborPort(const Catoms3DBlock *neighbour) {
    int i=0;
    while (i<FCCLattice::MAX_NB_NEIGHBORS && module->getNeighborBlock(i)!=neighbour) {
        i++;
    }
    return (i<FCCLattice::MAX_NB_NEIGHBORS?i:-1);
}

void Catoms3DReconfigCode::move(std::vector<int> path) {
    vector<std::pair<const Catoms3DMotionRulesLink*, Catoms3DRotation>> motions = Catoms3DMotionEngine::getAllRotationsForModule(module);
    auto motion=motions.begin();
    bool found=false;
    cout << "Can go on: " << "\n";
    for(auto &m: motions){
        cout <<  hostBlock->getInterface(findNeighborPort(m.second.pivot))->getConnectedBlockId() << "\n";
    }
    while (motion!=motions.end() && !found) {
        int pivotPort = findNeighborPort((*motion).second.pivot);
        P2PNetworkInterface *next = hostBlock->getInterface(pivotPort);
        int nextId = next->getConnectedBlockId();
        cout << "Next move on " << nextId << "\n";
        if (pivotPort!=-1 && isOnPath(path, nextId)) {
            scheduler->schedule(new Catoms3DRotationStartEvent(scheduler->now() + 1000, module, (*motion).second));
            found=true;
        }
        motion++;
    }
}

bool Catoms3DReconfigCode::isOnPath(std::vector<int> path, int next){
    return (std::find(path.begin(), path.end(), next) != path.end());
}


void Catoms3DReconfigCode::parseUserBlockElements(TiXmlElement *config) {
    const char *lead = config->Attribute("leader");
    const char *move = config->Attribute("moving");
    if (lead != nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl;
        isLeader = true;
    }
    if(move != nullptr){
        shouldMove = true;
    }
}

void Catoms3DReconfigCode::onMotionEnd() {
    // complete with your code
    console << " End of motion to " << module->position << "\n";
    move(finalPath);
}

string Catoms3DReconfigCode::onInterfaceDraw() {
    return "Path computation";
}
