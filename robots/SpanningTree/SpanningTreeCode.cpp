#include "SpanningTreeCode.hpp"

SpanningTreeCode::SpanningTreeCode(BlinkyBlocksBlock *host):BlinkyBlocksBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

    // Registers a callback (myGOFunc) to the message of type O
    addMessageEventFunc2(GO_MSG_ID,
                       std::bind(&SpanningTreeCode::myGOFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBACKFunc) to the message of type C
    addMessageEventFunc2(BACK_MSG_ID,
                       std::bind(&SpanningTreeCode::myBACKFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

     // Registers a callback (myNEIGHBOURSFunc) to the message of type C
    addMessageEventFunc2(NEIGHBOURS_MSG_ID,
                       std::bind(&SpanningTreeCode::myNEIGHBOURSFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

}

void SpanningTreeCode::startup() {
    console << "start " << module->blockId << "\n";
    if (isLeader) {
        module->setColor(RED);
        int id = module->blockId; //Getting the actual block id
        // Send a GO message to all leader's neigbours to begin the spanning tree construction. We transmit in the pair the id of the leader
        nbWaitedAnswers=sendMessageToAllNeighbors("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(id, 1)),1000,100,0);
  	}
}
void SpanningTreeCode::myGOFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    if(parent==0){
        parent = msgData.first; //Getting the parent id in the pair included in the message
        int id = module->blockId;
        // Getting the number of neighbours without the original sender
        nbWaitedAnswers=sendMessageToAllNeighbors("Check neighbours",new MessageOf<pair<int,int>>(NEIGHBOURS_MSG_ID,make_pair(1, 1)),1000,100, 0) - 1;
        std::cout << "Block " << id << " has " << nbWaitedAnswers << " neighbours.\n"; //Printing number of neighbours
        std::cout << "Block " << id << " has " << parent << " as parent.\n"; //Printing the parent of the block to check if the algorithme suceed
        module->setColor(parent);
        // If their is no more neighbours, we send back a message to the parent with the id of the actual block in the pair included in the message
        if(nbWaitedAnswers == 0){
            sendMessage("No more child", new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(id, 1)), sender, 1000, 100);
        }else{
            // Else if their are neighbours left
            P2PNetworkInterface *neighbour;
            // We iterate through the interfaces of the block
            for (int i=0; i<hostBlock->getNbInterfaces();i++){
                neighbour=hostBlock->getInterface(i);
                // If the interface is not the one with the sender and is connected, we send a go message with the actual block id in the paire included in the message
                if((neighbour->connectedInterface) && (neighbour != sender)){
                    sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(id, 1)), neighbour, 1000,100);
                }
            }
        }
    }else{
        sendMessage("Back null", new MessageOf<pair<int, int>>(BACK_MSG_ID,make_pair(0, 1)), sender, 1000, 100);
    }
}

void SpanningTreeCode::myBACKFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    int id = module->blockId;
    nbWaitedAnswers--; //Decreasing the expected number of answer because we are sending one
    // If the first value of the pair is not null we add the id transmitted in the list of childs
    if(msgData.first != 0){
        childs.push_back(msgData.first);
    }
    // If their is no more answers to wait
    if(nbWaitedAnswers == 0){
        // If the message doesn't come from the parent
        if(parent != id){
            P2PNetworkInterface *neighbour;
            // We iterate through the interfaces of the block
            for (int i=0; i<hostBlock->getNbInterfaces();i++){
                neighbour=hostBlock->getInterface(i);
                // If the interface is the parent and connected, we transmit to it the actual id
                if((neighbour->connectedInterface) && (neighbour->getConnectedBlockId() == parent)){
                    sendMessage("Back",new MessageOf<pair<int,int>>(BACK_MSG_ID,make_pair(id, 1)), neighbour, 1000,100);
                }
            }
        }
    }
}

void SpanningTreeCode::myNEIGHBOURSFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    // This function does nothing but count the number of neighbours around the leader
}

void SpanningTreeCode::parseUserBlockElements(TiXmlElement *config) {
    const char *attr = config->Attribute("leader");
    if (attr!=nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl;
        isLeader=true;
    }
}

void SpanningTreeCode::onUserKeyPressed(unsigned char c, int x, int y) {
    switch (c) {
        case 'a' : // update with your code
        break;
    }
}

void SpanningTreeCode::onTap(int face) {
    std::cout << "Block 'tapped':" << module->blockId << std::endl; // complete with your code
}
