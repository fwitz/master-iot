#ifndef ReconfigurationCode_H_
#define ReconfigurationCode_H_

#include "robots/hexanodes/hexanodesSimulator.h"
#include "robots/hexanodes/hexanodesWorld.h"
#include "robots/hexanodes/hexanodesBlockCode.h"
#include "robots/hexanodes/hexanodesMotionEvents.h"
#include "robots/hexanodes/hexanodesMotionEngine.h"

static const int MOVE_MSG_ID = 1001;

using namespace Hexanodes;

class ReconfigurationCode : public HexanodesBlockCode {
private:
  bool isLeader = false;
  bool rightPlace = false;
	HexanodesBlock *module = nullptr;
public :
	ReconfigurationCode(HexanodesBlock *host);
	~ReconfigurationCode() {};

/**
  * This function is called on startup of the blockCode, it can be used to perform initial
  *  configuration of the host or this instance of the program.
  * @note this can be thought of as the main function of the module
  */
    void startup() override;

/**
  * @brief Message handler for the message 'MOVE'
  * @param _msg Pointer to the message received by the module, requires casting
  * @param sender Connector of the module that has received the message and that is connected to the sender
  */
   void myMOVEFunc(std::shared_ptr<Message>_msg,P2PNetworkInterface *sender);

/**
  * @brief Provides the user with a pointer to the configuration file parser, which can be used to read additional user information from each block config. Has to be overridden in the child class.
  * @param config : pointer to the TiXmlElement representing the block configuration file, all information related to concerned block have already been parsed
  *
  */
    void parseUserBlockElements(TiXmlElement *config) override;

/**
  * @brief Callback function executed whenever the module finishes a motion
  */
    void onMotionEnd() override;

    std::vector<Cell3DPosition> findPossiblePositions();
    pair<bool,Cell3DPosition> isConnector();

/**
  * User-implemented keyboard handler function that gets called when
  *  a key press event could not be caught by openglViewer
  * @param c key that was pressed (see openglViewer.cpp)
  * @param x location of the pointer on the x axis
  * @param y location of the pointer on the y axis
  * @note call is made from GlutContext::keyboardFunc (openglViewer.h)
  */
    void onUserKeyPressed(unsigned char c, int x, int y) override;

/**
  * @brief This function is called when a module is tapped by the user. Prints a message to the console by default.
     Can be overloaded in the user blockCode
  * @param face face that has been tapped
  */
    void onTap(int face) override;

/**
  * Called by openglviewer during interface drawing phase, can be used by a user
  *  to draw a custom Gl string onto the bottom-left corner of the GUI
  * @note call is made from OpenGlViewer::drawFunc
  * @return a string (can be multi-line with `
`) to display on the GUI
  */
    string onInterfaceDraw() override;

/*****************************************************************************/
/** needed to associate code to module                                      **/
	static BlockCode *buildNewBlockCode(BuildingBlock *host) {
	    return(new ReconfigurationCode((HexanodesBlock*)host));
	}
/*****************************************************************************/
};

#endif /* ReconfigurationCode_H_ */