#include "ReconfigurationCode.hpp"

ReconfigurationCode::ReconfigurationCode(HexanodesBlock *host):HexanodesBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

}

void ReconfigurationCode::startup() {
    // If bot already on a final position, set it's color to green
    if (target && target->isInTarget(module->position)) {
      setColor(GREEN);
      rightPlace = true;
    }else{
        // Else, we count the number of possible moves. If their are none, set the color as gray, blue if their is exactly one and orange if more
        vector<HexanodesMotion*> tab = Hexanodes::getWorld()->getAllMotionsForModule(module);
        int n=0;
        for (auto motion:tab) {
            Cell3DPosition destination = motion->getFinalPos(module->position);
            if (lattice->isInGrid(destination)) n++;
        }
        setColor(n==0?GREY:(n==1?BLUE:ORANGE));
    }
    HexanodesWorld *wrl = Hexanodes::getWorld();
    // Leader only will do this action
    if (isLeader){ 
        int furthest = 14;
        bool end = false;
        int index = 1;
        ReconfigurationCode* testNode = new ReconfigurationCode(wrl->getBlockById(index));
        console << testNode->module->blockId << "\n";
        while(end == false){
            /*testNode = new ReconfigurationCode(wrl->getBlockById(index));
            if(testNode == nullptr){
                furthest = index;
                end = true;
            }
            index ++;*/
            end = true;
        }
        // Getting the furthest bot from leader on the world
        ReconfigurationCode* toMove = new ReconfigurationCode(wrl->getBlockById(furthest));
        // Getting the list of valid rotations for the furthest bot
        vector<HexanodesMotion*> tab = wrl->getAllMotionsForModule(toMove->module);
        // Searching for first ClockWise rotations in the list
        vector<HexanodesMotion*>::const_iterator ci=tab.begin();
        while (ci!=tab.end() && (*ci)->direction!=motionDirection::CW) {
                ci++;
        }
        //If their is a CW rotation, we start the move of the furthest bot
        if (ci!=tab.end()) {
            auto orient = (*ci)->getFinalOrientation(toMove->module->orientationCode);
            Cell3DPosition destination = (*ci)->getFinalPos(toMove->module->position);
            scheduler->schedule(new HexanodesMotionStartEvent(scheduler->now()+100000, toMove->module,destination,orient));
        }
    }

}


void ReconfigurationCode::parseUserBlockElements(TiXmlElement *config) {
    const char *attr = config->Attribute("leader");
    if (attr!=nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl; // complete with your code
        isLeader = true; // Setting the bot as leader
    }
}

void ReconfigurationCode::onMotionEnd() {
    // Getting the world to manipulate it later
    HexanodesWorld *wrl = Hexanodes::getWorld();
    // If the block arrived on a final position, we set its color to green and launch the move of the next bot
    if (target && target->isInTarget(module->position) && rightPlace == false) {
        // Getting the list of valid rotation for this bot in its actual position
        vector<HexanodesMotion*> tab = wrl->getAllMotionsForModule(module);
        // Searching for the first ClockWise rotations in the list
        vector<HexanodesMotion*>::const_iterator ci=tab.begin();
        while (ci!=tab.end() && (*ci)->direction!=motionDirection::CW) {
            ci++;
        }
        //If their is a CW rotation, we start to move this bot again
        if (ci!=tab.end()) {
            auto orient = (*ci)->getFinalOrientation(module->orientationCode);
            Cell3DPosition destination = (*ci)->getFinalPos(module->position);
            // Getting all possible position for the bot that are in the target area and free
            std::vector<Cell3DPosition> possiblePos = findPossiblePositions();
            int noCounter = 0;
            // Checking for each of these possible position if one is equal to the bot destination
            // If yes, we move the bot. Otherwise we increment a counter that will help us to know if the bot is
            // at the right place
            cout << "Destination " << destination << "\n";
            for(int i=0;i<possiblePos.size();i++){
                cout << "Possible pos " << (module->position + possiblePos[i]) << "\n";
                if(target->isInTarget((module->position + possiblePos[i])) && (module->position + possiblePos[i]) == destination){
                    scheduler->schedule(new HexanodesMotionStartEvent(scheduler->now()+100000, module,destination,orient));
                }else{          
                    noCounter++;
                }

            }
            if(noCounter == possiblePos.size()){
                rightPlace = true;
            }
        }
        if(rightPlace == true){
            setColor(GREEN);
            ReconfigurationCode* next = new ReconfigurationCode(wrl->getBlockById(module->blockId - 1));
            cout << "Block " << module->blockId-1 << " is in the right place: " << next->rightPlace << "\n";
            if(next->rightPlace == false){
                  // Getting the list of valid rotation for the next bot
                vector<HexanodesMotion*> tab = wrl->getAllMotionsForModule(next->module);
                // Searching for the first ClockWise rotations in the list
                vector<HexanodesMotion*>::const_iterator ci=tab.begin();
                while (ci!=tab.end() && (*ci)->direction!=motionDirection::CW) {
                    ci++;
                }
                // If their is a CW rotation, we start the move of the next bot
                if (ci!=tab.end()) {
                    auto orient = (*ci)->getFinalOrientation(next->module->orientationCode);
                    Cell3DPosition destination = (*ci)->getFinalPos(next->module->position);
                    scheduler->schedule(new HexanodesMotionStartEvent(scheduler->now()+100000, next->module,destination,orient));
                }
            }
        }
    }else{
            // Setting the color of this block to orange as its not on a final position
        setColor(ORANGE);
        // Getting the list of valid rotation for this bot in its actual position
        vector<HexanodesMotion*> tab = wrl->getAllMotionsForModule(module);
        // Searching for the first ClockWise rotations in the list
        vector<HexanodesMotion*>::const_iterator ci=tab.begin();
        while (ci!=tab.end() && (*ci)->direction!=motionDirection::CW) {
            ci++;
        }
        //If their is a CW rotation, we start to move this bot again
        if (ci!=tab.end()) {
            auto orient = (*ci)->getFinalOrientation(module->orientationCode);
            Cell3DPosition destination = (*ci)->getFinalPos(module->position);
            scheduler->schedule(new HexanodesMotionStartEvent(scheduler->now()+100000, module,destination,orient));
        }
    }
}

std::vector<Cell3DPosition> ReconfigurationCode::findPossiblePositions(){
    std::vector<Cell3DPosition> pos;
    for(int i=0;i< HHLattice::MAX_NB_NEIGHBORS; i++){
        if(lattice->isFree(module->position + (static_cast<HHLattice *>(lattice))->getNeighborRelativePos(static_cast<HHLattice::Direction>(i)))
            && target->isInTarget(module->position + (static_cast<HHLattice *>(lattice))->getNeighborRelativePos(static_cast<HHLattice::Direction>(i)))
        ){
            pos.push_back((static_cast<HHLattice *>(lattice))->getNeighborRelativePos(static_cast<HHLattice::Direction>(i)));
        }
    }
    return pos;
}

pair<bool,Cell3DPosition> ReconfigurationCode::isConnector() {
    int n = 0;
    while (n < HHLattice::MAX_NB_NEIGHBORS &&
            ( !lattice->isFree(module->position + (static_cast<HHLattice *>(lattice))->getNeighborRelativePos(
                    static_cast<HHLattice::Direction>(n)))
                    ||
           !target->isInTarget(module->position + (static_cast<HHLattice *>(lattice))->getNeighborRelativePos(
                   static_cast<HHLattice::Direction>(n))))) {
        n++;
    }
    return (n < HHLattice::MAX_NB_NEIGHBORS?
      make_pair(true,module->position + (static_cast<HHLattice *>(lattice))->getNeighborRelativePos(
              static_cast<HHLattice::Direction>(n)))
              :make_pair(false,Cell3DPosition(0,0,0)));
}

void ReconfigurationCode::onUserKeyPressed(unsigned char c, int x, int y) {
    switch (c) {
        case 'a' : // update with your code
        break;
    }
}

void ReconfigurationCode::onTap(int face) {
    std::cout << "Block 'tapped':" << module->blockId << std::endl; // complete with your code
}

string ReconfigurationCode::onInterfaceDraw() {
  return "";
}
