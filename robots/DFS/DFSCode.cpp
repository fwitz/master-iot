#include "DFSCode.hpp"

DFSCode::DFSCode(BlinkyBlocksBlock *host):BlinkyBlocksBlockCode(host),module(host) {
    // @warning Do not remove block below, as a blockcode with a NULL host might be created
    //  for command line parsing
    if (not host) return;

    // Registers a callback (myGOFunc) to the message of type O
    addMessageEventFunc2(GO_MSG_ID,
                       std::bind(&DFSCode::myGOFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

    // Registers a callback (myBACKFunc) to the message of type C
    addMessageEventFunc2(BACK_MSG_ID,
                       std::bind(&DFSCode::myBACKFunc,this,
                       std::placeholders::_1, std::placeholders::_2));

}

void DFSCode::startup() {
    console << "start " << module->blockId << "\n";
    if (isLeader) {
        module->setColor(RED);
        int id = module->blockId; //Getting the actual block id
        parent = id; // The parent is himself
        // Send a GO message to all leader's neigbours to begin the spanning tree construction by distance.
        P2PNetworkInterface *neighbour;
        // We iterate through the interfaces of the block
        for (int i=0; i<hostBlock->getNbInterfaces();i++){
            neighbour=hostBlock->getInterface(i);
            // If the interface is connected, we send a GO message
            if(neighbour->connectedInterface){
                sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(id, 1)), neighbour, 1000,100);
            }
        }
  	}
}

void DFSCode::myGOFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {

    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    int id = module->blockId;
    P2PNetworkInterface *neighbour;
    // If the block has no parent
    if(parent == 0){
        parent = msgData.first; // Storing the parent id
        module->setColor(parent); // Setting the block's color to visualize the tree construction
        visited.push_back(msgData.first); // Add to the visited blocks the parent id
        // Getting all the block neighbours
        for (int i=0; i<hostBlock->getNbInterfaces();i++){
            neighbour=hostBlock->getInterface(i);
            if(neighbour -> connectedInterface){
                neighbours.push_back(neighbour -> getConnectedBlockId());
            }
        }
        // Checking if we visited all the neighbours
        bool same = true;
        if(neighbours.size() == visited.size()){
            for(int i=0;i<visited.size();i++){
                if(neighbours[i] != visited[i]){
                    same = false;
                }
            }
        }else{
            same = false;
        }
        // If we have visited all the neighbours, we send a confirmation to the sender
        // Else, we remove the visited blocks from the neighbours and send a message to all unvisited ones
        if(same == true){
            sendMessage("Back yes",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(0, id)), sender, 1000,100);
        }else{
            for(int i=0;i<visited.size();i++){
                std::vector<int>::iterator position = std::find(neighbours.begin(), neighbours.end(), visited[i]);
                // If the element was found we erase it from the list
                if (position != neighbours.end()){
                    neighbours.erase(position);
                }
            }
            for(int i=0;i<neighbours.size();i++){
                for(int j=0;j<hostBlock->getNbInterfaces();j++){
                    neighbour=hostBlock->getInterface(j);
                    if(neighbour->connectedInterface && neighbour->getConnectedBlockId() == neighbours[i]){
                        sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(id, 1)), neighbour, 1000,100);
                    }
                }
            }
        }
    }else{
        // If the block has a parent, their is nothing to do
        sendMessage("Back no",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(1, id)), sender, 1000,100);
    }

}

void DFSCode::myBACKFunc(std::shared_ptr<Message>_msg, P2PNetworkInterface*sender) {
    MessageOf<pair<int, int>>* msg = static_cast<MessageOf<pair<int, int>>*>(_msg.get());
    pair<int, int> msgData = *msg->getData();
    int id = module->blockId;
    P2PNetworkInterface *neighbour;
    // If we receive a 'yes', we add the sender as a child to this node
    if(msgData.first == 0){
        childs.push_back(msgData.second);
    }
    visited.push_back(msgData.second); // Adding the sender to the visited nodes list
    // Getting all block's neighbours
    for (int i=0; i<hostBlock->getNbInterfaces();i++){
        neighbour=hostBlock->getInterface(i);
        if(neighbour -> connectedInterface){
            neighbours.push_back(neighbour -> getConnectedBlockId());
        }
    }
    // Checking if we have visted all neighbours
    bool same = true;
    if(neighbours.size() == visited.size()){
        for(int i=0;i<visited.size();i++){
            if(neighbours[i] != visited[i]){
                same = false;
            }
        }
    }else{
        same = false;
    }
    // If we have visited all the neighbours, we send a confirmation to the sender
    // Else, we remove the visited blocks from the neighbours and send a message to all unvisited ones
    if(same == true){
        for (int i=0; i<hostBlock->getNbInterfaces();i++){
            neighbour=hostBlock->getInterface(i);
            if(neighbour -> connectedInterface && neighbour->getConnectedBlockId() == parent){
                sendMessage("Back yes",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(0, id)), neighbour, 1000,100);
            }
        }
    }else{
        for(int i=0;i<visited.size();i++){
            std::vector<int>::iterator position = std::find(neighbours.begin(), neighbours.end(), visited[i]);
            // If the element was found we erase it from the list
            if (position != neighbours.end()){
                neighbours.erase(position);
            }
        }
        for(int i=0;i<neighbours.size();i++){
            for(int j=0;j<hostBlock->getNbInterfaces();j++){
                neighbour=hostBlock->getInterface(j);
                if(neighbour->connectedInterface && neighbour->getConnectedBlockId() == neighbours[i]){
                    sendMessage("Go",new MessageOf<pair<int,int>>(GO_MSG_ID,make_pair(id, 1)), neighbour, 1000,100);
                }
            }
        }
    }
}


void DFSCode::parseUserBlockElements(TiXmlElement *config) {
      const char *attr = config->Attribute("leader");
    if (attr!=nullptr) {
        std::cout << module->blockId << " is leader!" << std::endl;
        isLeader = true;
    }
}

void DFSCode::onUserKeyPressed(unsigned char c, int x, int y) {
    switch (c) {
        case 'a' : // update with your code
        break;
    }
}

void DFSCode::onTap(int face) {
    std::cout << "Block 'tapped':" << module->blockId << std::endl; // complete with your code
}
